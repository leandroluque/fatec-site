# Pré-matrícula

Para preparar o sistema para a pré-matrícula, acesse o endereço: /admin/enrollment/configure

Siga os passos de 1 a 4.

Reinicie o servidor Tomcat, pois as configurações de semestre/ano são gravadas em cache em memória.