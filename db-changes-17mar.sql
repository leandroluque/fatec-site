ALTER_TABLE _request SET document_type='OTHER' WHERE document_type='PRESENTATION_LETTER';

INSERT INTO _document_request (id, creation_date, hash, last_update, document_type, request_situation, request_id, enabled)
SELECT id, creation_date, hash, last_update, document_type, request_situation, id, enabled FROM _request;

ALTER TABLE _request DROP COLUMN document_type;
ALTER TABLE _request DROP COLUMN request_situation;
ALTER TABLE _request MODIFY comments VARCHAR(1000);
