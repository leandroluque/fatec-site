# Employee courses
ALTER TABLE _employee_courses ADD COLUMN course_temp VARCHAR(50);
UPDATE _employee_courses ec SET course_temp=(SELECT acronym FROM _course WHERE id=ec.course);
DELETE t1 FROM _employee_courses t1 INNER JOIN _employee_courses t2 WHERE t1.employee = t2.employee AND t1.course_temp = t2.course_temp AND t1.course < t2.course;

ALTER TABLE _employee_courses DROP FOREIGN KEY `FKo8jfabw263kj99aittgijh2ot`;
ALTER TABLE _employee_courses DROP FOREIGN KEY `FKpyqhq7qynkox8uuyda1x0rjmb`;
ALTER TABLE _employee_courses DROP INDEX `FKpyqhq7qynkox8uuyda1x0rjmb`;
ALTER TABLE _employee_courses DROP PRIMARY KEY;

ALTER TABLE _employee_courses DROP COLUMN course;
ALTER TABLE _employee_courses CHANGE COLUMN course_temp course VARCHAR(50) NOT NULL;
ALTER TABLE _employee_courses ADD PRIMARY KEY (employee, course);
ALTER TABLE _employee_courses ADD FOREIGN KEY (employee) REFERENCES _employee (id);

# Course coordination
UPDATE _employee ec SET coordinator_course=(SELECT acronym FROM _course WHERE id=ec.coordinator_course_id);
ALTER TABLE _employee DROP FOREIGN KEY `FKjl2lcin0qgvpcxe3sih2tyusa`;
ALTER TABLE _employee DROP COLUMN coordinator_course_id;

# Internship responsibility courses
ALTER TABLE _internship_responsibility_courses ADD COLUMN course_temp VARCHAR(50);
UPDATE _internship_responsibility_courses ec SET course_temp=(SELECT acronym FROM _course WHERE id=ec.course);
DELETE t1 FROM _internship_responsibility_courses t1 INNER JOIN _internship_responsibility_courses t2 WHERE t1.employee = t2.employee AND t1.course_temp = t2.course_temp AND t1.course < t2.course;

ALTER TABLE _internship_responsibility_courses DROP FOREIGN KEY `FK82j7c6gdu7fv0kct6qchecmcx`;
ALTER TABLE _internship_responsibility_courses DROP PRIMARY KEY;

ALTER TABLE _internship_responsibility_courses DROP COLUMN course;
ALTER TABLE _internship_responsibility_courses CHANGE COLUMN course_temp course VARCHAR(50) NOT NULL;
ALTER TABLE _internship_responsibility_courses ADD PRIMARY KEY (employee, course);
ALTER TABLE _internship_responsibility_courses ADD FOREIGN KEY (employee) REFERENCES _employee (id);

# Congregation courses
ALTER TABLE _congregation_courses ADD COLUMN course_temp VARCHAR(50);
UPDATE _congregation_courses ec SET course_temp=(SELECT acronym FROM _course WHERE id=ec.course);
DELETE t1 FROM _congregation_courses t1 INNER JOIN _congregation_courses t2 WHERE t1.employee = t2.employee AND t1.course_temp = t2.course_temp AND t1.course < t2.course;

ALTER TABLE _congregation_courses DROP FOREIGN KEY `FK5mijgexfiqtxinboy5f3ubweu`;
ALTER TABLE _congregation_courses DROP PRIMARY KEY;

ALTER TABLE _congregation_courses DROP COLUMN course;
ALTER TABLE _congregation_courses CHANGE COLUMN course_temp course VARCHAR(50) NOT NULL;
ALTER TABLE _congregation_courses ADD PRIMARY KEY (employee, course);
ALTER TABLE _congregation_courses ADD FOREIGN KEY (employee) REFERENCES _employee (id);

