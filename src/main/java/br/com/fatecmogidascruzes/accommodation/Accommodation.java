package br.com.fatecmogidascruzes.accommodation;

import br.com.fatecmogidascruzes.domain.EntityImpl;
import br.com.fatecmogidascruzes.student.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_accommodation")
public class Accommodation extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "student", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Student student;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "accommodation")
	private Set<DisciplineToAdd> disciplinesToAdd = new HashSet<>();

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "accommodation")
	private Set<DisciplineToRemove> disciplinesToRemove = new HashSet<>();

	@Basic
	@Column(name = "semester", nullable = false)
	private int semester;
	
	@Basic
	@Column(name = "year", nullable = false)
	private int year;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_accomm")
	@Id
	@Override
	@SequenceGenerator(name = "seq_accomm", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}