package br.com.fatecmogidascruzes.accommodation;

import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.domain.EntityImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_disc_to_add")
public class DisciplineToAdd extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "accommodation", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Accommodation accommodation;

	@JoinColumn(name = "discipline_offer", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private DisciplineOffer disciplineOffer;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_disc_to_add")
	@Id
	@Override
	@SequenceGenerator(name = "seq_disc_to_add", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}