package br.com.fatecmogidascruzes.accommodation.data;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AccommodationDAO extends DAOImpl<Accommodation, Long>, JpaRepository<Accommodation, Long> {

	Optional<Accommodation> findByEnabledTrueAndStudentAndSemesterAndYear(Student student, int semester, int year);

	List<Accommodation> findByEnabledTrueAndSemesterAndYear(int semester, int year);

	@Query("SELECT acc FROM Accommodation acc LEFT JOIN acc.student st WHERE TRUE = acc.enabled AND (UPPER(st.name) LIKE CONCAT('%',:filter,'%') OR UPPER(st.username) LIKE CONCAT('%',:filter,'%') OR UPPER(st.email) LIKE CONCAT('%',:filter,'%') OR UPPER(st.password) LIKE CONCAT('%',:filter,'%'))")
	Page<Accommodation> getEnabledByFilter(@Param("filter") String filter, Pageable pageable);
}