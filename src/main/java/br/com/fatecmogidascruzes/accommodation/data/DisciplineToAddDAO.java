package br.com.fatecmogidascruzes.accommodation.data;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DisciplineToAddDAO extends DAOImpl<DisciplineToAdd, Long>, JpaRepository<DisciplineToAdd, Long> {

	Optional<DisciplineToAdd> findByEnabledTrueAndAccommodationAndDisciplineOffer(Accommodation accommodation,
			DisciplineOffer disciplineOffer);

}