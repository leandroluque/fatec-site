package br.com.fatecmogidascruzes.accommodation.data;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DisciplineToRemoveDAO
		extends DAOImpl<DisciplineToRemove, Long>, JpaRepository<DisciplineToRemove, Long> {

	Optional<DisciplineToRemove> findByEnabledTrueAndAccommodationAndDisciplineOffer(Accommodation accommodation,
			DisciplineOffer disciplineOffer);

}