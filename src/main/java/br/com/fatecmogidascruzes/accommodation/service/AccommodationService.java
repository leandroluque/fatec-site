package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface AccommodationService extends BaseService<Accommodation, Long> {

	Optional<Accommodation> getEnabledByStudentAndSemesterAndYear(Student student, int semester, int year);

	List<Accommodation> getEnabledBySemesterAndYear(int semester, int year);

	Page<Accommodation> getEnabledByFilter(SearchCriteria searchCriteria);

}
