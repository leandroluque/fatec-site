package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.data.AccommodationDAO;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccommodationServiceImpl extends BaseServiceImpl<Accommodation, Long> implements AccommodationService {

	private AccommodationDAO accommodationDAO;

	public AccommodationServiceImpl(AccommodationDAO accommodationDAO) {
		super(accommodationDAO);
		this.accommodationDAO = accommodationDAO;
	}

	@Override
	public List<Accommodation> getEnabledBySemesterAndYear(int semester, int year) {
		return accommodationDAO.findByEnabledTrueAndSemesterAndYear(semester, year);
	}

	@Override
	public Optional<Accommodation> getEnabledByStudentAndSemesterAndYear(Student student, int semester, int year) {
		return accommodationDAO.findByEnabledTrueAndStudentAndSemesterAndYear(student, semester, year);
	}

	@Override
	public Page<Accommodation> getEnabledByFilter(SearchCriteria searchCriteria) {
		return accommodationDAO.getEnabledByFilter(searchCriteria.getFilter(), prepareCriteria(searchCriteria));
	}
}
