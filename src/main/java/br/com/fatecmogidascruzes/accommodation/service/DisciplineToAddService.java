package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.Optional;

public interface DisciplineToAddService extends BaseService<DisciplineToAdd, Long> {

	Optional<DisciplineToAdd> getEnabledByAccommodationAndDisciplineOffer(Accommodation accommodation,
			DisciplineOffer disciplineOffer);

}
