package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.accommodation.data.DisciplineToAddDAO;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DisciplineToAddServiceImpl extends BaseServiceImpl<DisciplineToAdd, Long>
		implements DisciplineToAddService {

	private DisciplineToAddDAO accommodationDAO;

	public DisciplineToAddServiceImpl(DisciplineToAddDAO accommodationDAO) {
		super(accommodationDAO);
		this.accommodationDAO = accommodationDAO;
	}

	@Override
	public Optional<DisciplineToAdd> getEnabledByAccommodationAndDisciplineOffer(Accommodation accommodation,
			DisciplineOffer disciplineOffer) {
		return accommodationDAO.findByEnabledTrueAndAccommodationAndDisciplineOffer(accommodation, disciplineOffer);
	}

}
