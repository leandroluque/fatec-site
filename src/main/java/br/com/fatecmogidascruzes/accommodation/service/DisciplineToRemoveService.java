package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.Optional;

public interface DisciplineToRemoveService extends BaseService<DisciplineToRemove, Long> {

	Optional<DisciplineToRemove> getEnabledByAccommodationAndDisciplineOffer(Accommodation accommodation, DisciplineOffer disciplineOffer);

}
