package br.com.fatecmogidascruzes.accommodation.service;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import br.com.fatecmogidascruzes.accommodation.data.DisciplineToRemoveDAO;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DisciplineToRemoveServiceImpl extends BaseServiceImpl<DisciplineToRemove, Long>
		implements DisciplineToRemoveService {

	private DisciplineToRemoveDAO accommodationDAO;

	public DisciplineToRemoveServiceImpl(DisciplineToRemoveDAO accommodationDAO) {
		super(accommodationDAO);
		this.accommodationDAO = accommodationDAO;
	}

	@Override
	public Optional<DisciplineToRemove> getEnabledByAccommodationAndDisciplineOffer(Accommodation accommodation,
			DisciplineOffer disciplineOffer) {
		return accommodationDAO.findByEnabledTrueAndAccommodationAndDisciplineOffer(accommodation, disciplineOffer);
	}

}
