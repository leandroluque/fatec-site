package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import br.com.fatecmogidascruzes.accommodation.service.AccommodationService;
import br.com.fatecmogidascruzes.accommodation.service.DisciplineToAddService;
import br.com.fatecmogidascruzes.accommodation.service.DisciplineToRemoveService;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.discipline.service.web.DisciplineTableRowDTO;
import br.com.fatecmogidascruzes.dto.TableDTO;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/admin/accommodations")
public class AccommodationController {

	private final AccommodationWebService accommodationWebService;
	private final AccommodationService accommodationService;
	private final DisciplineOfferService disciplineOfferService;
	private final DisciplineToAddService disciplineToAddService;
	private final DisciplineToRemoveService disciplineToRemoveService;

	@Autowired
	public AccommodationController(AccommodationWebService accommodationWebService,
			AccommodationService accommodationService, DisciplineOfferService disciplineOfferService,
			DisciplineToAddService disciplineToAddService, DisciplineToRemoveService disciplineToRemoveService) {
		super();
		this.accommodationWebService = accommodationWebService;
		this.accommodationService = accommodationService;
		this.disciplineOfferService = disciplineOfferService;
		this.disciplineToAddService = disciplineToAddService;
		this.disciplineToRemoveService = disciplineToRemoveService;
	}

	@GetMapping
	public ModelAndView search(RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("/accommodation/search");
		modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));
		return modelAndView;
	}

	// Fields to filter the table - related to the JPQL.
	private static String[] fields = { null, "student.name" };

	@RequestMapping(path = "/table", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody TableDTO<AccommodationTableRowDTO> getTable(
			@RequestParam(name = "draw", required = false) Integer draw,
			@RequestParam(name = "start", required = false, defaultValue = "0") Integer initialPage,
			@RequestParam(name = "length", required = false, defaultValue = "10") Integer numberOfRegisters,
			@RequestParam(name = "search[value]", required = false, defaultValue = "") String filter,
			@RequestParam(name = "order[0][column]", required = false, defaultValue = "1") Integer columnsToSort,
			@RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String columnsOrder) {

		SearchCriteria searchCriteria = new SearchCriteria();
		if (filter != null && !filter.equals("")) {
			searchCriteria.setFilter(filter);
		}
		if (columnsToSort < fields.length) {
			String fieldName = fields[columnsToSort];
			searchCriteria.addSortBy(fieldName);
			searchCriteria
					.setOrder(
							columnsOrder.equalsIgnoreCase("asc") ? SearchCriteria.Order.ASCENDING
									: SearchCriteria.Order.DESCENDING);
		}

		searchCriteria.setWhatToFilter(Arrays.asList(fields));
		searchCriteria.setInitialRegister(initialPage / numberOfRegisters);
		searchCriteria.setNumberOfRegisters(numberOfRegisters);

		return accommodationWebService.getTable(searchCriteria, draw);
	}

	@GetMapping(path = "/summary")
	public ModelAndView getSummary() {
		ModelAndView modelAndView = new ModelAndView("accommodation/summary");

		modelAndView.addObject("accommodations", this.accommodationWebService.getSummary());

		return modelAndView;
	}

	@RequestMapping("/{accommodationHashString}")
	public ModelAndView edit(
			@PathVariable("accommodationHashString") UUID accommodationHash,
			RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("accommodation/edit");

		Optional<Accommodation> accommodationOptional = this.accommodationService.getEnabledByHash(accommodationHash);
		if (accommodationOptional.isPresent()) {
			Accommodation accommodation = accommodationOptional.get();
			modelAndView.addObject("accommodation", accommodation);
			modelAndView.addObject("student", accommodation.getStudent());
		}

		modelAndView
				.addObject(
						"disciplines",
						DisciplineTableRowDTO
								.listFrom(
										this.disciplineOfferService
												.getEnabledBySemesterAndYear(
														AccommodationSiteController.currentSemester,
														AccommodationSiteController.currentYear)));

		modelAndView.addObject("error", redirectAttributes.getFlashAttributes().get("error"));

		return modelAndView;
	}

	@RequestMapping("/removeDiscipline")
	public ModelAndView removeDiscipline(
			@RequestParam("accommodationHashString") UUID accommodationHash,
			@RequestParam("disciplineOfferHashString") UUID disciplineOfferHash,
			RedirectAttributes redirectAttributes) {

		Optional<Accommodation> accommodationOptional = this.accommodationService.getEnabledByHash(accommodationHash);
		if (accommodationOptional.isPresent()) {

			Accommodation accommodation = accommodationOptional.get();

			Optional<DisciplineOffer> disciplineOfferOptional = this.disciplineOfferService
					.getByHash(disciplineOfferHash);
			if (disciplineOfferOptional.isPresent()) {

				DisciplineToRemove disciplineToRemove = null;
				Optional<DisciplineToRemove> disciplineToRemoveOptional = this.disciplineToRemoveService
						.getEnabledByAccommodationAndDisciplineOffer(accommodation, disciplineOfferOptional.get());
				if (disciplineToRemoveOptional.isPresent()) {
					disciplineToRemove = disciplineToRemoveOptional.get();
					disciplineToRemove.setEnabled(true);
				} else {
					disciplineToRemove = new DisciplineToRemove();
					disciplineToRemove.setAccommodation(accommodation);
					disciplineToRemove
							.setDisciplineOffer(this.disciplineOfferService.getByHash(disciplineOfferHash).get());
				}
				this.disciplineToRemoveService.save(disciplineToRemove);

			} else {
				redirectAttributes.addAttribute("error", "The discipline does not exist.");
			}

		} else {
			redirectAttributes.addAttribute("error", "The accommodation does not exist.");
		}

		return new ModelAndView(
				"redirect:/admin/accommodations/" + FriendlyId.toFriendlyId(accommodationHash) + "?hash="
						+ UUID.randomUUID().toString());
	}

	@RequestMapping("/addDiscipline")
	public ModelAndView addDiscipline(
			@RequestParam("accommodationHashString") UUID accommodationHash,
			@RequestParam("disciplineOfferHashString") UUID disciplineOfferHash,
			RedirectAttributes redirectAttributes) {

		Optional<Accommodation> accommodationOptional = this.accommodationService.getEnabledByHash(accommodationHash);
		if (accommodationOptional.isPresent()) {

			Accommodation accommodation = accommodationOptional.get();

			Optional<DisciplineOffer> disciplineOfferOptional = this.disciplineOfferService
					.getByHash(disciplineOfferHash);
			if (disciplineOfferOptional.isPresent()) {

				DisciplineToAdd disciplineToAdd = null;
				Optional<DisciplineToAdd> disciplineToAddOptional = this.disciplineToAddService
						.getEnabledByAccommodationAndDisciplineOffer(accommodation, disciplineOfferOptional.get());
				if (disciplineToAddOptional.isPresent()) {
					disciplineToAdd = disciplineToAddOptional.get();
					disciplineToAdd.setEnabled(true);
				} else {
					disciplineToAdd = new DisciplineToAdd();
					disciplineToAdd.setAccommodation(accommodation);
					disciplineToAdd
							.setDisciplineOffer(this.disciplineOfferService.getByHash(disciplineOfferHash).get());
				}
				this.disciplineToAddService.save(disciplineToAdd);

			} else {
				redirectAttributes.addAttribute("error", "The discipline does not exist.");
			}

		} else {
			redirectAttributes.addAttribute("error", "The accommodation does not exist.");
		}

		return new ModelAndView(
				"redirect:/admin/accommodations/" + FriendlyId.toFriendlyId(accommodationHash) + "?hash="
						+ UUID.randomUUID().toString());
	}

	@RequestMapping("/declineAdding")
	public ModelAndView declineAdding(
			@RequestParam("accommodationHashString") UUID accommodationHash,
			@RequestParam("disciplineHashString") UUID disciplineHash,
			RedirectAttributes redirectAttributes) {

		Optional<Accommodation> accommodationOptional = this.accommodationService.getEnabledByHash(accommodationHash);
		if (accommodationOptional.isPresent()) {

			DisciplineToAdd disciplineToAdd = null;
			Optional<DisciplineToAdd> disciplineToAddOptional = this.disciplineToAddService
					.getEnabledByHash(disciplineHash);
			if (disciplineToAddOptional.isPresent()) {
				disciplineToAdd = disciplineToAddOptional.get();
				disciplineToAdd.setEnabled(false);
				this.disciplineToAddService.save(disciplineToAdd);
			}

		} else {
			redirectAttributes.addAttribute("error", "The accommodation does not exist.");
		}

		return new ModelAndView(
				"redirect:/admin/accommodations/" + FriendlyId.toFriendlyId(accommodationHash) + "?hash="
						+ UUID.randomUUID().toString());

	}

	@RequestMapping("/declineRemoval")
	public ModelAndView declineRemoval(
			@RequestParam("accommodationHashString") UUID accommodationHash,
			@RequestParam("disciplineHashString") UUID disciplineHash,
			RedirectAttributes redirectAttributes) {
		Optional<Accommodation> accommodationOptional = this.accommodationService.getEnabledByHash(accommodationHash);
		if (accommodationOptional.isPresent()) {

			DisciplineToRemove disciplineToRemove = null;
			Optional<DisciplineToRemove> disciplineToRemoveOptional = this.disciplineToRemoveService
					.getByHash(disciplineHash);
			if (disciplineToRemoveOptional.isPresent()) {
				disciplineToRemove = disciplineToRemoveOptional.get();
				disciplineToRemove.setEnabled(false);
				this.disciplineToRemoveService.save(disciplineToRemove);
			}

		} else {
			redirectAttributes.addAttribute("error", "The accommodation does not exist.");
		}

		return new ModelAndView(
				"redirect:/admin/accommodations/" + FriendlyId.toFriendlyId(accommodationHash) + "?hash="
						+ UUID.randomUUID().toString());
	}

}
