package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import br.com.fatecmogidascruzes.accommodation.service.AccommodationService;
import br.com.fatecmogidascruzes.accommodation.service.DisciplineToAddService;
import br.com.fatecmogidascruzes.accommodation.service.DisciplineToRemoveService;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.discipline.service.web.DisciplineTableRowDTO;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static br.com.fatecmogidascruzes.config.SemesterYearConfig.*;

@Controller
@RequestMapping("/accommodation")
public class AccommodationSiteController {

	private final StudentService studentService;
	private final AccommodationService accommodationService;
	private final DisciplineOfferService disciplineOfferService;
	private final DisciplineToAddService disciplineToAddService;
	private final DisciplineToRemoveService disciplineToRemoveService;

	public static int currentSemester = getPreEnrollSemester();;
	public static int currentYear = getPreEnrollYear();

	private static LocalDate start = LocalDate.of(currentYear, getStartAccomodationMonth(), getStartAccomodationDay());
	private static LocalDate end = LocalDate.of(currentYear, getEndAccomodationMonth(), getEndAccomodationDay());

	@Autowired
	public AccommodationSiteController(StudentService studentService, AccommodationService accommodationService,
			DisciplineOfferService disciplineOfferService, DisciplineToAddService disciplineToAddService,
			DisciplineToRemoveService disciplineToRemoveService) {
		super();
		this.studentService = studentService;
		this.accommodationService = accommodationService;
		this.disciplineOfferService = disciplineOfferService;
		this.disciplineToAddService = disciplineToAddService;
		this.disciplineToRemoveService = disciplineToRemoveService;
	}

	@RequestMapping
	public String index() {
		return "accommodation/accommodationSignIn";
	}

	@RequestMapping("/signIn")
	public ModelAndView signIn(@RequestParam("username") String username, @RequestParam("password") String password) {
		Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			ModelAndView modelAndView = new ModelAndView("accommodation/siteEdit");
			modelAndView.addObject("student", student);

			Optional<Accommodation> accommodationOptional = this.accommodationService
					.getEnabledByStudentAndSemesterAndYear(student, currentSemester, currentYear);
			if (accommodationOptional.isPresent()) {
				modelAndView.addObject("accommodation", accommodationOptional.get());
			} else {
				Accommodation accommodation = new Accommodation();
				accommodation.setStudent(student);
				accommodation.setSemester(currentSemester);
				accommodation.setYear(currentYear);
				accommodationService.save(accommodation);
				modelAndView.addObject("accommodation", accommodation);
			}

			modelAndView.addObject("disciplines", DisciplineTableRowDTO
					.listFrom(this.disciplineOfferService.getEnabledBySemesterAndYear(currentSemester, currentYear)));

			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}
	}

	@RequestMapping("/removeDiscipline")
	public ModelAndView removeDiscipline(@RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("studentHashString") UUID studentHash,
			@RequestParam("disciplineOfferHashString") UUID disciplineOfferHash) {
		if (LocalDate.now().isBefore(start) || LocalDate.now().isAfter(end)) {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

		Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			Optional<Accommodation> accommodationOptional = this.accommodationService
					.getEnabledByStudentAndSemesterAndYear(student, currentSemester, currentYear);
			Accommodation accommodation = null;
			if (accommodationOptional.isPresent()) {
				accommodation = accommodationOptional.get();
			} else {
				accommodation = new Accommodation();
				accommodation.setStudent(student);
				accommodation.setSemester(currentSemester);
				accommodation.setYear(currentYear);
				accommodationService.save(accommodation);
			}

			Optional<DisciplineOffer> disciplineOfferOptional = this.disciplineOfferService
					.getByHash(disciplineOfferHash);
			if (disciplineOfferOptional.isPresent()) {

				DisciplineToRemove disciplineToRemove = null;
				Optional<DisciplineToRemove> disciplineToRemoveOptional = this.disciplineToRemoveService
						.getEnabledByAccommodationAndDisciplineOffer(accommodation, disciplineOfferOptional.get());
				if (disciplineToRemoveOptional.isPresent()) {
					disciplineToRemove = disciplineToRemoveOptional.get();
					disciplineToRemove.setEnabled(true);
				} else {
					disciplineToRemove = new DisciplineToRemove();
					disciplineToRemove.setAccommodation(accommodation);
					disciplineToRemove
							.setDisciplineOffer(this.disciplineOfferService.getByHash(disciplineOfferHash).get());
				}
				this.disciplineToRemoveService.save(disciplineToRemove);

				return new ModelAndView("redirect:/accommodation/signIn?username=" + username + "&password=" + password
						+ "&hash=" + UUID.randomUUID().toString());
			} else {
				ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
				return modelAndView;
			}

		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}
	}

	@RequestMapping("/addDiscipline")
	public ModelAndView addDiscipline(@RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("studentHashString") UUID studentHash,
			@RequestParam("disciplineOfferHashString") UUID disciplineOfferHash) {
		if (LocalDate.now().isBefore(start) || LocalDate.now().isAfter(end)) {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

		Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			Optional<Accommodation> accommodationOptional = this.accommodationService
					.getEnabledByStudentAndSemesterAndYear(student, currentSemester, currentYear);
			Accommodation accommodation = null;
			if (accommodationOptional.isPresent()) {
				accommodation = accommodationOptional.get();
			} else {
				accommodation = new Accommodation();
				accommodation.setStudent(student);
				accommodation.setSemester(currentSemester);
				accommodation.setYear(currentYear);
				accommodationService.save(accommodation);
			}

			Optional<DisciplineOffer> disciplineOfferOptional = this.disciplineOfferService
					.getByHash(disciplineOfferHash);
			if (disciplineOfferOptional.isPresent()) {

				DisciplineToAdd disciplineToAdd = null;
				Optional<DisciplineToAdd> disciplineToAddOptional = this.disciplineToAddService
						.getEnabledByAccommodationAndDisciplineOffer(accommodation, disciplineOfferOptional.get());
				if (disciplineToAddOptional.isPresent()) {
					disciplineToAdd = disciplineToAddOptional.get();
					disciplineToAdd.setEnabled(true);
				} else {
					disciplineToAdd = new DisciplineToAdd();
					disciplineToAdd.setAccommodation(accommodation);
					disciplineToAdd
							.setDisciplineOffer(this.disciplineOfferService.getByHash(disciplineOfferHash).get());
				}
				this.disciplineToAddService.save(disciplineToAdd);

				return new ModelAndView("redirect:/accommodation/signIn?username=" + username + "&password=" + password
						+ "&hash=" + UUID.randomUUID().toString());

			} else {

				ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
				return modelAndView;

			}

		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

	}

	@RequestMapping("/declineAdding")
	public ModelAndView declineAdding(@RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("studentHashString") UUID studentHash,
			@RequestParam("disciplineHashString") UUID disciplineHash) {
		if (LocalDate.now().isBefore(start) || LocalDate.now().isAfter(end)) {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

		Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			Optional<Accommodation> accommodationOptional = this.accommodationService
					.getEnabledByStudentAndSemesterAndYear(student, currentSemester, currentYear);
			Accommodation accommodation = null;
			if (accommodationOptional.isPresent()) {
				accommodation = accommodationOptional.get();
			} else {
				accommodation = new Accommodation();
				accommodation.setStudent(student);
				accommodation.setSemester(currentSemester);
				accommodation.setYear(currentYear);
				accommodationService.save(accommodation);
			}

			DisciplineToAdd disciplineToAdd = null;
			Optional<DisciplineToAdd> disciplineToAddOptional = this.disciplineToAddService
					.getEnabledByHash(disciplineHash);
			if (disciplineToAddOptional.isPresent()) {
				disciplineToAdd = disciplineToAddOptional.get();
				disciplineToAdd.setEnabled(false);
				this.disciplineToAddService.save(disciplineToAdd);
			}

			return new ModelAndView("redirect:/accommodation/signIn?username=" + username + "&password=" + password
					+ "&hash=" + UUID.randomUUID().toString());

		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

	}

	@RequestMapping("/declineRemoval")
	public ModelAndView declineRemoval(@RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("studentHashString") UUID studentHash,
			@RequestParam("disciplineHashString") UUID disciplineHash) {
		if (LocalDate.now().isBefore(start) || LocalDate.now().isAfter(end)) {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

		Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			Optional<Accommodation> accommodationOptional = this.accommodationService
					.getEnabledByStudentAndSemesterAndYear(student, currentSemester, currentYear);
			Accommodation accommodation = null;
			if (accommodationOptional.isPresent()) {
				accommodation = accommodationOptional.get();
			} else {
				accommodation = new Accommodation();
				accommodation.setStudent(student);
				accommodation.setSemester(currentSemester);
				accommodation.setYear(currentYear);
				accommodationService.save(accommodation);
			}

			DisciplineToRemove disciplineToRemove = null;
			Optional<DisciplineToRemove> disciplineToRemoveOptional = this.disciplineToRemoveService
					.getByHash(disciplineHash);
			if (disciplineToRemoveOptional.isPresent()) {
				disciplineToRemove = disciplineToRemoveOptional.get();
				disciplineToRemove.setEnabled(false);
				this.disciplineToRemoveService.save(disciplineToRemove);
			}

			return new ModelAndView("redirect:/accommodation/signIn?username=" + username + "&password=" + password
					+ "&hash=" + UUID.randomUUID().toString());

		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/accommodation?error=");
			return modelAndView;
		}

	}

}
