package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.DisciplineToAdd;
import br.com.fatecmogidascruzes.accommodation.DisciplineToRemove;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.*;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class AccommodationSummaryEntryDTO {

	private UUID hash;
	private String hashString;
	private String studentCourse;
	private String studentShift;
	private String studentRegistry;
	private String studentName;
	private Set<DisciplineEntry> disciplinesToAdd = new HashSet<>();
	private Set<DisciplineEntry> disciplinesToRemove = new HashSet<>();

	public void addDisciplineToAdd(DisciplineEntry discipline) {
		this.disciplinesToAdd.add(discipline);
	}

	public void addDisciplineToRemove(DisciplineEntry discipline) {
		this.disciplinesToRemove.add(discipline);
	}

	@AllArgsConstructor
	@Getter
	public static class DisciplineEntry {

		private String code;
		private String name;
		private String shift;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DisciplineEntry other = (DisciplineEntry) obj;
			if (code == null) {
				if (other.code != null)
					return false;
			} else if (!code.equals(other.code))
				return false;
			return true;
		}

	}

	public static AccommodationSummaryEntryDTO from(Accommodation object) {
		AccommodationSummaryEntryDTO dto = new AccommodationSummaryEntryDTO();
		dto.setHash(object.getHash());
		dto.setHashString(FriendlyId.toFriendlyId(object.getHash()));
		dto.setStudentName(object.getStudent().getName());
		dto.setStudentRegistry(object.getStudent().getRegistry());
		dto.setStudentCourse(object.getStudent().getCourse().getAcronym());
		dto.setStudentShift(object.getStudent().getCourse().getShift().getName());

		for (DisciplineToAdd discipline : object.getDisciplinesToAdd()) {
			if (discipline.isEnabled()) {
				String shift = null != discipline.getDisciplineOffer().getDisciplineClasses()
						&& !discipline.getDisciplineOffer().getDisciplineClasses().isEmpty()
						&& null != discipline.getDisciplineOffer().getDisciplineClasses().iterator().next().getWeekDay()
								? discipline
										.getDisciplineOffer()
										.getDisciplineClasses()
										.iterator()
										.next()
										.getWeekDay()
										.getName()
								: "";
				if (!shift.isEmpty()) {
					shift += " - " + discipline
							.getDisciplineOffer()
							.getDisciplineClasses()
							.iterator()
							.next()
							.getTimeSlot()
							.getName();
				}
				dto
						.addDisciplineToAdd(
								new DisciplineEntry(
										discipline.getDisciplineOffer().getDiscipline().getAcronym(),
										discipline.getDisciplineOffer().getDiscipline().getName(),
										shift));
			}
		}

		for (DisciplineToRemove discipline : object.getDisciplinesToRemove()) {
			if (discipline.isEnabled()) {
				String shift = null != discipline.getDisciplineOffer().getDisciplineClasses()
						&& !discipline.getDisciplineOffer().getDisciplineClasses().isEmpty()
						&& null != discipline.getDisciplineOffer().getDisciplineClasses().iterator().next().getWeekDay()
								? discipline
										.getDisciplineOffer()
										.getDisciplineClasses()
										.iterator()
										.next()
										.getWeekDay()
										.getName()
								: "";
				if (!shift.isEmpty()) {
					shift += " - " + discipline
							.getDisciplineOffer()
							.getDisciplineClasses()
							.iterator()
							.next()
							.getTimeSlot()
							.getName();
				}
				dto
						.addDisciplineToRemove(
								new DisciplineEntry(
										discipline.getDisciplineOffer().getDiscipline().getAcronym(),
										discipline.getDisciplineOffer().getDiscipline().getName(),
										shift));
			}
		}

		return dto;
	}

	public static List<AccommodationSummaryEntryDTO> listFrom(List<Accommodation> objects) {
		List<AccommodationSummaryEntryDTO> objectTableRowDTOs = new ArrayList<>();
		objects.forEach(baseCategory -> objectTableRowDTOs.add(AccommodationSummaryEntryDTO.from(baseCategory)));
		return objectTableRowDTOs;
	}

}
