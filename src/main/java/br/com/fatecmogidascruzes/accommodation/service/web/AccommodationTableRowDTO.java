package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class AccommodationTableRowDTO {

	private UUID hash;
	private String hashString;
	private String studentName;
	private int disciplinesToAdd;
	private int disciplinesToRemove;

	public static AccommodationTableRowDTO from(Accommodation object) {
		AccommodationTableRowDTO tableRowDTO = new AccommodationTableRowDTO();
		tableRowDTO.setHash(object.getHash());
		tableRowDTO.setHashString(FriendlyId.toFriendlyId(object.getHash()));
		tableRowDTO.setStudentName(object.getStudent().getName());
		tableRowDTO.setDisciplinesToAdd(object.getDisciplinesToAdd() != null ? object.getDisciplinesToAdd().size() : 0);
		tableRowDTO.setDisciplinesToRemove(
				object.getDisciplinesToRemove() != null ? object.getDisciplinesToRemove().size() : 0);
		return tableRowDTO;
	}

	public static List<AccommodationTableRowDTO> listFrom(List<Accommodation> objects) {
		List<AccommodationTableRowDTO> objectTableRowDTOs = new ArrayList<>();
		objects.forEach(baseCategory -> objectTableRowDTOs.add(AccommodationTableRowDTO.from(baseCategory)));
		return objectTableRowDTOs;
	}

}
