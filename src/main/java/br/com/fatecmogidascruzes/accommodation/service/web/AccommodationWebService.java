package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;

import java.util.List;

public interface AccommodationWebService {

	TableDTO<AccommodationTableRowDTO> getTable(SearchCriteria searchCriteria, int draw);

	List<AccommodationSummaryEntryDTO> getSummary();

}
