package br.com.fatecmogidascruzes.accommodation.service.web;

import br.com.fatecmogidascruzes.accommodation.Accommodation;
import br.com.fatecmogidascruzes.accommodation.service.AccommodationService;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

import static br.com.fatecmogidascruzes.config.SemesterYearConfig.getPreEnrollSemester;
import static br.com.fatecmogidascruzes.config.SemesterYearConfig.getPreEnrollYear;

@Service
public class AccommodationWebServiceImpl implements AccommodationWebService {

	private final AccommodationService accommodationService;

	public static int currentSemester = getPreEnrollSemester();
	public static int currentYear = getPreEnrollYear();

	@Autowired
	public AccommodationWebServiceImpl(AccommodationService accommodationService) {
		super();
		this.accommodationService = accommodationService;
	}

	@Override
	public List<AccommodationSummaryEntryDTO> getSummary() {

		List<Accommodation> registers = this.accommodationService.getEnabledBySemesterAndYear(currentSemester, currentYear);
		return AccommodationSummaryEntryDTO.listFrom(registers);

	}

	@Override
	public TableDTO<AccommodationTableRowDTO> getTable(SearchCriteria searchCriteria, int draw) {

		Page<Accommodation> pages = this.accommodationService.getEnabledByFilter(searchCriteria);
		TableDTO<AccommodationTableRowDTO> table = new TableDTO<>();
		table.setDraw(draw);
		table.setRecordsTotal((int) accommodationService.countEnabled().longValue());
		table.setRecordsFiltered((int) pages.getTotalElements());

		table.setData(AccommodationTableRowDTO.listFrom(pages.getContent()));

		return table;
	}
}
