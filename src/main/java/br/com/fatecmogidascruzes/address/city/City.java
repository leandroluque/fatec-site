package br.com.fatecmogidascruzes.address.city;

import br.com.fatecmogidascruzes.address.state.State;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_city")
public class City extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "state", nullable = false)
	@ManyToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private State state;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_city")
	@Id
	@Override
	@SequenceGenerator(name = "seq_city", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}
