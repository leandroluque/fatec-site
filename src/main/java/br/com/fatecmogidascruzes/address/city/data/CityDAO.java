package br.com.fatecmogidascruzes.address.city.data;

import br.com.fatecmogidascruzes.address.city.City;
import br.com.fatecmogidascruzes.address.state.State;
import br.com.fatecmogidascruzes.data.DAOImpl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityDAO extends DAOImpl<City, Long>, JpaRepository<City, Long> {

	List<City> findByEnabledTrueAndState(State state);

}
