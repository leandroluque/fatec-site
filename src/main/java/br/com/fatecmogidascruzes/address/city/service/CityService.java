package br.com.fatecmogidascruzes.address.city.service;

import br.com.fatecmogidascruzes.address.city.City;
import br.com.fatecmogidascruzes.address.state.State;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.List;

public interface CityService extends BaseService<City, Long> {

	List<City> getEnabledByState(State state);

}
