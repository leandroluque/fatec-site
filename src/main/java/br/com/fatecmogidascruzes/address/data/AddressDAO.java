package br.com.fatecmogidascruzes.address.data;

import br.com.fatecmogidascruzes.address.Address;
import br.com.fatecmogidascruzes.data.DAOImpl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressDAO extends DAOImpl<Address, Long>, JpaRepository<Address, Long> {

}
