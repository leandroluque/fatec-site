package br.com.fatecmogidascruzes.address.state;

import br.com.fatecmogidascruzes.address.city.City;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_state")
public class State extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "acronym", nullable = false, length = 4)
	private String acronym;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "state")
	private Set<City> cities = new HashSet<>();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_state")
	@Id
	@Override
	@SequenceGenerator(name = "seq_state", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}
