package br.com.fatecmogidascruzes.address.state.data;

import br.com.fatecmogidascruzes.address.state.State;
import br.com.fatecmogidascruzes.data.DAOImpl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StateDAO extends DAOImpl<State, Long>, JpaRepository<State, Long> {

	Optional<State> findByEnabledTrueAndAcronym(String acronym);

}
