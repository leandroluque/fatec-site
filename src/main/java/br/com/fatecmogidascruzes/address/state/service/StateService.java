package br.com.fatecmogidascruzes.address.state.service;

import br.com.fatecmogidascruzes.address.state.State;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.Optional;

public interface StateService extends BaseService<State, Long> {

	Optional<State> getEnabledByAcronym(String acronym);

}
