package br.com.fatecmogidascruzes.agenda;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_agenda_entry", indexes = { @Index(name = "ind_age_ent_name", columnList = "name", unique = false) })
public class AgendaEntry extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "start_date", nullable = true)
	private LocalDate startDate;

	@Column(name = "end_date", nullable = true)
	private LocalDate endDate;

	@Column(name = "start_time", nullable = true)
	private LocalTime startTime;

	@Column(name = "end_time", nullable = true)
	private LocalTime endTime;

	@Basic
	@Column(name = "short_description", nullable = true, length = 100)
	private String shortDescription;

	@Column(name = "long_description", nullable = true)
	@Lob
	private String longDescription;

	@JoinColumn(name = "base_agenda_entry", nullable = true)
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	private AgendaEntry baseAgendaEntry;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_agenda_entry")
	@Override
	@SequenceGenerator(name = "seq_agenda_entry", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public AgendaEntry copy() {
		AgendaEntry event = new AgendaEntry();
		event.setEndDate(endDate);
		event.setEndTime(endTime);
		event.setName(name);
		event.setShortDescription(shortDescription);
		event.setStartDate(startDate);
		event.setStartTime(startTime);
		return event;
	}

}