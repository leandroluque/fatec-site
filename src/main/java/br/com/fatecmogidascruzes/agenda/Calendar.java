package br.com.fatecmogidascruzes.agenda;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.gallery.Album;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@Getter
@Entity
@Setter
@Table(name = "_calendar", indexes = { })
public class Calendar extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "file", nullable = false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private File file;

	public Calendar() {
		setId(1L);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_calendar")
	@Override
	@SequenceGenerator(name = "seq_calendar", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}