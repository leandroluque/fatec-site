package br.com.fatecmogidascruzes.agenda.data;

import br.com.fatecmogidascruzes.agenda.Calendar;
import br.com.fatecmogidascruzes.data.DAOImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CalendarDAO extends DAOImpl<Calendar, Long>, JpaRepository<Calendar, Long> {

	@Query("SELECT obj FROM Calendar obj")
	List<Calendar> getCalendar(Pageable pageable);

}
