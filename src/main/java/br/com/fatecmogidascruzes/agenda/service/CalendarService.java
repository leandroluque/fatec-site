package br.com.fatecmogidascruzes.agenda.service;

import br.com.fatecmogidascruzes.agenda.Calendar;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.Optional;

public interface CalendarService extends BaseService<Calendar, Long> {

	Optional<Calendar> getCalendar();

}