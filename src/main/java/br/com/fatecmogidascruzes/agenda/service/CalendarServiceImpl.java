package br.com.fatecmogidascruzes.agenda.service;

import br.com.fatecmogidascruzes.agenda.Calendar;
import br.com.fatecmogidascruzes.agenda.data.CalendarDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CalendarServiceImpl extends BaseServiceImpl<Calendar, Long> implements CalendarService {

	private final CalendarDAO calendarDAO;

	public CalendarServiceImpl(CalendarDAO calendarDAO) {
		super(calendarDAO);
		this.calendarDAO = calendarDAO;
	}

	@Override
	public Optional<Calendar> getCalendar() {
		return this.calendarDAO.getCalendar(PageRequest.of(0,1)).stream().findFirst();
	}

}
