package br.com.fatecmogidascruzes.agenda.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateLimitMustBeInFutureValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DateLimitMustBeInFuture {

	String message() default "A data de replicação do evento deve ser futura";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}