package br.com.fatecmogidascruzes.agenda.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EndMustBeAfterStartValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EndMustBeAfterStart {

	String message() default "A data de término do evento deve ser posterior à data de início";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}