package br.com.fatecmogidascruzes.agenda.service.web;

import br.com.fatecmogidascruzes.agenda.AgendaEntry;
import br.com.fatecmogidascruzes.agenda.RepetitionPolicy;
import br.com.fatecmogidascruzes.agenda.service.validator.DateLimitMustBeInFuture;
import com.devskiller.friendly_id.FriendlyId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Setter
@DateLimitMustBeInFuture
public class AgendaEntryRepetitionEditDTO {

	private String hashString;

	@NotNull(message = "A data/hora de término das repetições é obrigatória")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateLimit;

	@NotNull(message = "A frequência de repetição é obrigatória")
	private RepetitionPolicy repetition;

	public static AgendaEntryRepetitionEditDTO from(AgendaEntry event) {
		AgendaEntryRepetitionEditDTO eventEditDTO = new AgendaEntryRepetitionEditDTO();

		eventEditDTO.setHashString(FriendlyId.toFriendlyId(event.getHash()));
		return eventEditDTO;
	}

}
