package br.com.fatecmogidascruzes.agenda.service.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Setter
public class CalendarEditDTO {

    @NotNull
    private MultipartFile file;

    private boolean currentlyHasFile;

}
