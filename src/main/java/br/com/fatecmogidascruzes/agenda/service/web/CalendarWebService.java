package br.com.fatecmogidascruzes.agenda.service.web;

import br.com.fatecmogidascruzes.agenda.Calendar;

import java.util.Optional;

public interface CalendarWebService {

	Optional<Calendar> getCalendar();

	void save(CalendarEditDTO agendaCalendarEditDTO);

}
