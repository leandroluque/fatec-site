package br.com.fatecmogidascruzes.agenda.service.web;

import br.com.fatecmogidascruzes.agenda.Calendar;
import br.com.fatecmogidascruzes.agenda.service.CalendarService;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CalendarWebServiceImpl implements CalendarWebService {

	private final CalendarService agendaCalendarService;
	private final FileService fileService;

	@Autowired
	public CalendarWebServiceImpl(CalendarService agendaCalendarService, FileService fileService) {
		this.agendaCalendarService = agendaCalendarService;
		this.fileService = fileService;
	}

	@Override
	public Optional<Calendar> getCalendar() {
		return agendaCalendarService.getCalendar();
	}

	@Override
	public void save(CalendarEditDTO agendaCalendarEditDTO) {
		Optional<Calendar> opCalendar = getCalendar();

		Calendar calendar = opCalendar.orElseGet(() -> new Calendar());

		if (null != agendaCalendarEditDTO.getFile() && !agendaCalendarEditDTO.getFile().isEmpty()) {
			// If the user has specified a calendar but another one exists, delete the old one.
			if (null != calendar.getFile()) {
				fileService.removeByKey(calendar.getFile().getId());
			}

			File newFile = fileService.saveFile(agendaCalendarEditDTO.getFile());
			calendar.setFile(newFile);
		}
		agendaCalendarService.save(calendar);
	}

}
