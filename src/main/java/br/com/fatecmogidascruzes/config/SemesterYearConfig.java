package br.com.fatecmogidascruzes.config;

import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.configuration.ConfigurationEnum;
import br.com.fatecmogidascruzes.configuration.service.ConfigurationService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
public class SemesterYearConfig {

	@Setter @Getter
	private static ConfigurationService configurationService;

	public static Boolean isPreEnrollEnabled() {
		return convertValueToBoolean(ConfigurationEnum.PRE_ENROLL_ENABLED);
	}

	public static Boolean isPreEnrollMenuEnabled() {
		return convertValueToBoolean(ConfigurationEnum.PRE_ENROLL_MENU_ENABLED);
	}

	public static Integer getPreEnrollSemester() {
		return convertValueToInteger(ConfigurationEnum.PRE_ENROLL_SEMESTER);
	}

	public static Integer getPreEnrollYear() {
		return convertValueToInteger(ConfigurationEnum.PRE_ENROLL_YEAR);
	}

	public static Integer getStartAccomodationMonth() {
		return convertValueToInteger(ConfigurationEnum.START_ACCOMMODATION_MONTH);
	}

	public static Integer getStartAccomodationDay() {
		return convertValueToInteger(ConfigurationEnum.START_ACCOMMODATION_DAY);
	}

	public static Integer getEndAccomodationMonth() {
		return convertValueToInteger(ConfigurationEnum.END_ACCOMMODATION_MONTH);
	}

	public static Integer getEndAccomodationDay() {
		return convertValueToInteger(ConfigurationEnum.END_ACCOMMODATION_DAY);
	}

	private static Integer convertValueToInteger(ConfigurationEnum configEnum) {
		String value = getValue(configEnum);
		return Integer.parseInt(value);
	}

	private static Boolean convertValueToBoolean(ConfigurationEnum configEnum) {
		String value = getValue(configEnum);
		return Boolean.parseBoolean(value);
	}

	private static String getValue(ConfigurationEnum enumConf) {
		Configuration value = getConfigurationService().getByEnabledTrueAndName(enumConf.toString()).orElse(null);
		return value != null ? value.getValue() : enumConf.getDefaultValue();
	}
}
