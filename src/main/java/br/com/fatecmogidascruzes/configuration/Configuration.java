package br.com.fatecmogidascruzes.configuration;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "_configuration")
public class Configuration extends NamedEntity {

	private static final long serialVersionUID = 1L;

	public Configuration(String name, String value) {
		super();
		setValue(value);
		setName(name);
	}

	@Basic
	@Column(nullable = false, length = 255)
	private String value;

}