package br.com.fatecmogidascruzes.configuration;

public enum ConfigurationEnum {
    START_ACCOMMODATION_DAY("10"),
    PRE_ENROLL_ENABLED("false"),
    PRE_ENROLL_MENU_ENABLED("false"),
    PRE_ENROLL_SEMESTER("1"),
    PRE_ENROLL_YEAR("2022"),
    START_ACCOMMODATION_MONTH("1"),
    END_ACCOMMODATION_DAY("20"),
    END_ACCOMMODATION_MONTH("1");

    String defaultValue;

    ConfigurationEnum(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
