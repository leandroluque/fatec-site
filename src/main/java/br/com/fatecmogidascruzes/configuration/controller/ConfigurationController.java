package br.com.fatecmogidascruzes.configuration.controller;

import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.configuration.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@PreAuthorize("hasRole('ADMINISTRATIVE')")
@RequestMapping("/admin/configuration")
public class ConfigurationController {

	private ConfigurationService configurationService;

	@Autowired
	public ConfigurationController(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@GetMapping
	public ResponseEntity<Configuration> getConfigValueByFieldName(@RequestParam("name") String name) {
		Configuration configurationResponse =
				configurationService.getByEnabledTrueAndName(name).orElse(null);
		return configurationResponse != null
				? new ResponseEntity<>(configurationResponse, HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<Configuration> createNewConfiguration(@RequestParam("name") String name, @RequestParam("value") String value) {
		Configuration newConfiguration = new Configuration(name, value);
		try {
			newConfiguration = configurationService.upinsert(newConfiguration);
		} catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(newConfiguration, HttpStatus.OK);
	}
}
