package br.com.fatecmogidascruzes.configuration.data;

import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ConfigurationDAO extends DAOImpl<Configuration, Long>, JpaRepository<Configuration, Long> {

	Optional<Configuration> findFirstEnabledTrueByName(String name);
}