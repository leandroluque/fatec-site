package br.com.fatecmogidascruzes.configuration.service;

import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.Optional;

public interface ConfigurationService extends BaseService<Configuration, Long> {

	Optional<Configuration> getByEnabledTrueAndName(String name);

	Configuration upinsert(Configuration configuration);

}
