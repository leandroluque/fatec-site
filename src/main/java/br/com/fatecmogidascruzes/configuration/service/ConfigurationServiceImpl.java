package br.com.fatecmogidascruzes.configuration.service;

import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.configuration.data.ConfigurationDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConfigurationServiceImpl extends BaseServiceImpl<Configuration, Long> implements ConfigurationService {

	private ConfigurationDAO configurationDAO;

	@Autowired
	public ConfigurationServiceImpl(ConfigurationDAO configurationDAO) {
		super(configurationDAO);
		this.configurationDAO = configurationDAO;
	}

	@Override
	public Optional<Configuration> getByEnabledTrueAndName(String name) {
		return configurationDAO.findFirstEnabledTrueByName(name);
	}

	@Override
	public Configuration upinsert(Configuration configuration) {
		Configuration queriedConfiguration = configurationDAO
				.findFirstEnabledTrueByName(configuration.getName()).orElse(configuration);
		if (queriedConfiguration.getId() != null) {
			queriedConfiguration.setValue(configuration.getValue());
			update(queriedConfiguration);
		} else {
			save(queriedConfiguration);
		}
		return queriedConfiguration;
	}
}
