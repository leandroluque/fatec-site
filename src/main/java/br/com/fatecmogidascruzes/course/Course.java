package br.com.fatecmogidascruzes.course;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.file.File;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_course")
public class Course extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "acronym", nullable = false, length = 8)
	private String acronym;

	@Column(name = "shift", nullable = false, length = 30)
	@Enumerated(EnumType.STRING)
	private Shift shift;

	@JoinColumn(name = "current_schedule", nullable = true)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private File currentSchedule;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_course")
	@Id
	@Override
	@SequenceGenerator(name = "seq_course", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}