package br.com.fatecmogidascruzes.course;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Courses {

    ADS("Análise e Desenvolvimento de Sistemas", "ADS"),
    AGRO("Agronegócio", "AGRO"),
    GESTAO("Gestão Empresarial", "GESTAO"),
    LOG("Logística", "LOG"),
    RH("Gestão de Recursos Humanos", "RH");

    private final String name;
    private final String acronym;

    public static Courses getByAcronym(String acronym) {
    	for (Courses course : Courses.values()) {
    		if (course.getAcronym().equals(acronym)) {
    			return course;
    		}
    	}
    	return null;
    }

}
