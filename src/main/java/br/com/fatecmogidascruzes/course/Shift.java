package br.com.fatecmogidascruzes.course;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Shift {

	MORNING("Manhã"), AFTERNOON("Tarde"), NIGHT("Noite");

	private String name;

	public static Shift getShiftByName(String name) {
		for(Shift shift : values()) {
			if (shift.getName().equalsIgnoreCase(name))
				return shift;
		}
		return null;
	}

}
