package br.com.fatecmogidascruzes.course.data;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.data.DAOImpl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CourseDAO extends DAOImpl<Course, Long>, JpaRepository<Course, Long> {

	long countByEnabledTrueAndAcronym(String acronym);

	List<Course> findByEnabledTrueAndAcronym(String acronym);

	Optional<Course> findByEnabledTrueAndNameAndShift(String name, Shift shift);

	Optional<Course> findByEnabledTrueAndAcronymAndShift(String acronym, Shift shift);

}