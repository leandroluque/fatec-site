package br.com.fatecmogidascruzes.course.dto;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor

@Getter
@Setter
public class CourseDTO {

	protected String hashString;
	protected String acronym;
	protected String name;
	protected Shift shift;

	public static CourseDTO from(Course course) {
		CourseDTO dto = new CourseDTO();
		dto.setHashString(FriendlyId.toFriendlyId(course.getHash()));
		dto.setAcronym(course.getAcronym());
		dto.setName(course.getName());
		dto.setShift(course.getShift());

		return dto;
	}

	public static List<CourseDTO> listFrom(Collection<Course> courses) {
		List<CourseDTO> dTOs = new ArrayList<>();
		courses.forEach(disciplineOffer -> dTOs.add(CourseDTO.from(disciplineOffer)));
		return dTOs;
	}

}
