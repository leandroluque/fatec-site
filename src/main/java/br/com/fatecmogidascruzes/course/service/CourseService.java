package br.com.fatecmogidascruzes.course.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.List;
import java.util.Optional;

public interface CourseService extends BaseService<Course, Long> {

	List<Course> findEnabledByAcronym(String acronym);
	
	long countEnabledByAcronym(String acronym);

	Optional<Course> getEnabledByNameAndShift(String name, Shift shift);

	Optional<Course> getEnabledByAcronymAndShift(String acronym, Shift shift);

}
