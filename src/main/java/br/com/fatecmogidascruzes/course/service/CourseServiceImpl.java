package br.com.fatecmogidascruzes.course.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.course.data.CourseDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl extends BaseServiceImpl<Course, Long> implements CourseService {

	private CourseDAO courseDAO;

	@Autowired
	public CourseServiceImpl(CourseDAO courseDAO) {
		super(courseDAO);
		this.courseDAO = courseDAO;
	}

	@Override
	public List<Course> findEnabledByAcronym(String acronym) {
		return courseDAO.findByEnabledTrueAndAcronym(acronym);
	}

	@Override
	public long countEnabledByAcronym(String acronym) {
		return courseDAO.countByEnabledTrueAndAcronym(acronym);
	}

	@Override
	public Optional<Course> getEnabledByNameAndShift(String name, Shift shift) {
		return courseDAO.findByEnabledTrueAndNameAndShift(name, shift);
	}

	@Override
	public Optional<Course> getEnabledByAcronymAndShift(String acronym, Shift shift) {
		return courseDAO.findByEnabledTrueAndAcronymAndShift(acronym, shift);
	}

}
