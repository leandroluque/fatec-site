package br.com.fatecmogidascruzes.discipline;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_discipline")
public class Discipline extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "acronym", nullable = false, length = 8)
	private String acronym;

	@JoinColumn(name = "course")
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Course course;

	@Basic
	@Column(name = "_group", nullable = true, length = 1)
	private String group;

	@Basic
	@Column(name = "semester", nullable = true)
	private int semester;

	@Basic
	@Column(name = "number_of_hours", nullable = true)
	private int hours;

	@JoinTable(name = "_discipline_req", joinColumns = @JoinColumn(name = "discipline"), inverseJoinColumns = @JoinColumn(name = "requirement"))
	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE }, fetch = FetchType.LAZY)
	private Set<Discipline> requirements = new HashSet<>();

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "discipline")
	private Set<DisciplineOffer> offers = new HashSet<>();

	@JoinTable(name = "_discipline_enforcements", joinColumns = @JoinColumn(name = "discipline"), inverseJoinColumns = @JoinColumn(name = "enforced_disc"))
	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE }, fetch = FetchType.LAZY)
	private Set<Discipline> enforcements = new HashSet<>();

	@ManyToMany(mappedBy = "enforcements")
	private Set<Discipline> enforced = new HashSet<>();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_discipline")
	@Id
	@Override
	@SequenceGenerator(name = "seq_discipline", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public Optional<DisciplineOffer> getOfferFor(int semester, int year) {
		return this.offers.stream().filter(dof -> dof.getSemester() == semester && dof.getYear() == year).findAny();
	}

	public void addOffer(DisciplineOffer offer) {
		this.offers.add(offer);
	}

}