package br.com.fatecmogidascruzes.discipline;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.domain.EntityImpl;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.student.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_discipline_offer")
public class DisciplineOffer extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "discipline", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Discipline discipline;

	@JoinTable(name = "_discipline_offer_prof", joinColumns = @JoinColumn(name = "discipline"), inverseJoinColumns = @JoinColumn(name = "professor"))
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Employee professor;

	@Basic
	@Column(name = "semester", nullable = false)
	private int semester;

	@Basic
	@Column(name = "year", nullable = false)
	private int year;

	@Basic
	@Column(name = "capacity", nullable = true)
	private int capacity;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "disciplineOffer")
	private Set<StudentEnrollment> enrollments = new HashSet<>();

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "disciplineOffer")
	private Set<DisciplineOfferClass> disciplineClasses = new HashSet<>();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_discipline_offer")
	@Id
	@Override
	@SequenceGenerator(name = "seq_discipline_offer", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public List<StudentEnrollment> getApprovedEnrollments() {
		List<StudentEnrollment> approvedEnrollments = new ArrayList<>(enrollments);
		Collections.sort(approvedEnrollments, new Comparator<StudentEnrollment>() {

			@Override
			public int compare(StudentEnrollment one, StudentEnrollment other) {

				int disciplineSemester = getDiscipline().getSemester();
				Course course = getDiscipline().getCourse();

				if (one.getStudent().getEntryYearAndSemester() == null) {
					System.out.println(
							"Encontrado um estudante sem semestre/ano de entrada: (" + one.getStudent().getId() + ") "
									+ one.getStudent().getName() + " - " + one.getStudent().getEntryYearAndSemester());
					one.getStudent().setEntryYearAndSemester("20131");

				}
				int oneEntryYear = Integer.valueOf(one.getStudent().getEntryYearAndSemester().substring(0, 4));
				int oneEntrySemester = Integer.valueOf(one.getStudent().getEntryYearAndSemester().substring(4, 5));
				int oneCurrentSemester = (SemesterYearConfig.getPreEnrollYear() - oneEntryYear) * 2
						+ (SemesterYearConfig.getPreEnrollSemester() - oneEntrySemester) + 1;
				Shift oneShift = one.getStudent().getCourse().getShift();
				Course oneCourse = one.getStudent().getCourse();

				if (other.getStudent().getEntryYearAndSemester() == null) {
					other.getStudent().setEntryYearAndSemester("20131");
					System.out.println("Encontrado um outro estudante sem semestre/ano de entrada: ("
							+ other.getStudent().getId() + ") " + other.getStudent().getName() + " - "
							+ other.getStudent().getEntryYearAndSemester());
				}
				int otherEntryYear = Integer.valueOf(other.getStudent().getEntryYearAndSemester().substring(0, 4));
				int otherEntrySemester = Integer.valueOf(other.getStudent().getEntryYearAndSemester().substring(4, 5));
				int otherCurrentSemester = (SemesterYearConfig.getPreEnrollYear() - otherEntryYear) * 2
						+ (SemesterYearConfig.getPreEnrollSemester() - otherEntrySemester) + 1;
				Shift otherShift = other.getStudent().getCourse().getShift();
				Course otherCourse = other.getStudent().getCourse();

				int onePoints = 0;
				int otherPoints = 0;

				// Prioridades: Fase, turno, maior tempo de curso.

				// Rule 1.
				onePoints = (disciplineSemester == oneCurrentSemester) ? 1 : 0;
				otherPoints = (disciplineSemester == otherCurrentSemester) ? 1 : 0;

				if (onePoints > otherPoints) {
					return -1;
				} else if (otherPoints > onePoints) {
					return 1;
				}

				// Rule 2.
				if (oneShift == course.getShift()) {
					if (otherShift == course.getShift()) {
						int difference = otherCurrentSemester - oneCurrentSemester;
						if(0 == difference) {
							return one.getStudent().getName().compareTo(other.getStudent().getName());
						}
						return difference;
					} else {
						return -1;
					}
				} else {
					if (otherShift == course.getShift()) {
						return 1;
					} else {
						int difference = otherCurrentSemester - oneCurrentSemester;
						if(0 == difference) {
							return one.getStudent().getName().compareTo(other.getStudent().getName());
						}
						return difference;
					}
				}
			}

		});

		for (int i = getCapacity(); i < approvedEnrollments.size(); i++) {
			approvedEnrollments.remove(i);
			i--;
		}

		return approvedEnrollments;
	}

	public Optional<StudentEnrollment> getStudentEnrollment(Student student) {
		return this.enrollments.stream()
				.filter(enrollment -> student.getHash().equals(enrollment.getStudent().getHash())).findFirst();
	}

	public StudentEnrollment addStudentEnrollment(Student student) {
		Optional<StudentEnrollment> studentEnrollmentOptional = this.getStudentEnrollment(student);
		if (studentEnrollmentOptional.isPresent()) {
			return studentEnrollmentOptional.get();
		} else {
			StudentEnrollment studentEnrollment = new StudentEnrollment();
			studentEnrollment.setStudent(student);
			this.enrollments.add(studentEnrollment);
			studentEnrollment.setDisciplineOffer(this);
			return studentEnrollment;
		}
	}

	public void removeStudentEnrollment(Student student) {
		this.enrollments.removeIf(en -> en.getStudent().getHashString().equals(student.getHashString()));
	}

	public void addDisciplineOfferClass(DisciplineOfferClass disciplineOfferClass) {
		this.disciplineClasses.add(disciplineOfferClass);
	}

	public int countStudentEnrollments() {
		return this.enrollments.size();
	}

}