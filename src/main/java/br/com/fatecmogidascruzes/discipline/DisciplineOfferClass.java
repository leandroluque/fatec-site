package br.com.fatecmogidascruzes.discipline;

import br.com.fatecmogidascruzes.domain.EntityImpl;
import br.com.fatecmogidascruzes.domain.WeekDay;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_discipline_offer_class")
public class DisciplineOfferClass extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "discipline_offer", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private DisciplineOffer disciplineOffer;

	@Enumerated(EnumType.STRING)
	@Column(name = "week_day", nullable = true)
	private WeekDay weekDay;

	@Enumerated(EnumType.STRING)
	@Column(name = "time_slot", nullable = true)
	private TimeSlot timeSlot;

	@AllArgsConstructor
	@Getter
	public enum TimeSlot {
		// Morning.
		T_0800_0850("08:00 às 08:50"),
		T_0850_0940("08:50 às 09:40"),
		T_1000_1050("10:00 às 10:50"),
		T_1050_1140("10:50 às 11:40"),
		T_1150_1240("11:50 às 12:40"),
		T_1240_1330("12:40 às 13:30"),
		// Afternoon.
		T_1300_1350("13:00 às 13:50"),
		T_1350_1440("13:50 às 14:40"),
		T_1500_1550("15:00 às 15:50"),
		T_1550_1640("15:50 às 16:40"),
		T_1650_1740("16:50 às 17:40"),
		T_1740_1830("17:40 às 18:30"),
		// Night.
		T_1900_1950("19:00 às 19:50"),
		T_1950_2040("19:50 às 20:40"),
		T_2100_2150("21:00 às 21:50"),
		T_2150_2240("21:50 às 22:40");
		
		private String name;
		
	}

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_discipline_offer_class")
	@Id
	@Override
	@SequenceGenerator(name = "seq_discipline_offer_class", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}