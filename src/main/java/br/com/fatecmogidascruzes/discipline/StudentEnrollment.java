package br.com.fatecmogidascruzes.discipline;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.student.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_student_enrollment")
public class StudentEnrollment extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "discipline_offer", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private DisciplineOffer disciplineOffer;

	@JoinColumn(name = "student", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Student student;

	@Basic
	@Column(name = "grade", nullable = true)
	private Double grade;

	@Basic
	@Column(name = "absences", nullable = true)
	private Integer absences;

	@AllArgsConstructor
	@Getter
	public enum Situation {
		AP("Aprovado por Nota e Frequência"), RN("Reprovado por Nota"), RF("Reprovado por Frequência"),
		AE("Dispensa por Aprov. de Estudo"), EP("Dispensado Proficiência"), TM("Trancamento de Matrícula"),
		DM("Desistência de Matrícula"), CM("Cancelamento de Matrícula"), EA("Aprovado Exame Esp. de Avaliação"),
		CR("Cursa Disciplina"), NT("Não sabemos ainda"), RP("Reprovado");

		private String name;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "situation", nullable = true)
	private Situation situation;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_student_enrollment")
	@Id
	@Override
	@SequenceGenerator(name = "seq_student_enrollment", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}