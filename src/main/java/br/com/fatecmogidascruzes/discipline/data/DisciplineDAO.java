package br.com.fatecmogidascruzes.discipline.data;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DisciplineDAO extends DAOImpl<Discipline, Long>, JpaRepository<Discipline, Long> {

	List<Discipline> findByEnabledTrueAndCourse(Course course);

	Optional<Discipline> findByEnabledTrueAndAcronym(String acronym);

	Optional<Discipline> findByEnabledTrueAndAcronymAndCourse(String acronym, Course course);
}