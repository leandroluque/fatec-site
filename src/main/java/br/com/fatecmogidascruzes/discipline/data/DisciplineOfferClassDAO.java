package br.com.fatecmogidascruzes.discipline.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisciplineOfferClassDAO
		extends DAOImpl<DisciplineOfferClass, Long>, JpaRepository<DisciplineOfferClass, Long> {

}