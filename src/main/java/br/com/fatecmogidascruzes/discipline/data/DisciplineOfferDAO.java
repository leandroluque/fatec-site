package br.com.fatecmogidascruzes.discipline.data;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DisciplineOfferDAO extends DAOImpl<DisciplineOffer, Long>, JpaRepository<DisciplineOffer, Long> {

	List<DisciplineOffer> findByEnabledTrueAndSemesterAndYear(int semester, int year);

	@Query("SELECT do FROM DisciplineOffer do LEFT JOIN do.discipline di WHERE true = do.enabled AND :semester = do.semester AND :year = do.year AND :course = di.course")
	List<DisciplineOffer> findByEnabledTrueAndSemesterAndYearAndCourse(
			@Param("semester") int semester,
			@Param("year") int year,
			@Param("course") Course course);

	Optional<DisciplineOffer> findByEnabledTrueAndSemesterAndYearAndDiscipline(
			int semester,
			int year,
			Discipline discipline);

	long countByEnabledTrueAndSemesterAndYear(int semester, int year);

	@Query(nativeQuery = true, value = "SELECT dof.*, dop.* FROM _discipline_offer dof LEFT JOIN _discipline di ON di.id = dof.discipline LEFT JOIN _course co ON co.id = di.course LEFT JOIN _discipline_offer_prof dop ON dop.discipline = dof.id\n"
			+ "WHERE :semester = dof.semester AND :year = dof.year AND"
			+ " di.acronym NOT IN (SELECT d.acronym FROM _discipline_offer do INNER JOIN _discipline d ON do.discipline = d.id WHERE do.id IN (SELECT discipline_offer FROM _student_enrollment WHERE true = enabled AND student=:studentId AND situation IN ('AP','AE', 'EP', 'EA')))"
			+ "			            ORDER BY co.name, co.shift, di.name")
	List<DisciplineOffer> findByEnabledTrueAndAvailableFor(
			@Param("studentId") Long studentId,
			@Param("semester") int semester,
			@Param("year") int year);

	@Query(nativeQuery = true, value = "SELECT dof.*, dop.* FROM _discipline_offer dof LEFT JOIN _discipline di ON di.id = dof.discipline LEFT JOIN _course co ON co.id = di.course LEFT JOIN _discipline_offer_prof dop ON dop.discipline = dof.id\n"
			+ "WHERE :semester = dof.semester AND :year = dof.year AND"
			+ "			(SELECT DISTINCT count(id) FROM _student_enrollment WHERE true = enabled AND student=:studentId AND discipline_offer=dof.id)\n"
			+ "			 > 0\n" + "            ORDER BY co.name, co.shift, di.name")
	List<DisciplineOffer> findByEnabledTrueAndEnrolledBy(
			@Param("studentId") Long studentId,
			@Param("semester") int semester,
			@Param("year") int year);

}