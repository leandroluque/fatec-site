package br.com.fatecmogidascruzes.discipline.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentEnrollmentDAO extends DAOImpl<StudentEnrollment, Long>, JpaRepository<StudentEnrollment, Long> {

	Optional<StudentEnrollment> findByEnabledTrueAndStudentAndDisciplineOffer(
			Student student,
			DisciplineOffer disciplineOffer);

//	@Query("SELECT new br.com.fatecmogidascruzes.discipline.data.StudentEnrollmentDTO(st.hash, st.registry, st.name, cs.acronym, cs.shift) FROM Student st FETCH JOIN st.course cs WHERE true = st.enabled AND true = cs.enabled")
//	Page<StudentEnrollmentDTO> getTable(int semester, int year, Pageable pageable);
//
//	@Query("SELECT count(se.id) FROM StudentEnrollment se LEFT JOIN se.disciplineOffer dof LEFT JOIN se.student st WHERE true = se.enabled AND true = dof.enabled AND :semester = dof.semester AND :year = dof.year AND true = st.enabled AND :registry = st.registry")
//	long countEnrollmentsPerStudent(
//			@Param("semester") int semester,
//			@Param("year") int year,
//			@Param("registry") String registry);
//
//	@AllArgsConstructor
//	@Data
//	public class StudentEnrollmentDTO {
//
//		private UUID hash;
//		private String hashString;
//		private String registry;
//		private String name;
//		private String course;
//		private Shift shift;
//		private int howManyDisciplines;
//
//		public StudentEnrollmentDTO(UUID hash, String registry, String name, String course, Shift shift) {
//			super();
//			this.hash = hash;
//			this.registry = registry;
//			this.name = name;
//			this.course = course;
//			this.shift = shift;
//		}
//
//	}
}