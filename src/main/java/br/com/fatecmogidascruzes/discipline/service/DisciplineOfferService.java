package br.com.fatecmogidascruzes.discipline.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.student.Student;

import java.util.List;
import java.util.Optional;

public interface DisciplineOfferService extends BaseService<DisciplineOffer, Long> {

	List<DisciplineOffer> getEnabledBySemesterAndYear(int semester, int year);

	List<DisciplineOffer> getEnabledBySemesterAndYearAndCourse(int semester, int year, Course course);

	Optional<DisciplineOffer> getEnabledBySemesterAndYearAndDiscipline(int semester, int year, Discipline discipline);

	List<DisciplineOffer> findByEnabledTrueAndAvailableFor(Student student, int semester, int year);

	List<DisciplineOffer> findByEnabledTrueAndEnrolledBy(Student student, int semester, int year);
	
}
