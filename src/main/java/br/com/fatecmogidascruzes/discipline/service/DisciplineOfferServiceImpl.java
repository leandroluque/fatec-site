package br.com.fatecmogidascruzes.discipline.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.data.DisciplineOfferDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplineOfferServiceImpl extends BaseServiceImpl<DisciplineOffer, Long>
		implements DisciplineOfferService {

	private DisciplineOfferDAO disciplineOfferDAO;

	public DisciplineOfferServiceImpl(DisciplineOfferDAO disciplineOfferDAO) {
		super(disciplineOfferDAO);
		this.disciplineOfferDAO = disciplineOfferDAO;
	}

	@Override
	public List<DisciplineOffer> getEnabledBySemesterAndYear(int semester, int year) {
		return disciplineOfferDAO.findByEnabledTrueAndSemesterAndYear(semester, year);
	}

	@Override
	public List<DisciplineOffer> getEnabledBySemesterAndYearAndCourse(int semester, int year, Course course) {
		System.out.println(semester + "/" + year);
		return disciplineOfferDAO.findByEnabledTrueAndSemesterAndYearAndCourse(semester, year, course);
	}

	@Override
	public Optional<DisciplineOffer> getEnabledBySemesterAndYearAndDiscipline(int semester, int year,
			Discipline discipline) {
		return disciplineOfferDAO.findByEnabledTrueAndSemesterAndYearAndDiscipline(semester, year, discipline);
	}

	@Override
	public List<DisciplineOffer> findByEnabledTrueAndAvailableFor(Student student, int semester, int year) {
		return disciplineOfferDAO.findByEnabledTrueAndAvailableFor(student.getId(), semester, year);
	}

	@Override
	public List<DisciplineOffer> findByEnabledTrueAndEnrolledBy(Student student, int semester, int year) {
		return disciplineOfferDAO.findByEnabledTrueAndEnrolledBy(student.getId(), semester, year);
	}

}
