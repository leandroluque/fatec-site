package br.com.fatecmogidascruzes.discipline.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.List;
import java.util.Optional;

public interface DisciplineService extends BaseService<Discipline, Long> {

	List<Discipline> getEnabledByCourse(Course course);

	Optional<Discipline> getEnabledByAcronym(String acronym);

	Optional<Discipline> getEnabledByAcronymAndCourse(String acronym, Course course);

}
