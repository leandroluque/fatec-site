package br.com.fatecmogidascruzes.discipline.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.data.DisciplineDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplineServiceImpl extends BaseServiceImpl<Discipline, Long> implements DisciplineService {

	private DisciplineDAO disciplineDAO;

	public DisciplineServiceImpl(DisciplineDAO disciplineDAO) {
		super(disciplineDAO);
		this.disciplineDAO = disciplineDAO;
	}

	@Override
	public List<Discipline> getEnabledByCourse(Course course) {
		return disciplineDAO.findByEnabledTrueAndCourse(course);
	}

	@Override
	public Optional<Discipline> getEnabledByAcronym(String acronym) {
		return disciplineDAO.findByEnabledTrueAndAcronym(acronym);
	}
	
	@Override
	public Optional<Discipline> getEnabledByAcronymAndCourse(String acronym, Course course) {
		return disciplineDAO.findByEnabledTrueAndAcronymAndCourse(acronym, course);
	}

}
