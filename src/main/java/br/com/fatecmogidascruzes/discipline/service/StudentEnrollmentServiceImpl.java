package br.com.fatecmogidascruzes.discipline.service;

import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.discipline.data.StudentEnrollmentDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class StudentEnrollmentServiceImpl extends BaseServiceImpl<StudentEnrollment, Long>
        implements StudentEnrollmentService {

    private StudentEnrollmentDAO enrollmentDAO;

    public StudentEnrollmentServiceImpl(StudentEnrollmentDAO enrollmentDAO) {
        super(enrollmentDAO);
        this.enrollmentDAO = enrollmentDAO;
    }

}
