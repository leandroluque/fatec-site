package br.com.fatecmogidascruzes.discipline.service.web;

import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.data.DisciplineDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.transaction.Transactional;
import java.util.Optional;

@Controller
@RequestMapping("/admin/test")
public class DisciplineControllerTest {

	private final DisciplineDAO disciplineDAO;

	@Autowired
	public DisciplineControllerTest(DisciplineDAO disciplineDAO) {
		super();
		this.disciplineDAO = disciplineDAO;
	}

	@RequestMapping
	@Transactional
	public @ResponseBody String test() {

		Optional<Discipline> disc = disciplineDAO.findById(10L);
		if (disc.isPresent()) {
			return String.valueOf(disc.get().getOffers().size());
		}

		return "Nao achei";

	}

	@RequestMapping("/1")
	@Transactional
	public @ResponseBody String test1() {

		Optional<Discipline> disc = disciplineDAO.findById(10L);
		if (disc.isPresent()) {
			Discipline dis = disc.get();
			DisciplineOffer dof = new DisciplineOffer();
			dof.setDiscipline(dis);
			dis.addOffer(dof);
			return String.valueOf(disc.get().getOffers().size());
		}

		return "Nao achei";

	}

}
