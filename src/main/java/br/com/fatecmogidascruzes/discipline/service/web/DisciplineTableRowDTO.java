package br.com.fatecmogidascruzes.discipline.service.web;

import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class DisciplineTableRowDTO {

	protected String hashString;
	protected String course;
	protected String name;
	protected String professor;
	protected String when;

	public static DisciplineTableRowDTO from(DisciplineOffer disciplineOffer) {
		DisciplineTableRowDTO dto = new DisciplineTableRowDTO();
		dto.setHashString(FriendlyId.toFriendlyId(disciplineOffer.getHash()));
		dto.setName(disciplineOffer.getDiscipline().getName());
		if (null != disciplineOffer.getProfessor()) {
			dto.setProfessor(disciplineOffer.getProfessor().getName());
		} else {
			dto.setProfessor("");
		}
		dto.setCourse(disciplineOffer.getDiscipline().getCourse().getName());
		// dto.setWhen(disciplineOffer.getWeekDay() + " : " +
		// disciplineOffer.getTimeSlot());

		return dto;
	}

	public static List<DisciplineTableRowDTO> listFrom(List<DisciplineOffer> disciplinesOffer) {
		List<DisciplineTableRowDTO> dTOs = new ArrayList<>();
		disciplinesOffer.forEach(disciplineOffer -> dTOs.add(DisciplineTableRowDTO.from(disciplineOffer)));
		return dTOs;
	}
}
