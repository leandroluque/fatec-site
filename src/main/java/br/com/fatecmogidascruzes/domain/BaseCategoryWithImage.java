package br.com.fatecmogidascruzes.domain;

import br.com.fatecmogidascruzes.file.File;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@MappedSuperclass
@NoArgsConstructor
@Setter
public class BaseCategoryWithImage extends BaseCategory {

	protected static final long serialVersionUID = 1L;

	@JoinColumn(name = "image", nullable = false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private File image;

	@Basic
	@Column(name = "alt_desc", nullable = true, length = 100)
	protected String imageAlternativeDescription;

}
