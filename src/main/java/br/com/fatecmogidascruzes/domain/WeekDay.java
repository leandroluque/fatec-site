package br.com.fatecmogidascruzes.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WeekDay {

	MONDAY("Segunda-feira"), TUESDAY("Terça-feira"), WEDNESDAY("Quarta-feira"), THURSDAY("Quinta-feira"), FRIDAY("Sexta-feira"), SATURDAY("Sábado");

	private String name;

}