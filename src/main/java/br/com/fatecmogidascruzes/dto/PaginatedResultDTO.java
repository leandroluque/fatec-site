package br.com.fatecmogidascruzes.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PaginatedResultDTO<T> {

	private int numberOfPages;
	private int currentPage;
	private List<T> data = new ArrayList<>();

}
