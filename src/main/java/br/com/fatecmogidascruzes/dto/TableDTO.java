package br.com.fatecmogidascruzes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class TableDTO<T> {

	private Integer recordsTotal;
	private Integer recordsFiltered;
	private Integer draw;
	private Collection<T> data = new ArrayList<T>();

	public void add(T row) {
		this.data.add(row);
	}

}
