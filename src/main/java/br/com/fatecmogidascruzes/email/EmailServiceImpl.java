package br.com.fatecmogidascruzes.email;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

	@Value("${email.from}") @Getter
	private String from;
	@Value("${email.to}") @Getter
	private String to;

	private JavaMailSender emailSender;

	@Autowired
	public EmailServiceImpl(JavaMailSender emailSender) {
		this.emailSender = emailSender;
	}

	private void sendEmail(String from, String to, String subject, String content) {
		new Thread(() -> {
			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message);

			try {
				helper.setFrom(from);
				helper.setTo(to.split(","));
				helper.setSubject(subject);
				helper.setText(content, true);

				emailSender.send(message);
			} catch (MessagingException e) {
				log.error("Fail sending email via Gmail SMTP account", e);
			}
		}).start();
	}

	@Override
	public void sendRecoveryPasswordEMail(String to, String hash)
			throws SAXException, IOException, ParserConfigurationException, MessagingException {

		String content = "<!DOCTYPE HTML>" + "<html lang=\"pt\">" + "  <head>"
				+ "    <meta charset=\"utf-8\" />" + "  </head>" + "  <body bgcolor='#F1F1F1'>"
				+ "	<div style=\"background-color: white; margin: 0 auto 0 auto; padding: 1em; text-align: center;\">"
				+ "	  <p>Você solicitou a recuperação da sua senha de acesso ao site da Fatec Mogi das Cruzes.<br /><a href=\"https://fatecmogidascruzes.com.br/changePassword?hash="
				+ hash
				+ "\">Clique aqui para alterar a sua senha.</a><br /><em>Este link estará disponível por 24 horas.</em><br /><br />Caso você não tenha solicitado a alteração de senha, apenas desconsidere este e-mail.</p>"
				+ "	  <br />" + "	  <footer>2018 - Fatec Mogi das Cruzes</footer>" + "	</div>" + ""
				+ "</body>" + "</html>";
		sendEmail(getFrom(), to, "Recuperação de Senha - Fatec Mogi das Cruzes", content);
	}

	@Override
	public void sendContactEmail(String from, String phone, String name, String messageContent)
			throws SAXException, IOException, ParserConfigurationException, MessagingException {

		String content = "<!DOCTYPE HTML>" + "<html lang=\"pt\">" + "  <head>"
				+ "    <meta charset=\"utf-8\" />" + "  </head>" + "  <body bgcolor='#F1F1F1'>"
				+ "	<div style=\"background-color: white; margin: 0 auto 0 auto; padding: 1em; text-align: center;\">"
				+ "	  <p>Um contato foi recebido pelo site da Fatec Mogi das Cruzes:</p>" + "	  <p>Nome: " + name
				+ "</p>" + "	  <p>E-mail: " + from + "</p>" + "	  <p>Telefone: " + phone + "</p>"
				+ "	  <p>Mensagem: " + messageContent + "</p>"
				+ "	  <footer>2018 - Fatec Mogi das Cruzes</footer>" + "	</div>" + "" + "</body>"
				+ "</html>";

		sendEmail(getFrom(), getTo(), "Contato via site - Fatec Mogi das Cruzes", content);

	}

	@Override
	public void sendRequestEMail(String email, String name, String protocol) throws SAXException, IOException, ParserConfigurationException, MessagingException {

		String content = "<!DOCTYPE HTML>" + "<html lang=\"pt\">" + "  <head>"
				+ "    <meta charset=\"utf-8\" />" + "  </head>" + "  <body bgcolor='#F1F1F1'>"
				+ "	<div style=\"background-color: white; margin: 0 auto 0 auto; padding: 1em; text-align: center;\">"
				+ "	  <p>" + name
				+ ", você solicitou um documento por meio do site da Fatec Mogi das Cruzes. Estamos enviando este e-mail para lhe informar sobre a situação da sua solicitação.</p>"
				+ "	  <p>Protocolo: " + protocol + "</p>"
				+ "	  <footer>2018 - Fatec Mogi das Cruzes</footer>" + "	</div>" + "" + "</body>"
				+ "</html>";

		sendEmail(getFrom(), email, "Solicitação de Documento - Fatec Mogi das Cruzes", content);

	}

}
