package br.com.fatecmogidascruzes.employee;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.file.File;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Entity
@Setter
@Table(name = "_employee", indexes = { @Index(name = "ind_employee_name", columnList = "name", unique = false) })
public class Employee extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "name_in_siga", nullable = false, length = 100)
	private String nameInSIGA;

	@JoinColumn(name = "image", nullable = true)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private File image;

	@AllArgsConstructor
	@Getter
	public enum EducationLevel {

		TECNICO("Nível Técnico", ""), SUPERIOR("Superior", ""), ESPECIALIZACAO("Especialização", "Esp."),
		MESTRADO("Mestrado", "Me."), DOUTORADO("Doutorado", "Dr.");

		private String name;
		private String titulation;

	}

	@Getter
	public enum SelectionType {

		CONFIANCA("Confiança"), PROFESSOR("Concursado como Professor"), PROFESSOR_SELECIONADO("Professor Temporário"),
		AGENTE_TECNICO_ADMINISTRATIVO("Concursado como Agente Técnico Administrativo"),
		ANALISTA_SUPORTE_GESTAO("Concursado como Analista de Suporte e Gestão"),
		AUXILIAR_DOCENTE("Concursado como Auxiliar Docente");

		private String name;

		private SelectionType(String name) {
			this.name = name;
		}

	}

	@Getter
	public enum Allocation {

		NENHUMA("Nenhuma"), COORDENADOR("Coordenador"), DIRETOR("Diretor"), DIRETOR_ACADEMICO("Diretor Acadêmico"),
		DIRETOR_ADMINISTRATIVO("Diretor Administrativo"),
		ASSISTENTE_TECNICO_ADMINISTRATIVO("Assistente Técnico Administrativo"),
		ASSISTENTE_ADMINISTRATIVO("Assistente Administrativo");

		private String name;

		private Allocation(String name) {
			this.name = name;
		}

	}

	@AllArgsConstructor
	@Getter
	public enum Level {

		I("Ensino Superior I"), II("Ensino Superior II"), III("Ensino Superior III");

		private String name;

	}

	@Column(name = "allocation", nullable = true)
	@Enumerated(EnumType.STRING)
	private Allocation allocation;
	private Boolean removed;
	@Enumerated(EnumType.STRING)
	private Courses coordinatorCourse;

	@Column(name = "selection_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private SelectionType selectionType;

	@Column(name = "education_level", nullable = true)
	@Enumerated(EnumType.STRING)
	private EducationLevel educationLevel;

	@Column(name = "level", nullable = true)
	@Enumerated(EnumType.STRING)
	private Level level;

	@Column(name = "congregation_member", nullable = true)
    private Boolean congregationMember;
	@ElementCollection(targetClass = Courses.class)
	@JoinTable(name = "_congregation_courses", joinColumns = @JoinColumn(name = "employee"))
	@Column(name = "course")
	@Enumerated(EnumType.STRING)
	private Set<Courses> congregationCourses = new HashSet<>();
	private Boolean internshipReponsible;
	@ElementCollection(targetClass = Courses.class)
	@JoinTable(name = "_internship_responsibility_courses", joinColumns = @JoinColumn(name = "employee"))
	@Column(name = "course")
	@Enumerated(EnumType.STRING)
	private Set<Courses> internshipResponsibilityCourses = new HashSet<>();

	@Basic
	@Column(name = "pro_tempore", nullable = true)
	private boolean proTempore;

	@Column(name = "curriculum")
	@Lob
	private String curriculum;

	@Basic
	@Column(name = "lattes", nullable = true, length = 100)
	private String lattes;

	@Basic
	@Column(name = "homepage", nullable = true, length = 100)
	private String homepage;

	@ElementCollection(targetClass = Courses.class)
	@JoinTable(name = "_employee_courses", joinColumns = @JoinColumn(name = "employee"))
	@Column(name = "course")
	@Enumerated(EnumType.STRING)
	private Set<Courses> courses = new HashSet<>();

	private String email;
	private String teams;
	private String workingHours;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_employee")
	@Id
	@Override
	@SequenceGenerator(name = "seq_employee", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public boolean isAllocatedIn(Course course) {
		return this.courses.stream().filter(c -> course.getAcronym().equals(c.getAcronym())).count() > 0;
	}

	public boolean isAllocatedIn(String courseAcronym) {
		return this.courses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).count() > 0;
	}

	public boolean isCoordinatorOf(String courseAcronym) {
		return allocation == Allocation.COORDENADOR && coordinatorCourse != null && courseAcronym.equals(coordinatorCourse.getAcronym());
	}

	public boolean isMemberOfCongregationOf(String courseAcronym) {
		return this.congregationMember != null && this.congregationMember == true && this.congregationCourses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).count() > 0;
	}
	public boolean isResponsibleForTheInternshipOf(String courseAcronym) {
		return this.internshipReponsible != null && this.internshipReponsible == true && this.internshipResponsibilityCourses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).count() > 0;
	}
	public void addCourse(Courses course) {
		this.courses.add(course);
	}

	public void addCourses(List<Courses> courses) {
		this.courses.addAll(courses);
	}

	public void removeCourses(String courseAcronym) {
		List<Courses> coursesToRemove = new ArrayList<>();
		coursesToRemove.addAll(
				this.courses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).collect(Collectors.toList()));
		this.courses.removeAll(coursesToRemove);
	}

	public void addInternshipCourse(Courses course) {
		this.internshipResponsibilityCourses.add(course);
	}
	public void addInternshipCourses(List<Courses> courses) {
		this.internshipResponsibilityCourses.addAll(courses);
	}

	public void removeInternshipCourses(String courseAcronym) {
		List<Courses> coursesToRemove = new ArrayList<>();
		coursesToRemove.addAll(
				this.internshipResponsibilityCourses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).collect(Collectors.toList()));
		this.internshipResponsibilityCourses.removeAll(coursesToRemove);
	}

	public void addCongregationCourse(Courses course) {
		this.congregationCourses.add(course);
	}
	public void addCongregationCourses(List<Courses> courses) {
		this.congregationCourses.addAll(courses);
	}

	public void removeCongregationCourses(String courseAcronym) {
		List<Courses> coursesToRemove = new ArrayList<>();
		coursesToRemove.addAll(
				this.congregationCourses.stream().filter(c -> courseAcronym.equals(c.getAcronym())).collect(Collectors.toList()));
		this.congregationCourses.removeAll(coursesToRemove);
	}

}