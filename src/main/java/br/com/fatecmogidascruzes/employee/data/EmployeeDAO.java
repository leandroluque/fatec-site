package br.com.fatecmogidascruzes.employee.data;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.employee.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EmployeeDAO extends DAOImpl<Employee, Long>, JpaRepository<Employee, Long> {

	@Query("SELECT em FROM #{#entityName} em WHERE TRUE = em.enabled AND (UPPER(em.name) LIKE CONCAT('%',:filter,'%') OR UPPER(em.curriculum) LIKE CONCAT('%',:filter,'%'))")
	Page<Employee> getEnabledByFilter(@Param("filter") String filter, Pageable pageable);

	@Query("SELECT em FROM #{#entityName} em WHERE TRUE = em.enabled AND ('PROFESSOR' = em.selectionType OR 'PROFESSOR_SELECIONADO' = em.selectionType) ORDER BY em.name")
	List<Employee> getEnabledProfessors();

	@Query("SELECT distinct(em) FROM #{#entityName} em WHERE TRUE = em.enabled AND :course MEMBER OF em.courses AND ('PROFESSOR' = em.selectionType OR 'PROFESSOR_SELECIONADO' = em.selectionType) ORDER BY em.name")
	List<Employee> getEnabledProfessorsByCourse(@Param("course") Courses course);

	@Query("SELECT distinct(em) FROM #{#entityName} em WHERE TRUE = em.enabled AND em.congregationMember = TRUE ORDER BY em.level")
	List<Employee> getEnabledMembersOfCongregation();

	@Query("SELECT distinct(em) FROM #{#entityName} em WHERE TRUE = em.enabled AND (:allocation = em.allocation) ORDER BY em.name")
	List<Employee> getEnabledByAllocation(@Param("allocation") Employee.Allocation allocation);

	Optional<Employee> getByEnabledTrueAndNameInSIGA(String name);
	
	Optional<Employee> getByNameInSIGA(String name);

	List<Employee> getDistinctByEnabledTrueAndInternshipReponsibleTrue();

}