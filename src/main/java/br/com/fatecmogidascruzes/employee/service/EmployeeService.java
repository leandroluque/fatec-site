package br.com.fatecmogidascruzes.employee.service;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.service.BaseService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface EmployeeService extends BaseService<Employee, Long> {

	Page<Employee> getEnabledByFilter(SearchCriteria searchCriteria);

	List<Employee> getEnabledProfessorsByCourse(Courses course);

    List<Employee> getEnabledByAllocation(Employee.Allocation allocation);

    List<Employee> getEnabledCongregationMembers();

    List<Employee> getEnabledProfessorsByCourseAcronym(String acronym);

	List<Employee> getEnabledProfessors();

	Optional<Employee> getEnabledBySIGAInName(String nameInSIGA);
	
	Optional<Employee> getBySIGAInName(String nameInSIGA);

	List<Employee> getEnabledInternshipResponsibles();

}
