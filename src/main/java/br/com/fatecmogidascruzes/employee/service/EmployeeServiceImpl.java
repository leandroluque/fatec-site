package br.com.fatecmogidascruzes.employee.service;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.data.EmployeeDAO;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee, Long> implements EmployeeService {

	private EmployeeDAO employeeDAO;

	public EmployeeServiceImpl(EmployeeDAO employeeDAO) {
		super(employeeDAO);
		this.employeeDAO = employeeDAO;
	}

	@Override
	public Page<Employee> getEnabledByFilter(SearchCriteria searchCriteria) {
		return employeeDAO.getEnabledByFilter(searchCriteria.getFilter(), prepareCriteria(searchCriteria));
	}

	@Override
	public List<Employee> getEnabledProfessors() {
		return employeeDAO.getEnabledProfessors();
	}

	public List<Employee> getEnabledProfessorsByCourse(Courses course) {
		return this.employeeDAO.getEnabledProfessorsByCourse(course);
	}

	@Override
	public List<Employee> getEnabledByAllocation(Employee.Allocation allocation) {
		return this.employeeDAO.getEnabledByAllocation(allocation);
	}

	@Override
	public List<Employee> getEnabledCongregationMembers() {
		return this.employeeDAO.getEnabledMembersOfCongregation();
	}

	public List<Employee> getEnabledProfessorsByCourseAcronym(String acronym) {
		return this.employeeDAO.getEnabledProfessorsByCourse(Courses.getByAcronym(acronym));
	}

	public List<Employee> getEnabledInternshipResponsibles() {
		return this.employeeDAO.getDistinctByEnabledTrueAndInternshipReponsibleTrue();
	}

	@Override
	public Optional<Employee> getEnabledBySIGAInName(String nameInSIGA) {
		return employeeDAO.getByEnabledTrueAndNameInSIGA(nameInSIGA);
	}

	@Override
	public Optional<Employee> getBySIGAInName(String nameInSIGA) {
		return employeeDAO.getByNameInSIGA(nameInSIGA);
	}

}
