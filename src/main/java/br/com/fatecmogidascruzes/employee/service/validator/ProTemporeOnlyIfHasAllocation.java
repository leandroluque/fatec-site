package br.com.fatecmogidascruzes.employee.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ProTemporeOnlyIfHasAllocationValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ProTemporeOnlyIfHasAllocation {

	String message() default "O funcionário só deve ser marcado como pro tempore se tiver alocado em alguma função";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}