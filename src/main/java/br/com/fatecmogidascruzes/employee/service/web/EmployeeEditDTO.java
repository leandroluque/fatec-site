package br.com.fatecmogidascruzes.employee.service.web;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.Employee.Allocation;
import br.com.fatecmogidascruzes.employee.Employee.EducationLevel;
import br.com.fatecmogidascruzes.employee.Employee.SelectionType;
import br.com.fatecmogidascruzes.employee.service.validator.EmployeeImageMustHaveDescription;
import br.com.fatecmogidascruzes.employee.service.validator.EmployeeMustHaveImage;
import br.com.fatecmogidascruzes.employee.service.validator.ProTemporeOnlyIfHasAllocation;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ProTemporeOnlyIfHasAllocation
@EmployeeMustHaveImage
@EmployeeImageMustHaveDescription
public class EmployeeEditDTO {

	private String hashString;
	@NotBlank(message = "O nome do funcionário é obrigatório")
	@Length(max = 100, message = "O nome do funcionário não pode exceder 100 caracteres")
	private String name;
	@Length(max = 2000, message = "O currículo do funcionário não pode exceder 2000 caracteres")
	private String curriculum;
	@NotNull(message = "O tipo de contrataçao do funcionário é obrigatório")
	private SelectionType selectionType;
	@NotNull(message = "O maior nível educacional do funcionário é obrigatório")
	private EducationLevel educationLevel;
	private Allocation allocation;
	private boolean proTempore;
	private MultipartFile image;
	private String imageAlternativeDescription;
	private String lattes;
	private String homepage;
	private List<Courses> courses;
	private boolean ads, agro, rh, log, gestao;

	private boolean internshipResponsible;
	private boolean adsInternship, agroInternship, rhInternship, logInternship, gestaoInternship;
	private boolean congregationMember;
	private boolean adsCongregation, agroCongregation, rhCongregation, logCongregation, gestaoCongregation;
	private boolean adsCoordinator, agroCoordinator, rhCoordinator, logCoordinator, gestaoCoordinator;

	private String email;
	private String teams;
	private String workingHours;

	private Employee.Level level;

	public static EmployeeEditDTO from(Employee employee) {
		EmployeeEditDTO employeeEditDTO = new EmployeeEditDTO();
		employeeEditDTO.setHashString(FriendlyId.toFriendlyId(employee.getHash()));
		employeeEditDTO.setName(employee.getName());
		employeeEditDTO.setSelectionType(employee.getSelectionType());
		employeeEditDTO.setEducationLevel(employee.getEducationLevel());
		employeeEditDTO.setAllocation(employee.getAllocation());
		employeeEditDTO.setProTempore(employee.isProTempore());
		employeeEditDTO.setCourses(new ArrayList<>(employee.getCourses()));
		employeeEditDTO.setCurriculum(employee.getCurriculum());
		employeeEditDTO.setLattes(employee.getLattes());
		employeeEditDTO.setHomepage(employee.getHomepage());
		
		employeeEditDTO.setAds(employee.isAllocatedIn("ADS"));
		employeeEditDTO.setAgro(employee.isAllocatedIn("AGRO"));
		employeeEditDTO.setGestao(employee.isAllocatedIn("GESTAO"));
		employeeEditDTO.setLog(employee.isAllocatedIn("LOG"));
		employeeEditDTO.setRh(employee.isAllocatedIn("RH"));

		employeeEditDTO.setAdsCoordinator(employee.isCoordinatorOf("ADS"));
		employeeEditDTO.setAgroCoordinator(employee.isCoordinatorOf("AGRO"));
		employeeEditDTO.setGestaoCoordinator(employee.isCoordinatorOf("GESTAO"));
		employeeEditDTO.setLogCoordinator(employee.isCoordinatorOf("LOG"));
		employeeEditDTO.setRhCoordinator(employee.isCoordinatorOf("RH"));

		employeeEditDTO.setCongregationMember(employee.getCongregationMember() != null && employee.getCongregationMember() == true);
		employeeEditDTO.setAdsCongregation(employee.isMemberOfCongregationOf("ADS"));
		employeeEditDTO.setAgroCongregation(employee.isMemberOfCongregationOf("AGRO"));
		employeeEditDTO.setGestaoCongregation(employee.isMemberOfCongregationOf("GESTAO"));
		employeeEditDTO.setLogCongregation(employee.isMemberOfCongregationOf("LOG"));
		employeeEditDTO.setRhCongregation(employee.isMemberOfCongregationOf("RH"));

		employeeEditDTO.setInternshipResponsible(employee.getInternshipReponsible() != null && employee.getInternshipReponsible() == true);
		employeeEditDTO.setAdsInternship(employee.isResponsibleForTheInternshipOf("ADS"));
		employeeEditDTO.setAgroInternship(employee.isResponsibleForTheInternshipOf("AGRO"));
		employeeEditDTO.setGestaoInternship(employee.isResponsibleForTheInternshipOf("GESTAO"));
		employeeEditDTO.setLogInternship(employee.isResponsibleForTheInternshipOf("LOG"));
		employeeEditDTO.setRhInternship(employee.isResponsibleForTheInternshipOf("RH"));

		employeeEditDTO.setEmail(employee.getEmail());
		employeeEditDTO.setTeams(employee.getTeams());
		employeeEditDTO.setWorkingHours(employee.getWorkingHours());
		employeeEditDTO.setLevel(employee.getLevel());

		if (null != employee.getImage()) {
			employeeEditDTO.setImageAlternativeDescription(employee.getImage().getAlternativeDescription());
		}

		return employeeEditDTO;
	}

	public void fill(Employee employee) {
		employee.setName(name);
		employee.setSelectionType(selectionType);
		employee.setEducationLevel(educationLevel);
		employee.setAllocation(allocation);
		employee.setProTempore(proTempore);
		employee.setCurriculum(curriculum);
		employee.setLattes(lattes);
		employee.setHomepage(homepage);
		employee.setEmail(email);
		employee.setTeams(teams);
		employee.setWorkingHours(workingHours);
		employee.setInternshipReponsible(internshipResponsible);
		employee.setCongregationMember(congregationMember);
		employee.setLevel(level);
	}

}
