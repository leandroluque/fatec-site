package br.com.fatecmogidascruzes.employee.service.web;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;
import br.com.fatecmogidascruzes.exception.web.BadRequestException;
import br.com.fatecmogidascruzes.exception.web.ForbiddenException;
import br.com.fatecmogidascruzes.exception.web.InternalErrorException;

import java.util.List;

public interface EmployeeWebService {

	void save(EmployeeEditDTO expenseEditDTO) throws BadRequestException, ForbiddenException, InternalErrorException;

	TableDTO<EmployeeTableRowDTO> getTable(SearchCriteria searchCriteria, int draw);

	List<EmployeeEntryDTO> getEnabledProfessors();

	List<EmployeeEntryDTO> getEnabledByCourse(Courses course);
	
	List<EmployeeEntryDTO> getEnabledByCourseAcronym(String acronym);

}
