package br.com.fatecmogidascruzes.employee.service.web;

import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.service.EmployeeService;
import br.com.fatecmogidascruzes.exception.web.BadRequestException;
import br.com.fatecmogidascruzes.exception.web.ForbiddenException;
import br.com.fatecmogidascruzes.exception.web.InternalErrorException;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeWebServiceImpl implements EmployeeWebService {

	private final EmployeeService employeeService;
	private final FileService fileService;
	private final CourseService courseService;

	@Autowired
	public EmployeeWebServiceImpl(EmployeeService employeeService, FileService fileService,
			CourseService courseService) {
		super();
		this.employeeService = employeeService;
		this.fileService = fileService;
		this.courseService = courseService;
	}

	@Override
	public void save(EmployeeEditDTO employeeEditDTO)
			throws BadRequestException, ForbiddenException, InternalErrorException {
		Employee employee = new Employee();
		if (null != employeeEditDTO.getHashString() && !employeeEditDTO.getHashString().trim().isEmpty()) {
			Optional<Employee> categoryOptional = this.employeeService
					.getEnabledByHash(FriendlyId.toUuid(employeeEditDTO.getHashString()));
			if (!categoryOptional.isPresent() || !categoryOptional.get().isEnabled()) {
				throw new BadRequestException(
						"The specified employee does not exist or is disabled and then cannot be updated");
			}
			employee = categoryOptional.get();
		}
		employeeEditDTO.fill(employee);

		if (null != employeeEditDTO.getImage()) {
			if (!employeeEditDTO.getImage().isEmpty()) {
				// If the user has specified an image but another one exists, delete the old
				// one.
				if (null != employee.getImage()) {
					fileService.removeByKey(employee.getImage().getId());
				}

				try {
					File newImage = fileService.saveImage(employeeEditDTO.getImage(),
							employeeEditDTO.getImageAlternativeDescription());
					employee.setImage(newImage);
				} catch (IOException error) {
					error.printStackTrace();
					throw new InternalErrorException("An error happened while trying to save the employee image");
				}
			}

			employee.getImage().setAlternativeDescription(employeeEditDTO.getImageAlternativeDescription());
		}

		// Employee internship courses.

		if (employeeEditDTO.isAdsInternship()) {
			employee.addInternshipCourse(Courses.ADS);
		} else {
			employee.removeInternshipCourses("ADS");
		}

		if (employeeEditDTO.isRhInternship()) {
			employee.addInternshipCourse(Courses.RH);
		} else {
			employee.removeInternshipCourses("RH");
		}

		if (employeeEditDTO.isLogInternship()) {
			employee.addInternshipCourse(Courses.LOG);
		} else {
			employee.removeInternshipCourses("LOG");
		}

		if (employeeEditDTO.isAgroInternship()) {
			employee.addInternshipCourse(Courses.AGRO);
		} else {
			employee.removeInternshipCourses("AGRO");
		}

		if (employeeEditDTO.isGestaoInternship()) {
			employee.addInternshipCourse(Courses.GESTAO);
		} else {
			employee.removeInternshipCourses("GESTAO");
		}

		// Employee congregation courses.

		if (employeeEditDTO.isAdsCongregation()) {
			employee.addCongregationCourse(Courses.ADS);
		} else {
			employee.removeCongregationCourses("ADS");
		}

		if (employeeEditDTO.isRhCongregation()) {
			employee.addCongregationCourse(Courses.RH);
		} else {
			employee.removeCongregationCourses("RH");
		}

		if (employeeEditDTO.isLogCongregation()) {
			employee.addCongregationCourse(Courses.LOG);
		} else {
			employee.removeCongregationCourses("LOG");
		}

		if (employeeEditDTO.isAgroCongregation()) {
			employee.addCongregationCourse(Courses.AGRO);
		} else {
			employee.removeCongregationCourses("AGRO");
		}

		if (employeeEditDTO.isGestaoCongregation()) {
			employee.addCongregationCourse(Courses.GESTAO);
		} else {
			employee.removeCongregationCourses("GESTAO");
		}

		// Employee coordinator courses.

		if (employeeEditDTO.isAdsCoordinator()) {
			employee.setCoordinatorCourse(Courses.ADS);
		} else if (employeeEditDTO.isRhCoordinator()) {
			employee.setCoordinatorCourse(Courses.RH);
		} else if (employeeEditDTO.isLogCoordinator()) {
			employee.setCoordinatorCourse(Courses.LOG);
		} else if (employeeEditDTO.isAgroCoordinator()) {
			employee.setCoordinatorCourse(Courses.AGRO);
		} else if (employeeEditDTO.isGestaoCoordinator()) {
			employee.setCoordinatorCourse(Courses.GESTAO);
		} else {
			employee.setCoordinatorCourse(null);
		}

		// Employee courses.

		if (employeeEditDTO.isAds()) {
			employee.addCourse(Courses.ADS);
		} else {
			employee.removeCourses("ADS");
		}

		if (employeeEditDTO.isRh()) {
			employee.addCourse(Courses.RH);
		} else {
			employee.removeCourses("RH");
		}

		if (employeeEditDTO.isLog()) {
			employee.addCourse(Courses.LOG);
		} else {
			employee.removeCourses("LOG");
		}

		if (employeeEditDTO.isAgro()) {
			employee.addCourse(Courses.AGRO);
		} else {
			employee.removeCourses("AGRO");
		}

		if (employeeEditDTO.isGestao()) {
			employee.addCourse(Courses.GESTAO);
		} else {
			employee.removeCourses("GESTAO");
		}

		this.employeeService.save(employee);
	}

	@Override
	public TableDTO<EmployeeTableRowDTO> getTable(SearchCriteria searchCriteria, int draw) {

		Page<Employee> categoriesPage = this.employeeService.getEnabledByFilter(searchCriteria);
		TableDTO<EmployeeTableRowDTO> table = new TableDTO<>();
		table.setDraw(draw);
		table.setRecordsTotal((int) employeeService.countEnabled().longValue());
		table.setRecordsFiltered((int) categoriesPage.getTotalElements());

		table.setData(EmployeeTableRowDTO.listFrom(categoriesPage.getContent()));

		return table;
	}

	public List<EmployeeEntryDTO> getEnabledProfessors() {
		return EmployeeEntryDTO.listFrom(this.employeeService.getEnabledProfessors());
	}

	public List<EmployeeEntryDTO> getEnabledByCourse(Courses course) {
		return EmployeeEntryDTO.listFrom(this.employeeService.getEnabledProfessorsByCourse(course));
	}

	public List<EmployeeEntryDTO> getEnabledByCourseAcronym(String acronym) {
		return EmployeeEntryDTO.listFrom(this.employeeService.getEnabledProfessorsByCourseAcronym(acronym));
	}
}
