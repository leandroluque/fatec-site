package br.com.fatecmogidascruzes.enrollment;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.student.Student;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
@Table(name = "_enrollment", indexes = {
		@Index(name = "ind_enrollment_student", columnList = "student", unique = false) })
public class Enrollment extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "internship", nullable = true)
	private Boolean internship;

	@Basic
	@Column(name = "capstone_project", nullable = true)
	private Boolean capstoneProject;

	@JoinColumn(name = "student", nullable = false)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Student student;

	@Basic
	@Column(name = "semester", nullable = false)
	private int semester;

	@Basic
	@Column(name = "year", nullable = false)
	private int year;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "declaration_file_id", referencedColumnName = "id")
	private File declarationFile;

}