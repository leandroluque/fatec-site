package br.com.fatecmogidascruzes.enrollment.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EnrollmentDAO extends DAOImpl<Enrollment, Long>, JpaRepository<Enrollment, Long> {

	Optional<Enrollment> getByEnabledTrueAndStudent(Student student);

	@Query("SELECT do FROM DisciplineOffer do LEFT JOIN do.discipline di LEFT JOIN di.course co WHERE TRUE = do.enabled AND TRUE = di.enabled AND TRUE = co.enabled AND :semester = do.semester AND :year = do.year  AND (UPPER(di.name) LIKE CONCAT('%',:filter,'%') OR UPPER(co.name) LIKE CONCAT('%',:filter,'%'))")
	Page<DisciplineOffer> getEnabledByDisciplineAndFilter(
			@Param("filter") String filter,
			@Param("semester") int semester,
			@Param("year") int year,
			Pageable pageable);

	@Query("SELECT st FROM Student st LEFT JOIN st.course co WHERE TRUE = co.enabled AND TRUE = st.enabled  AND (UPPER(st.name) LIKE CONCAT('%',:filter,'%') OR UPPER(co.acronym) LIKE CONCAT('%',:filter,'%') OR UPPER(co.name) LIKE CONCAT('%',:filter,'%')) " +
			"AND (st.id IN (SELECT se.student.id FROM StudentEnrollment se LEFT JOIN se.disciplineOffer do WHERE TRUE = se.enabled AND TRUE = do.enabled AND :semester = do.semester AND :year = do.year) " +
			"OR st.id in (SELECT enr.student.id FROM Enrollment enr WHERE TRUE = enr.enabled AND :semester = enr.semester AND :year = enr.year))")
	Page<Student> getEnabledByStudentAndFilter(
			@Param("filter") String filter,
			@Param("semester") int semester,
			@Param("year") int year,
			Pageable pageable);

	@Query("SELECT se FROM StudentEnrollment se LEFT JOIN se.disciplineOffer do WHERE TRUE = se.enabled AND TRUE = do.enabled AND :hash = do.hash")
	List<StudentEnrollment> getEnabledEnrollmentsForDiscipline(@Param("hash") UUID disciplineOfferHash);

	@Query("SELECT se FROM StudentEnrollment se LEFT JOIN se.student st LEFT JOIN se.disciplineOffer do WHERE TRUE = se.enabled AND TRUE = st.enabled AND TRUE = do.enabled AND :hash = st.hash AND :semester = do.semester AND :year = do.year")
	List<StudentEnrollment> getEnabledEnrollmentsForStudent(@Param("hash") UUID studentHash, @Param("semester") int semester, @Param("year") int year);

	@Query("SELECT se FROM Enrollment se WHERE TRUE = se.enabled AND se.internship IS TRUE")
	List<Enrollment> getEnabledInternshipEnrollments();

	@Query("SELECT se FROM Enrollment se WHERE TRUE = se.enabled AND se.capstoneProject IS TRUE")
	List<Enrollment> getEnabledCapstoneProjectEnrollments();

	Enrollment getDistinctFirstByStudentAndSemesterAndYear(Student student, int semester, int year);

}