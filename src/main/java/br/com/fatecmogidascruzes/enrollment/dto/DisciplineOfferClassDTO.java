package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass.TimeSlot;
import br.com.fatecmogidascruzes.domain.WeekDay;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class DisciplineOfferClassDTO {

	private WeekDay weekDay;
	private TimeSlot timeSlot;

	public static DisciplineOfferClassDTO from(DisciplineOfferClass disciplineOfferClass) {
		DisciplineOfferClassDTO disciplineOfferClassDTO = new DisciplineOfferClassDTO();
		disciplineOfferClassDTO.setWeekDay(disciplineOfferClass.getWeekDay());
		disciplineOfferClassDTO.setTimeSlot(disciplineOfferClass.getTimeSlot());

		return disciplineOfferClassDTO;
	}

	public static List<DisciplineOfferClassDTO> listFrom(Collection<DisciplineOfferClass> objects) {
		List<DisciplineOfferClassDTO> dtos = new ArrayList<>();
		objects.forEach(request -> dtos.add(DisciplineOfferClassDTO.from(request)));
		return dtos;
	}
	
}
