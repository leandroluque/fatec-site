package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass;
import lombok.Data;

import java.util.*;

@Data
public class DisciplineOfferDTO {

	private String hashString;
	private String courseShift;
	private String courseAcronym;
	private String acronym;
	private int semester;
	private String name;
	private List<DisciplineOfferClassDTO> classes = new ArrayList<>();
	private String hours;
	private Set<String> enforcements = new HashSet<>();

	public void addEnforcementAcronym(String disciplineAcronym) {
		this.enforcements.add(disciplineAcronym);
	}

	public static DisciplineOfferDTO from(DisciplineOffer disciplineOffer) {
		DisciplineOfferDTO disciplineOfferDTO = new DisciplineOfferDTO();
		disciplineOfferDTO.setSemester(disciplineOffer.getDiscipline().getSemester());
		disciplineOfferDTO.setCourseAcronym(disciplineOffer.getDiscipline().getCourse().getAcronym());
		disciplineOfferDTO.setCourseShift(disciplineOffer.getDiscipline().getCourse().getShift().name());
		disciplineOfferDTO.setAcronym(disciplineOffer.getDiscipline().getAcronym());
		disciplineOfferDTO.setHashString(disciplineOffer.getHashString());
		disciplineOfferDTO.setName(disciplineOffer.getDiscipline().getCourse().getAcronym() + " ("
				+ disciplineOffer.getDiscipline().getCourse().getShift().getName() + ") - "
				+ disciplineOffer.getDiscipline().getName());
		disciplineOfferDTO.setClasses(DisciplineOfferClassDTO.listFrom(disciplineOffer.getDisciplineClasses()));

		for (Discipline enforcedDiscipline : disciplineOffer.getDiscipline().getEnforcements()) {
			Optional<DisciplineOffer> disciplineOfferOptional = enforcedDiscipline
					.getOfferFor(disciplineOffer.getSemester(), disciplineOffer.getYear());
			if (disciplineOfferOptional.isPresent()) {
				disciplineOfferDTO.addEnforcementAcronym(disciplineOfferOptional.get().getHashString());
			}
		}

		for (Discipline enforcedDiscipline : disciplineOffer.getDiscipline().getEnforced()) {
			Optional<DisciplineOffer> disciplineOfferOptional = enforcedDiscipline
					.getOfferFor(disciplineOffer.getSemester(), disciplineOffer.getYear());
			if (disciplineOfferOptional.isPresent()) {
				disciplineOfferDTO.addEnforcementAcronym(disciplineOfferOptional.get().getHashString());
			}
		}

		int i = 0;
		String hours = "";
		for (DisciplineOfferClass disciplineOfferClass : disciplineOffer.getDisciplineClasses()) {
			if (null != disciplineOfferClass.getWeekDay() && null != disciplineOfferClass.getTimeSlot()) {
				if (i > 0) {
					hours += ",";
				}

				hours += String.format("%14s: %s", disciplineOfferClass.getWeekDay().getName(),
						disciplineOfferClass.getTimeSlot().getName());
				i++;
			}
		}
		disciplineOfferDTO.setHours(hours);

		return disciplineOfferDTO;
	}

	public static List<DisciplineOfferDTO> listFrom(Collection<DisciplineOffer> objects) {
		List<DisciplineOfferDTO> dtos = new ArrayList<>();
		objects.forEach(request -> dtos.add(DisciplineOfferDTO.from(request)));
		return dtos;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DisciplineOfferDTO))
			return false;
		DisciplineOfferDTO other = (DisciplineOfferDTO) obj;
		if (hashString == null) {
			if (other.hashString != null)
				return false;
		} else if (!hashString.equals(other.hashString))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hashString == null) ? 0 : hashString.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

}
