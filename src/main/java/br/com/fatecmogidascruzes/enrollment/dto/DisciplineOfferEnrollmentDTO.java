package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class DisciplineOfferEnrollmentDTO {

	private UUID hash;
	private String hashString;
	private String registry;
	private String date;
	private String name;
	private String entryYearAndSemester;
	private int disciplineSemester;
	private int studentSemester;
	private String disciplineShift;
	private String studentShift;
	private boolean attendRequirements;
	private boolean approved;

	public static DisciplineOfferEnrollmentDTO from(
			StudentEnrollment object,
			DisciplineOfferService disciplineOfferService) {
		DisciplineOfferEnrollmentDTO disciplineOfferEnrollmentDTO = new DisciplineOfferEnrollmentDTO();
		disciplineOfferEnrollmentDTO.setHash(object.getHash());
		disciplineOfferEnrollmentDTO.setHashString(FriendlyId.toFriendlyId(object.getHash()));
		disciplineOfferEnrollmentDTO
				.setDate(object.getCreationDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
		disciplineOfferEnrollmentDTO.setDisciplineSemester(object.getDisciplineOffer().getDiscipline().getSemester());
		disciplineOfferEnrollmentDTO.setDisciplineShift(object.getDisciplineOffer().getDiscipline().getCourse().getShift().getName());
		disciplineOfferEnrollmentDTO.setRegistry(object.getStudent().getRegistry());
		disciplineOfferEnrollmentDTO.setName(object.getStudent().getName());
		disciplineOfferEnrollmentDTO.setEntryYearAndSemester(object.getStudent().getEntryYearAndSemester());
		int entryYear = Integer.valueOf(object.getStudent().getEntryYearAndSemester().substring(0, 4));
		int entrySemester = Integer.valueOf(object.getStudent().getEntryYearAndSemester().substring(4, 5));
		int currentSemester = (SemesterYearConfig.getPreEnrollYear() - entryYear) * 2 + (SemesterYearConfig.getPreEnrollSemester() - entrySemester) + 1;

		disciplineOfferEnrollmentDTO.setStudentSemester(currentSemester);
		disciplineOfferEnrollmentDTO.setStudentShift(object.getStudent().getCourse().getShift().getName());

		List<DisciplineOffer> offers = disciplineOfferService
				.findByEnabledTrueAndAvailableFor(object.getStudent(), SemesterYearConfig.getPreEnrollSemester(), SemesterYearConfig.getPreEnrollYear());

		disciplineOfferEnrollmentDTO.setAttendRequirements(true);
		for (Discipline discipline : object.getDisciplineOffer().getDiscipline().getRequirements()) {
			if (offers
					.stream()
					.filter(disciplineOffer -> disciplineOffer.getDiscipline().getHash().equals(discipline.getHash()))
					.count() > 0) {
				disciplineOfferEnrollmentDTO.setAttendRequirements(false);
				break;
			}
		}

		List<StudentEnrollment> approvedEnrollments = object.getDisciplineOffer().getApprovedEnrollments();
		if (approvedEnrollments
				.stream()
				.filter(enrollment -> enrollment.getStudent().getHash().equals(object.getStudent().getHash()))
				.count() > 0) {
			disciplineOfferEnrollmentDTO.setApproved(true);
		}
		return disciplineOfferEnrollmentDTO;
	}

	public static <T extends StudentEnrollment> List<DisciplineOfferEnrollmentDTO> listFrom(
			List<T> objects,
			DisciplineOfferService disciplineOfferService) {
		List<DisciplineOfferEnrollmentDTO> dtos = new ArrayList<>();
		objects.forEach(object -> dtos.add(DisciplineOfferEnrollmentDTO.from(object, disciplineOfferService)));
		return dtos;
	}

}
