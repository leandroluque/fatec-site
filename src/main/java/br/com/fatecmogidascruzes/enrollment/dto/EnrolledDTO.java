package br.com.fatecmogidascruzes.enrollment.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class EnrolledDTO {

	private String[] disciplines;
	private String student;
	private Boolean capstoneProject;
	private Boolean internship;
	private MultipartFile file;
}
