package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class EnrollmentByDisciplineTableRowDTO {

	private UUID hash;
	private String hashString;
	private String course;
	private String shift;
	private String name;
	private int howManyStudents;

	public static EnrollmentByDisciplineTableRowDTO from(DisciplineOffer disciplineOffer) {
		EnrollmentByDisciplineTableRowDTO enrollmentByDisciplineTableRowDTO = new EnrollmentByDisciplineTableRowDTO();
		enrollmentByDisciplineTableRowDTO.setHash(disciplineOffer.getHash());
		enrollmentByDisciplineTableRowDTO.setHashString(FriendlyId.toFriendlyId(disciplineOffer.getHash()));
		enrollmentByDisciplineTableRowDTO.setCourse(disciplineOffer.getDiscipline().getCourse().getName());
		enrollmentByDisciplineTableRowDTO.setShift(disciplineOffer.getDiscipline().getCourse().getShift().getName());
		enrollmentByDisciplineTableRowDTO.setName(disciplineOffer.getDiscipline().getName());
		enrollmentByDisciplineTableRowDTO.setHowManyStudents(disciplineOffer.getEnrollments().size());
		return enrollmentByDisciplineTableRowDTO;
	}

	public static <T extends DisciplineOffer> List<EnrollmentByDisciplineTableRowDTO> listFrom(
			List<T> disciplineOffers) {
		List<EnrollmentByDisciplineTableRowDTO> enrollmentByDisciplineTableRowDTOs = new ArrayList<>();
		disciplineOffers
				.forEach(
						object -> enrollmentByDisciplineTableRowDTOs
								.add(EnrollmentByDisciplineTableRowDTO.from(object)));
		return enrollmentByDisciplineTableRowDTOs;
	}

}
