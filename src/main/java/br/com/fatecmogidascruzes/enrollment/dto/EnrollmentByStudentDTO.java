package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.*;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class EnrollmentByStudentDTO {

	private UUID hash;
	private String hashString;
	private String course;
	private String shift;
	private String name;
	private String date;
	private UUID studentHash;
	private String studentName;
	private int howManyOthersInDiscipline;
	private boolean attendRequirements;
	private boolean approved;

	public static EnrollmentByStudentDTO from(StudentEnrollment object, DisciplineOfferService disciplineOfferService) {
		EnrollmentByStudentDTO dto = new EnrollmentByStudentDTO();
		dto.setHash(object.getHash());
		dto.setHashString(FriendlyId.toFriendlyId(object.getHash()));
		dto.setCourse(object.getDisciplineOffer().getDiscipline().getCourse().getAcronym());
		dto.setShift(object.getDisciplineOffer().getDiscipline().getCourse().getShift().getName());
		dto.setStudentName(object.getStudent().getName());
		dto.setName(object.getDisciplineOffer().getDiscipline().getAcronym() + " - "
				+ object.getDisciplineOffer().getDiscipline().getName());
		dto.setDate(object.getCreationDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
		dto.setStudentHash(object.getStudent().getHash());
		dto.setHowManyOthersInDiscipline(object.getDisciplineOffer().getEnrollments().size());

		List<DisciplineOffer> offers = disciplineOfferService.findByEnabledTrueAndAvailableFor(object.getStudent(),
				SemesterYearConfig.getPreEnrollSemester(), SemesterYearConfig.getPreEnrollYear());

		dto.setAttendRequirements(true);
		for (Discipline discipline : object.getDisciplineOffer().getDiscipline().getRequirements()) {
			if (offers.stream()
					.filter(disciplineOffer -> disciplineOffer.getDiscipline().getHash().equals(discipline.getHash()))
					.count() > 0) {
				dto.setAttendRequirements(false);
				break;
			}
		}

		List<StudentEnrollment> approvedEnrollments = object.getDisciplineOffer().getApprovedEnrollments();
		if (approvedEnrollments.stream()
				.filter(enrollment -> enrollment.getStudent().getHash().equals(object.getStudent().getHash()))
				.count() > 0) {
			dto.setApproved(true);
		}

		return dto;
	}

	public static <T extends StudentEnrollment> Set<EnrollmentByStudentDTO> setFrom(List<T> objects,
			DisciplineOfferService disciplineOfferService) {
		Set<EnrollmentByStudentDTO> dtos = new HashSet<>();
		objects.forEach(object -> dtos.add(EnrollmentByStudentDTO.from(object, disciplineOfferService)));
		return dtos;
	}

	public static <T extends StudentEnrollment> List<EnrollmentByStudentDTO> listFrom(List<T> objects,
			DisciplineOfferService disciplineOfferService) {
		List<EnrollmentByStudentDTO> dtos = new ArrayList<>();
		objects.forEach(object -> dtos.add(EnrollmentByStudentDTO.from(object, disciplineOfferService)));
		return dtos;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnrollmentByStudentDTO other = (EnrollmentByStudentDTO) obj;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		if (studentHash == null) {
			if (other.studentHash != null)
				return false;
		} else if (!studentHash.equals(other.studentHash))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((studentHash == null) ? 0 : studentHash.hashCode());
		return result;
	}

}
