package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.enrollment.data.EnrollmentDAO;
import br.com.fatecmogidascruzes.student.Student;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class EnrollmentByStudentTableRowDTO {

    private UUID hash;
    private String hashString;
    private String ra;
    private String course;
    private String shift;
    private String name;
    private String internship;
    private String capstone;
    private boolean hasFile;
    private String enrollmentHash;
    private long howManyDisciplines;

    public static EnrollmentByStudentTableRowDTO from(Student object, long howManyDisciplines, Enrollment enrollment) {
        EnrollmentByStudentTableRowDTO dto = new EnrollmentByStudentTableRowDTO();
        dto.setHash(object.getHash());
        dto.setHashString(FriendlyId.toFriendlyId(object.getHash()));
        dto.setCourse(object.getCourse().getName());
        dto.setShift(object.getCourse().getShift().getName());
        dto.setName(object.getName());
        dto.setHowManyDisciplines(howManyDisciplines);
        dto.setRa(object.getRegistry());
        dto.setInternship(enrollment != null && enrollment.getInternship() ? "Sim" : "Não");
        dto.setCapstone(enrollment != null && enrollment.getCapstoneProject() ? "Sim" : "Não");
        dto.setHasFile(enrollment != null && enrollment.getDeclarationFile() != null);
        dto.setEnrollmentHash(enrollment != null ? enrollment.getHashString() : null);
        return dto;
    }

    public static <T extends Student> List<EnrollmentByStudentTableRowDTO> setFrom(
            List<T> objects,
            EnrollmentDAO enrollmentDAO) {
        List<EnrollmentByStudentTableRowDTO> dtos = new ArrayList<>();
        objects.forEach(object -> dtos.add(
                EnrollmentByStudentTableRowDTO.from(
                        object,
                        enrollmentDAO.getEnabledEnrollmentsForStudent(object.getHash(),
                                SemesterYearConfig.getPreEnrollSemester(),
                                SemesterYearConfig.getPreEnrollYear()).size(),
                        enrollmentDAO.getDistinctFirstByStudentAndSemesterAndYear(object,
                                SemesterYearConfig.getPreEnrollSemester(),
                                SemesterYearConfig.getPreEnrollYear())
                )));
        return dtos;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EnrollmentByStudentTableRowDTO other = (EnrollmentByStudentTableRowDTO) obj;
        if (hash == null) {
            if (other.hash != null)
                return false;
        } else if (!hash.equals(other.hash))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((hash == null) ? 0 : hash.hashCode());
        return result;
    }

}
