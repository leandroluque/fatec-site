package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.storage.Storage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Data
@Slf4j
public class EnrollmentDTO {

	List<DisciplineOfferDTO> disciplines = new ArrayList<>();
	private Boolean internship;
	private Boolean capstoneProject;
	private boolean fileLoaded;
	private String fileName;
	private String hash;

	public void add(DisciplineOffer disciplineOffer) {
		if (null != disciplineOffer.getDisciplineClasses() && !disciplineOffer.getDisciplineClasses().isEmpty()) {
			this.disciplines.add(DisciplineOfferDTO.from(disciplineOffer));
		}
	}

	public static EnrollmentDTO from(Enrollment enrollment, Storage storage) {
		EnrollmentDTO enrollmentDTO = new EnrollmentDTO();
		enrollmentDTO.setInternship(enrollment.getInternship());
		enrollmentDTO.setCapstoneProject(enrollment.getCapstoneProject());
		enrollmentDTO.setFileLoaded(enrollment.getDeclarationFile() != null);
		if(enrollment.getDeclarationFile() != null) {
			enrollmentDTO.setFileName(enrollment.getDeclarationFile().getOriginalName());
		}
		enrollmentDTO.setHash(enrollment.getHashString());
		return enrollmentDTO;
	}

}
