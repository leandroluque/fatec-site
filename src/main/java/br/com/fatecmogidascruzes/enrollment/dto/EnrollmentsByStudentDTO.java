package br.com.fatecmogidascruzes.enrollment.dto;

import br.com.fatecmogidascruzes.enrollment.service.web.EnrollmentService;
import br.com.fatecmogidascruzes.student.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class EnrollmentsByStudentDTO {

	private String studentRegistry;
	private String studentCourse;
	private String studentShift;
	private String studentHash;
	private String studentName;
	private List<EnrollmentByStudentDTO> enrollments = new ArrayList<>();

	void addStudentEnrollment(EnrollmentByStudentDTO enrollmentByStudentDTO) {
		this.enrollments.add(enrollmentByStudentDTO);
	}

	public static EnrollmentsByStudentDTO from(Student student, EnrollmentService enrollmentService) {
		EnrollmentsByStudentDTO enrollmentsByStudentDTO = new EnrollmentsByStudentDTO();
		enrollmentsByStudentDTO.setStudentHash(student.getHashString());
		enrollmentsByStudentDTO.setStudentRegistry(student.getRegistry());
		enrollmentsByStudentDTO.setStudentName(student.getName());
		enrollmentsByStudentDTO.setStudentCourse(student.getCourse().getAcronym());
		enrollmentsByStudentDTO.setStudentShift(student.getCourse().getShift().getName());

		if (null != enrollmentService.findEnrollmentsForStudentAsList(student.getHash())) {
			enrollmentsByStudentDTO
					.setEnrollments(enrollmentService.findEnrollmentsForStudentAsList(student.getHash()));
			for(int i = 0; i < enrollmentsByStudentDTO.getEnrollments().size(); i++) {
				if(null == enrollmentsByStudentDTO.getEnrollments().get(i)) {
					enrollmentsByStudentDTO.getEnrollments().remove(i);
				}
			}
		}

		return enrollmentsByStudentDTO;
	}

	public static List<EnrollmentsByStudentDTO> listFrom(List<Student> objects, EnrollmentService enrollmentService) {
		List<EnrollmentsByStudentDTO> objectTableRowDTOs = new ArrayList<>();
		objects.forEach(baseCategory -> {
			EnrollmentsByStudentDTO enrollmentsByStudentDTO = EnrollmentsByStudentDTO.from(baseCategory,
					enrollmentService);
			if (null != enrollmentsByStudentDTO.getEnrollments()
					&& !enrollmentsByStudentDTO.getEnrollments().isEmpty()) {
				objectTableRowDTOs.add(enrollmentsByStudentDTO);
			}
		});
		return objectTableRowDTOs;
	}

}
