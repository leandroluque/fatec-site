package br.com.fatecmogidascruzes.enrollment.service.web;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.configuration.Configuration;
import br.com.fatecmogidascruzes.configuration.ConfigurationEnum;
import br.com.fatecmogidascruzes.configuration.service.ConfigurationService;
import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;
import br.com.fatecmogidascruzes.enrollment.data.EnrollmentDAO;
import br.com.fatecmogidascruzes.enrollment.dto.DisciplineOfferEnrollmentDTO;
import br.com.fatecmogidascruzes.enrollment.dto.EnrollmentByDisciplineTableRowDTO;
import br.com.fatecmogidascruzes.enrollment.dto.EnrollmentByStudentTableRowDTO;
import br.com.fatecmogidascruzes.enrollment.dto.EnrollmentsByStudentDTO;
import br.com.fatecmogidascruzes.enrollment.viewmodel.EnrollmentConfigVM;
import br.com.fatecmogidascruzes.student.service.StudentService;
import br.com.fatecmogidascruzes.user.service.UserService;

@Controller
@PreAuthorize("hasRole('ADMINISTRATIVE')")
@RequestMapping("/admin/enrollment")
public class EnrollmentAdminController extends MVCController {

	private EnrollmentService enrollmentService;
	private final StudentService studentService;
	private final EnrollmentDAO enrollmentDAO;

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	public EnrollmentAdminController(HttpSession httpSession, UserService userService, StudentService studentService,
			EnrollmentService enrollmentService, EnrollmentDAO enrollmentDAO) {
		super(httpSession, userService);
		this.studentService = studentService;
		this.enrollmentService = enrollmentService;
		this.enrollmentDAO = enrollmentDAO;
	}

	@GetMapping("/configure")
	public ModelAndView configure(@ModelAttribute("viewModel") EnrollmentConfigVM viewModel) {
		ModelAndView modelAndView = new ModelAndView("/enrollment/configure");

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime nextSemester = now.plusMonths(6);
		int enrollmentSemester = 1 + (nextSemester.getMonthValue() / 6);

		Optional<Configuration> opCurrentConfiguredSemester = configurationService
				.getByEnabledTrueAndName(ConfigurationEnum.PRE_ENROLL_SEMESTER.name());
		Optional<Configuration> opCurrentConfiguredYear = configurationService
				.getByEnabledTrueAndName(ConfigurationEnum.PRE_ENROLL_YEAR.name());

		String currentConfiguredSemester = null;
		if (opCurrentConfiguredSemester.isPresent()) {
			currentConfiguredSemester = opCurrentConfiguredSemester.get().getValue();
		} else {
			currentConfiguredSemester = "NÃO CONFIGURADO";
		}

		String currentConfiguredYear = null;
		if (opCurrentConfiguredYear.isPresent()) {
			currentConfiguredYear = opCurrentConfiguredYear.get().getValue();
		} else {
			currentConfiguredYear = "NÃO CONFIGURADO";
		}
		modelAndView.addObject("currentConfiguredPeriod", currentConfiguredSemester + "/" + currentConfiguredYear);

		if (null == viewModel.getSemester()) {
			viewModel.setSemester(enrollmentSemester);
			viewModel.setYear(nextSemester.getYear());
		}

		return modelAndView;
	}

	@PostMapping({"/configure", "/saveSemesterYear"})
	public ModelAndView saveSemesterYear(@ModelAttribute("viewModel") EnrollmentConfigVM viewModel,
			RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/enrollment/configure");

		try {
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_SEMESTER.name(),
					viewModel.getSemester().toString()));
			configurationService.upinsert(
					new Configuration(ConfigurationEnum.PRE_ENROLL_YEAR.name(), viewModel.getYear().toString()));
			redirectAttributes.addFlashAttribute("message", "Os dados foram gravados com sucesso. Vá para a etapa 2.");
			redirectAttributes.addFlashAttribute("messageType", "success");
		} catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("message",
					"Ocorreu um erro ao tentar gravar os dados. Contate o administrador do sistema.");
			redirectAttributes.addFlashAttribute("messageType", "error");
		}

		return modelAndView;
	}

	@GetMapping("/enableForAdmin")
	public ModelAndView enableForAdmin(RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/enrollment/configure");

		try {
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_ENABLED.name(), "true"));
			redirectAttributes.addFlashAttribute("message", "Os dados foram gravados com sucesso. Vá para a etapa 4.");
			redirectAttributes.addFlashAttribute("messageType", "success");
		} catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("message",
					"Ocorreu um erro ao tentar gravar os dados. Contate o administrador do sistema.");
			redirectAttributes.addFlashAttribute("messageType", "error");
		}

		return modelAndView;
	}

	@GetMapping("/disableForAdmin")
	public ModelAndView disableForAdmin(RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/enrollment/configure");

		try {
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_ENABLED.name(), "false"));
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_MENU_ENABLED.name(), "false"));
			redirectAttributes.addFlashAttribute("message", "Os dados foram gravados com sucesso. A matrícula está desabilitada.");
			redirectAttributes.addFlashAttribute("messageType", "success");
		} catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("message",
					"Ocorreu um erro ao tentar gravar os dados. Contate o administrador do sistema.");
			redirectAttributes.addFlashAttribute("messageType", "error");
		}

		return modelAndView;
	}
	
	@GetMapping("/enableForStudents")
	public ModelAndView enableForStudent(RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/enrollment/configure");

		try {
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_MENU_ENABLED.name(), "true"));
			redirectAttributes.addFlashAttribute("message", "Os dados foram gravados com sucesso. O menu de matrícula está visível para os estudantes.");
			redirectAttributes.addFlashAttribute("messageType", "success");
		} catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("message",
					"Ocorreu um erro ao tentar gravar os dados. Contate o administrador do sistema.");
			redirectAttributes.addFlashAttribute("messageType", "error");
		}

		return modelAndView;
	}

	@GetMapping("/disableForStudents")
	public ModelAndView siableForStudent(RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/enrollment/configure");

		try {
			configurationService.upinsert(new Configuration(ConfigurationEnum.PRE_ENROLL_MENU_ENABLED.name(), "false"));
			redirectAttributes.addFlashAttribute("message", "Os dados foram gravados com sucesso. O menu de matrícula está INvisível para os estudantes.");
			redirectAttributes.addFlashAttribute("messageType", "success");
		} catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("message",
					"Ocorreu um erro ao tentar gravar os dados. Contate o administrador do sistema.");
			redirectAttributes.addFlashAttribute("messageType", "error");
		}

		return modelAndView;
	}
	
	@RequestMapping("/discipline/search")
	public ModelAndView enrollmentByDiscipline() {
		ModelAndView modelAndView = new ModelAndView("/enrollment/searchByDiscipline");
		return modelAndView;
	}

	@RequestMapping("/view/discipline/{hash}")
	public ModelAndView enrollmentByDiscipline(@PathVariable("hash") UUID hash) {

		ModelAndView modelAndView = new ModelAndView("/enrollment/viewByDiscipline");
		List<DisciplineOfferEnrollmentDTO> enrollments = enrollmentService.findEnrollmentsForDiscipline(hash);

		Collections.sort(enrollments, new Comparator<DisciplineOfferEnrollmentDTO>() {
			@Override
			public int compare(DisciplineOfferEnrollmentDTO one, DisciplineOfferEnrollmentDTO other) {
				if (one.isAttendRequirements()) {
					if (!other.isAttendRequirements()) {
						return -1;
					}
				} else {
					if (other.isAttendRequirements()) {
						return 1;
					}
				}

				int oneEntryYear = Integer.valueOf(one.getEntryYearAndSemester().substring(0, 4));
				int oneEntrySemester = Integer.valueOf(one.getEntryYearAndSemester().substring(4, 5));
				int oneCurrentSemester = (SemesterYearConfig.getPreEnrollYear() - oneEntryYear) * 2
						+ (SemesterYearConfig.getPreEnrollSemester() - oneEntrySemester) + 1;

				int otherEntryYear = Integer.valueOf(other.getEntryYearAndSemester().substring(0, 4));
				int otherEntrySemester = Integer.valueOf(other.getEntryYearAndSemester().substring(4, 5));
				int otherCurrentSemester = (SemesterYearConfig.getPreEnrollYear() - otherEntryYear) * 2
						+ (SemesterYearConfig.getPreEnrollSemester() - otherEntrySemester) + 1;

				if (one.getDisciplineSemester() == one.getStudentSemester()) {
					if (other.getDisciplineSemester() == other.getStudentSemester()) {
						if (one.getDisciplineShift().equals(one.getStudentShift())) {
							if (other.getDisciplineShift().equals(other.getStudentShift())) {
								int difference = otherCurrentSemester - oneCurrentSemester;
								if (0 == difference) {
									return one.getName().compareTo(other.getName());
								}
								return difference;
							} else {
								return -1;
							}
						} else {
							if (other.getDisciplineShift().equals(other.getStudentShift())) {
								return 1;
							} else {
								int difference = otherCurrentSemester - oneCurrentSemester;
								if (0 == difference) {
									return one.getName().compareTo(other.getName());
								}
								return difference;
							}
						}
					} else {
						return -1;
					}
				} else {
					if (other.getDisciplineSemester() == other.getStudentSemester()) {
						return 1;
					} else {
						if (one.getDisciplineShift().equals(one.getStudentShift())) {
							if (other.getDisciplineShift().equals(other.getStudentShift())) {
								int difference = otherCurrentSemester - oneCurrentSemester;
								if (0 == difference) {
									return one.getName().compareTo(other.getName());
								}
								return difference;
							} else {
								return -1;
							}
						} else {
							if (one.getDisciplineShift().equals(one.getStudentShift())) {
								return 1;
							} else {
								int difference = otherCurrentSemester - oneCurrentSemester;
								if (0 == difference) {
									return one.getName().compareTo(other.getName());
								}
								return difference;
							}
						}
					}
				}
			}
		});

		modelAndView.addObject("enrollments", enrollments);
		return modelAndView;
	}

	// Fields to filter the table - related to the JPQL.
	private static String[] fields_disciplines = { null, "co.name", "co.shift", "di.name", null };

	@RequestMapping(path = "/discipline/table", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody TableDTO<EnrollmentByDisciplineTableRowDTO> getTable(
			@RequestParam(name = "draw", required = false) Integer draw,
			@RequestParam(name = "start", required = false, defaultValue = "0") Integer initialPage,
			@RequestParam(name = "length", required = false, defaultValue = "10") Integer numberOfRegisters,
			@RequestParam(name = "search[value]", required = false, defaultValue = "") String filter,
			@RequestParam(name = "order[0][column]", required = false, defaultValue = "1") Integer columnsToSort,
			@RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String columnsOrder) {

		SearchCriteria searchCriteria = new SearchCriteria();
		if (filter != null && !filter.equals("")) {
			searchCriteria.setFilter(filter);
		}
		if (columnsToSort < fields_disciplines.length) {
			String fieldName = fields_disciplines[columnsToSort];
			searchCriteria.addSortBy(fieldName);
			searchCriteria.setOrder(columnsOrder.equalsIgnoreCase("asc") ? SearchCriteria.Order.ASCENDING
					: SearchCriteria.Order.DESCENDING);
		}

		searchCriteria.setWhatToFilter(Arrays.asList(fields_students));
		searchCriteria.setInitialRegister(initialPage * numberOfRegisters);
		searchCriteria.setNumberOfRegisters(numberOfRegisters);

		TableDTO<EnrollmentByDisciplineTableRowDTO> result = enrollmentService.getTableByDiscipline(searchCriteria,
				draw);
		result.setDraw(draw);
		return result;
	}

	@RequestMapping("/student/search")
	public ModelAndView enrollmentByStudent() {
		ModelAndView modelAndView = new ModelAndView("/enrollment/searchByStudent");
		return modelAndView;
	}

	@RequestMapping("/view/student/{hash}")
	public ModelAndView enrollmentByStudent(@PathVariable("hash") UUID hash) {

		ModelAndView modelAndView = new ModelAndView("/enrollment/viewByStudent");
		modelAndView.addObject("studentName", studentService.getByHash(hash).get());
		modelAndView.addObject("enrollments", enrollmentService.findEnrollmentsForStudent(hash));
		return modelAndView;
	}

	@RequestMapping("/view/studentSummary")
	public ModelAndView studentSummary() {

		ModelAndView modelAndView = new ModelAndView("/enrollment/summary");

		List<EnrollmentsByStudentDTO> enrollmentByStudentDTO = enrollmentService.getEnrollmentSummary();
		Collections.sort(enrollmentByStudentDTO, new Comparator<EnrollmentsByStudentDTO>() {

			@Override
			public int compare(EnrollmentsByStudentDTO o1, EnrollmentsByStudentDTO o2) {
				return o1.getStudentName().compareTo(o2.getStudentName());
			}

		});

		modelAndView.addObject("enrollmentsByStudent", enrollmentByStudentDTO);
		modelAndView.addObject("internship", enrollmentDAO.getEnabledInternshipEnrollments());
		modelAndView.addObject("capstoneProject", enrollmentDAO.getEnabledCapstoneProjectEnrollments());
		return modelAndView;
	}

	// Fields to filter the table - related to the JPQL.
	private static String[] fields_students = { null, "co.name", "co.shift", "name", null };

	@RequestMapping(path = "/student/table", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody TableDTO<EnrollmentByStudentTableRowDTO> getTableByStudent(
			@RequestParam(name = "draw", required = false) Integer draw,
			@RequestParam(name = "start", required = false, defaultValue = "0") Integer initialPage,
			@RequestParam(name = "length", required = false, defaultValue = "10") Integer numberOfRegisters,
			@RequestParam(name = "search[value]", required = false, defaultValue = "") String filter,
			@RequestParam(name = "order[0][column]", required = false, defaultValue = "1") Integer columnsToSort,
			@RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String columnsOrder) {

		SearchCriteria searchCriteria = new SearchCriteria();
		if (filter != null && !filter.equals("")) {
			searchCriteria.setFilter(filter);
		}
		if (columnsToSort < fields_students.length) {
			String fieldName = fields_students[columnsToSort];
			searchCriteria.addSortBy(fieldName);
			searchCriteria.setOrder(columnsOrder.equalsIgnoreCase("asc") ? SearchCriteria.Order.ASCENDING
					: SearchCriteria.Order.DESCENDING);
		}

		searchCriteria.setWhatToFilter(Arrays.asList(fields_students));
		searchCriteria.setInitialRegister(initialPage / numberOfRegisters);
		searchCriteria.setNumberOfRegisters(numberOfRegisters);

		TableDTO<EnrollmentByStudentTableRowDTO> result = enrollmentService.getTableByStudent(searchCriteria, draw);
		result.setDraw(draw);
		return result;
	}

}
