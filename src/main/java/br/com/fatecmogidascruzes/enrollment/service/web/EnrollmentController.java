package br.com.fatecmogidascruzes.enrollment.service.web;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.enrollment.dto.DisciplineOfferDTO;
import br.com.fatecmogidascruzes.enrollment.dto.EnrolledDTO;
import br.com.fatecmogidascruzes.enrollment.dto.EnrollmentDTO;
import br.com.fatecmogidascruzes.exception.web.InternalErrorException;
import br.com.fatecmogidascruzes.exception.web.NotFoundException;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.storage.Storage;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.service.StudentService;
import br.com.fatecmogidascruzes.user.service.UserService;
import br.com.fatecmogidascruzes.util.Constants;
import com.devskiller.friendly_id.FriendlyId;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/enrollment")
public class EnrollmentController extends MVCController {

    private static final int SEMESTER = SemesterYearConfig.getPreEnrollSemester();
    private static final int YEAR = SemesterYearConfig.getPreEnrollYear();

    private final DisciplineOfferService disciplineOfferService;
    private final StudentService studentService;
    private final EnrollmentService enrollmentService;
    private final Storage storage;

    @Autowired
    public EnrollmentController(HttpSession httpSession, UserService userService,
                                DisciplineOfferService disciplineOfferService, StudentService studentService,
                                EnrollmentService enrollmentService, Storage storage) {
        super(httpSession, userService);
        this.disciplineOfferService = disciplineOfferService;
        this.studentService = studentService;
        this.enrollmentService = enrollmentService;
        this.storage = storage;
    }

    @GetMapping
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("enrollment/enrollmentSignIn");
        modelAndView.addObject("preEnrollmentEnabled", SemesterYearConfig.isPreEnrollEnabled());
        return modelAndView;
    }

    @PostMapping(value = "/signIn", produces = "multipart/form-data")
    public ModelAndView signIn(@RequestParam("username") String username, @RequestParam("password") String password) {
        Optional<Student> studentOptional = this.studentService.getEnabledByUsernameAndPassword(username, password);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            ModelAndView modelAndView = new ModelAndView("enrollment/siteEdit");
            modelAndView.addObject("student", student);

            Set<DisciplineOfferDTO> disciplineOffersDTOs = new HashSet<>();

            List<DisciplineOffer> disciplineOffers = disciplineOfferService
                    .findByEnabledTrueAndAvailableFor(student, SEMESTER, YEAR);

            disciplineOffersDTOs.addAll(DisciplineOfferDTO.listFrom(disciplineOffers));

            List<DisciplineOfferDTO> orderedDisciplineDTOs = new ArrayList<>(disciplineOffersDTOs);
            Collections.sort(orderedDisciplineDTOs, (arg0, arg1) -> arg0.getName().compareTo(arg1.getName()));

            Optional<Enrollment> enrollment = enrollmentService.getByEnabledTrueAndStudent(student);
            if (!enrollment.isPresent()) {
                enrollment = Optional.of(new Enrollment());
            }

            EnrollmentDTO enrollmentDTO = EnrollmentDTO.from(enrollment.get(), this.storage);
            enrollmentDTO.setDisciplines(orderedDisciplineDTOs);
            Gson gson = new Gson();
            modelAndView.addObject("student", student);

            EnrollmentDTO enrolledDTO = new EnrollmentDTO();
            enrolledDTO
                    .setDisciplines(
                            DisciplineOfferDTO
                                    .listFrom(
                                            disciplineOfferService
                                                    .findByEnabledTrueAndEnrolledBy(student, SEMESTER, YEAR)));

            modelAndView.addObject("enrolled", gson.toJson(enrolledDTO));
            modelAndView.addObject("semester", SEMESTER);
            modelAndView.addObject("year", YEAR);
            modelAndView.addObject("internship", enrollmentDTO.getInternship());
            modelAndView.addObject("capstoneProject", enrollmentDTO.getCapstoneProject());
            modelAndView.addObject("hash", enrollmentDTO.getHash());
            modelAndView.addObject("fileName", enrollmentDTO.getFileName());
            modelAndView.addObject("fileLoaded", enrollmentDTO.isFileLoaded());
            modelAndView.addObject("enrollment", gson.toJson(enrollmentDTO));

            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("redirect:/enrollment?error=");
            return modelAndView;
        }
    }

    @PostMapping(value = "/save", consumes = "multipart/form-data")
    public @ResponseBody String save(@Valid EnrolledDTO enrolledDTO) {
        try {
                enrollmentService.save(enrolledDTO);
                return "success";
        } catch (Exception error) {
            error.printStackTrace();
            throw error;
        }
    }

    @GetMapping(value = "/{hash}/file")
    public String getDeclarationFile(@PathVariable(value="hash") final String enrollmentHash,
         HttpServletRequest request) {

        try {
            Optional<Enrollment> enrollmentOptional =
                    this.enrollmentService.getEnabledByHash(FriendlyId.toUuid(enrollmentHash));
            if (enrollmentOptional.isPresent() && enrollmentOptional.get().getDeclarationFile() != null) {
                File file = enrollmentOptional.get().getDeclarationFile();
                return "redirect:" + getBaseURI(request) + Constants.FILES_PATH + file.getName();
            } else {
                throw new NotFoundException("The declaration file has not been found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new InternalErrorException(e);
        }
    }

}
