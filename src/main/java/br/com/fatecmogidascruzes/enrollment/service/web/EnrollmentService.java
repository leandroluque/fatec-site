package br.com.fatecmogidascruzes.enrollment.service.web;

import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.dto.TableDTO;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.enrollment.dto.*;
import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.student.Student;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface EnrollmentService extends BaseService<Enrollment, Long> {

	Optional<Enrollment> getByEnabledTrueAndStudent(Student student);

	TableDTO<EnrollmentByDisciplineTableRowDTO> getTableByDiscipline(SearchCriteria searchCriteria, int draw);

	List<DisciplineOfferEnrollmentDTO> findEnrollmentsForDiscipline(UUID disciplineOfferHash);

	Set<EnrollmentByStudentDTO> findEnrollmentsForStudent(UUID studentHash);

	TableDTO<EnrollmentByStudentTableRowDTO> getTableByStudent(SearchCriteria searchCriteria, int draw);

	List<EnrollmentsByStudentDTO> getEnrollmentSummary();

	List<EnrollmentByStudentDTO> findEnrollmentsForStudentAsList(UUID studentHash);

	void save(EnrolledDTO enrolledDTO);

}
