package br.com.fatecmogidascruzes.enrollment.service.web;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.discipline.data.DisciplineOfferDAO;
import br.com.fatecmogidascruzes.discipline.data.StudentEnrollmentDAO;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.dto.TableDTO;
import br.com.fatecmogidascruzes.enrollment.Enrollment;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.enrollment.data.EnrollmentDAO;
import br.com.fatecmogidascruzes.enrollment.dto.*;
import br.com.fatecmogidascruzes.file.service.FileService;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.data.StudentDAO;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class EnrollmentServiceImpl extends BaseServiceImpl<Enrollment, Long> implements EnrollmentService {

	private final DisciplineOfferService disciplineOfferService;
	private final DisciplineOfferDAO disciplineOfferDAO;
	private final EnrollmentDAO enrollmentDAO;
	private final StudentDAO studentDAO;
	private final StudentEnrollmentDAO studentEnrollmentDAO;
	private final FileService fileService;
	private static final int SEMESTER = SemesterYearConfig.getPreEnrollSemester();
	private static final int YEAR = SemesterYearConfig.getPreEnrollYear();

	@Autowired
	public EnrollmentServiceImpl(DisciplineOfferDAO disciplineOfferDAO, EnrollmentDAO enrollmentDAO,
			StudentDAO studentDAO, DisciplineOfferService disciplineOfferService,
		 	StudentEnrollmentDAO studentEnrollmentDAO, FileService fileService) {
		super(enrollmentDAO);
		this.disciplineOfferDAO = disciplineOfferDAO;
		this.enrollmentDAO = enrollmentDAO;
		this.studentDAO = studentDAO;
		this.disciplineOfferService = disciplineOfferService;
		this.studentEnrollmentDAO = studentEnrollmentDAO;
		this.fileService = fileService;
	}

	@Override
	public Optional<br.com.fatecmogidascruzes.enrollment.Enrollment> getByEnabledTrueAndStudent(Student student) {
		return enrollmentDAO.getByEnabledTrueAndStudent(student);
	}

	@Override
	public TableDTO<EnrollmentByDisciplineTableRowDTO> getTableByDiscipline(SearchCriteria searchCriteria, int draw) {

		Page<DisciplineOffer> page = this.enrollmentDAO.getEnabledByDisciplineAndFilter(searchCriteria.getFilter(),
				SEMESTER, YEAR, prepareCriteria(searchCriteria));
		TableDTO<EnrollmentByDisciplineTableRowDTO> table = new TableDTO<>();
		table.setDraw(draw);
		table.setRecordsTotal((int) disciplineOfferDAO.countByEnabledTrueAndSemesterAndYear(SEMESTER, YEAR));
		table.setRecordsFiltered((int) page.getTotalElements());

		table.setData(EnrollmentByDisciplineTableRowDTO.listFrom(page.getContent()));

		return table;
	}

	@Override
	public TableDTO<EnrollmentByStudentTableRowDTO> getTableByStudent(SearchCriteria searchCriteria, int draw) {

		Page<Student> page = this.enrollmentDAO.getEnabledByStudentAndFilter(searchCriteria.getFilter(), SEMESTER, YEAR,
				prepareCriteria(searchCriteria));
		TableDTO<EnrollmentByStudentTableRowDTO> table = new TableDTO<>();
		table.setDraw(draw);
		table.setRecordsTotal((int) studentDAO.countEnabledAndEnrolledIn(SEMESTER, YEAR));
		table.setRecordsFiltered(table.getRecordsTotal());

		table.setData(EnrollmentByStudentTableRowDTO.setFrom(page.getContent(), enrollmentDAO));

		return table;
	}

	@Override
	public List<DisciplineOfferEnrollmentDTO> findEnrollmentsForDiscipline(UUID disciplineOfferHash) {
		return DisciplineOfferEnrollmentDTO.listFrom(
				this.enrollmentDAO.getEnabledEnrollmentsForDiscipline(disciplineOfferHash), disciplineOfferService);
	}

	@Override
	public Set<EnrollmentByStudentDTO> findEnrollmentsForStudent(UUID studentHash) {
		return EnrollmentByStudentDTO.setFrom(this.enrollmentDAO.getEnabledEnrollmentsForStudent(studentHash,
				SemesterYearConfig.getPreEnrollSemester(), SemesterYearConfig.getPreEnrollYear()), disciplineOfferService);
	}

	@Override
	public List<EnrollmentByStudentDTO> findEnrollmentsForStudentAsList(UUID studentHash) {
		return EnrollmentByStudentDTO.listFrom(this.enrollmentDAO.getEnabledEnrollmentsForStudent(studentHash,
				SemesterYearConfig.getPreEnrollSemester(), SemesterYearConfig.getPreEnrollYear()), disciplineOfferService);
	}

	@Override
	public List<EnrollmentsByStudentDTO> getEnrollmentSummary() {

		List<Student> students = studentDAO.findAll();
		List<EnrollmentsByStudentDTO> enrollments = EnrollmentsByStudentDTO.listFrom(students, this);

		return enrollments;
	}

	@Override
	@Transactional
	public void save(EnrolledDTO enrolledDTO) {
		Optional<Student> studentOptional = this.studentDAO
				.findByHash(FriendlyId.toUuid(enrolledDTO.getStudent()));
		if (studentOptional.isPresent()) {
			Student student = studentOptional.get();

			List<DisciplineOffer> disciplineOffers = disciplineOfferDAO
					.findByEnabledTrueAndEnrolledBy(student.getId(), SEMESTER, YEAR);

			for (String disciplineHashString : enrolledDTO.getDisciplines()) {

				if (disciplineOffers
						.stream()
						.filter(dof -> disciplineHashString.equals(dof.getHashString()))
						.count() == 0) {

					Optional<DisciplineOffer> disciplineOfferOptional = this.disciplineOfferDAO
							.findByHash(FriendlyId.toUuid((disciplineHashString)));
					if (disciplineOfferOptional.isPresent()) {
						DisciplineOffer disciplineOffer = disciplineOfferOptional.get();

						disciplineOffer.addStudentEnrollment(student);

						disciplineOfferDAO.save(disciplineOffer);

					}

				}

			}

			// Remove.
			disciplineOffers.stream().forEach(dof -> {
				boolean found = false;
				for (int i = 0; i < enrolledDTO.getDisciplines().length; i++) {
					if (dof.getHashString().equals(enrolledDTO.getDisciplines()[i])) {
						found = true;
						break;
					}
				}

				if (!found) {
					dof.removeStudentEnrollment(student);
					Optional<StudentEnrollment> studentEnrollmentOptional = studentEnrollmentDAO
							.findByEnabledTrueAndStudentAndDisciplineOffer(student, dof);
					if (studentEnrollmentOptional.isPresent()) {
						studentEnrollmentDAO.delete(studentEnrollmentOptional.get());
					}
				}
			});

			Optional<Enrollment> enrollmentOptional = this.getByEnabledTrueAndStudent(student);
			if (!enrollmentOptional.isPresent()) {
				Enrollment enrollment = new Enrollment();
				enrollment.setStudent(student);
				enrollmentOptional = Optional.of(enrollment);
			}

			Enrollment enrollment = enrollmentOptional.get();
			enrollment.setCapstoneProject(enrolledDTO.getCapstoneProject());
			enrollment.setInternship(enrolledDTO.getInternship());
			enrollment.setSemester(SemesterYearConfig.getPreEnrollSemester());
			enrollment.setYear(SemesterYearConfig.getPreEnrollYear());
			this.update(enrollment);

			// Save student declaration file
			File file = null;
			if (enrolledDTO.getFile() != null && enrolledDTO.getFile().getSize() > 0) {
				file = fileService.saveFile(enrolledDTO.getFile(),
						String.valueOf(enrollment.getId()), "Student declaration file.");
				enrollment.setDeclarationFile(file);
				this.update(enrollment);
			}
		} else {
			throw new RuntimeException("Inexistent student");
		}
	}

}
