package br.com.fatecmogidascruzes.enrollment.viewmodel;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class EnrollmentConfigVM {

	@Min(1)
	@Max(2)
	@NotNull
	private Integer semester;
	@NotNull
	private Integer year;

	public EnrollmentConfigVM() {
	}

	public Integer getSemester() {
		return semester;
	}

	public void setSemester(Integer semester) {
		this.semester = semester;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}
