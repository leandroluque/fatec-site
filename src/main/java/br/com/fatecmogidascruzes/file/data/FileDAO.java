package br.com.fatecmogidascruzes.file.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.file.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDAO extends DAOImpl<File, Long>, JpaRepository<File, Long> {

}
