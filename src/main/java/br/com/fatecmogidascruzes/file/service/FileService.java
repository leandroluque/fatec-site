package br.com.fatecmogidascruzes.file.service;

import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.service.BaseService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService extends BaseService<File, Long> {

	File saveFile(MultipartFile file);

	File saveFile(MultipartFile multipartFile, String customName, String alternativeDescription);

	File copyFile(File file);

	File saveImage(MultipartFile multipartFile, String alternativeDescription) throws IOException;

	byte[] loadBytes(Long id);

	byte[] loadBytes(Long id, int width, int height);

}
