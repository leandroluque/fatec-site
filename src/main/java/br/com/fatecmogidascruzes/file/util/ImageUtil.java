package br.com.fatecmogidascruzes.file.util;

import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.ImageVariation;

import java.io.IOException;
import java.util.Set;

public interface ImageUtil {

	Set<ImageVariation> generateVariations(File file) throws IOException;

}