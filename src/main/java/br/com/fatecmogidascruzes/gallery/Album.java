package br.com.fatecmogidascruzes.gallery;

import br.com.fatecmogidascruzes.domain.NamedEntity;
import br.com.fatecmogidascruzes.event.Event;
import br.com.fatecmogidascruzes.file.File;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Access(AccessType.FIELD)
@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_album")
public class Album extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "description", nullable = true, length = 100)
	private String description;

	@JoinColumn(name = "event", nullable = true)
	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Event event;

	@JoinTable(name = "_album_images", joinColumns = @JoinColumn(name = "album"), inverseJoinColumns = @JoinColumn(name = "image"))
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<File> images;

	@JoinColumn(name = "cover", nullable = true)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private File cover;

	@Basic
	@Column(name = "is_fatec_album", nullable = false)
	private boolean fatecAlbum;

	@Access(AccessType.PROPERTY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_album")
	@Id
	@Override
	@SequenceGenerator(name = "seq_album", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public void setImages(File... images) {
		this.images = new HashSet<>(Arrays.asList(images));
	}

	public void addImage(File image) {
		this.images.add(image);
	}

	public Optional<File> getImage(UUID imageHash) {
		return this.images.parallelStream().filter(image -> imageHash.equals(image.getHash())).findFirst();
	}

	public Optional<File> getImage(String imageHash) {
		return this.images.parallelStream().filter(image -> imageHash.equals(image.getHashString())).findFirst();
	}

	public void removeImage(UUID imageHash) {
		this.images.removeIf(file -> imageHash.equals(file.getHash()));
	}

}
