package br.com.fatecmogidascruzes.gallery;

import br.com.fatecmogidascruzes.service.BaseService;

import java.util.List;

public interface AlbumService extends BaseService<Album, Long> {

	List<Album> getEnabledByFilter(String filter);

}
