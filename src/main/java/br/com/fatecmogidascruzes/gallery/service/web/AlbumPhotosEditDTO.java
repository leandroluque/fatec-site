package br.com.fatecmogidascruzes.gallery.service.web;

import br.com.fatecmogidascruzes.gallery.Album;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class AlbumPhotosEditDTO {

	private String hashString;
	private MultipartFile[] images;

	public AlbumPhotosEditDTO(String hashString) {
		super();
		this.hashString = hashString;
	}

	public static AlbumPhotosEditDTO from(Album album) {
		AlbumPhotosEditDTO albumPhotosEditDTO = new AlbumPhotosEditDTO();
		albumPhotosEditDTO.setHashString(FriendlyId.toFriendlyId(album.getHash()));

		return albumPhotosEditDTO;
	}

}
