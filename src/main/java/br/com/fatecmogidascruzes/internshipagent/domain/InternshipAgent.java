package br.com.fatecmogidascruzes.internshipagent.domain;

import br.com.fatecmogidascruzes.domain.NamedEntity;

import javax.persistence.*;

@Entity
@Table(name = "_internship_agent", indexes = { @Index(name = "ind_internship_agent_name", columnList = "name", unique = false) })
public class InternshipAgent extends NamedEntity {
	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_internship_agent")
	@Id
	@Override
	@SequenceGenerator(name = "seq_internship_agent", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}
	public InternshipAgent() {
    }
	public InternshipAgent(String name) {
		super(name);
	}
}