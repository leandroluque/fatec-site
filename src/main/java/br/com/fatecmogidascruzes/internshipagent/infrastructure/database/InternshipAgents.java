package br.com.fatecmogidascruzes.internshipagent.infrastructure.database;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InternshipAgents extends DAOImpl<InternshipAgent, Long>, JpaRepository<InternshipAgent, Long> {

    List<InternshipAgent> findByEnabledTrueOrderByName();
}