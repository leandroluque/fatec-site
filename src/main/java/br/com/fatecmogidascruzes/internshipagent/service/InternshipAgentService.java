package br.com.fatecmogidascruzes.internshipagent.service;

import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;
import br.com.fatecmogidascruzes.service.BaseService;

import java.util.List;

public interface InternshipAgentService extends BaseService<InternshipAgent, Long> {

    List<InternshipAgent> getEnabledSortedByName();
}
