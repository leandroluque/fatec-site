package br.com.fatecmogidascruzes.internshipagent.service;

import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;
import br.com.fatecmogidascruzes.internshipagent.infrastructure.database.InternshipAgents;
import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service public class InternshipAgentServiceImpl extends BaseServiceImpl<InternshipAgent, Long> implements InternshipAgentService {
    private final InternshipAgents internshipAgents;
    public InternshipAgentServiceImpl(InternshipAgents internshipAgents) {
        super(internshipAgents);
        this.internshipAgents = internshipAgents;
    }
    public List<InternshipAgent> getEnabledSortedByName() {
        return internshipAgents.findByEnabledTrueOrderByName();
    }
}
