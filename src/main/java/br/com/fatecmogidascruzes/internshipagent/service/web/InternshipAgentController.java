package br.com.fatecmogidascruzes.internshipagent.service.web;

import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/internshipAgents")
public class InternshipAgentController extends MVCController {

    private final InternshipAgentWebService webService;

    @Autowired
    public InternshipAgentController(HttpSession httpSession, UserService userService, InternshipAgentWebService webService) {
        super(httpSession, userService);
        this.webService = webService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATIVE')")
    public ModelAndView newEmployee(@ModelAttribute("internshipAgents") InternshipAgentsVM viewModel,
                                    RedirectAttributes redirectAttributes) {
        if(viewModel.isEmpty()) {
            viewModel.fillFrom(webService.getEnabled());
        }
        return new ModelAndView("/internshipAgent/edit");
    }

    @PostMapping("save")
    public ModelAndView save(@Valid @ModelAttribute("internshipAgents") InternshipAgentsVM viewModel,
                             BindingResult binding,
                             RedirectAttributes redirectAttributes) {
        if (binding.hasErrors()) {
            redirectAttributes.addFlashAttribute("message", "error");
        } else {
            try {
                webService.save(viewModel);
                redirectAttributes.addFlashAttribute("message", "success");
            } catch (Exception error) {
                error.printStackTrace();
                redirectAttributes.addFlashAttribute("message", "error");
            }
        }
        return new ModelAndView("redirect:/admin/internshipAgents");
    }
}