package br.com.fatecmogidascruzes.internshipagent.service.web;

import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;

import java.util.List;

public interface InternshipAgentWebService {
    void save(InternshipAgentsVM internshipAgentsVM);
    List<InternshipAgent> getEnabled();
}
