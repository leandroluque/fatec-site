package br.com.fatecmogidascruzes.internshipagent.service.web;

import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;
import br.com.fatecmogidascruzes.internshipagent.service.InternshipAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service public class InternshipAgentWebServiceImpl implements InternshipAgentWebService {

    private final InternshipAgentService internshipAgentService;

    @Autowired
    public InternshipAgentWebServiceImpl(InternshipAgentService internshipAgentService) {
        this.internshipAgentService = internshipAgentService;
    }

    @Override
    @Transactional
    public void save(InternshipAgentsVM internshipAgentsVM) {
        List<InternshipAgent> databaseInternshipAgents = internshipAgentService.getEnabled();
        List<InternshipAgent> modifiedInternshipAgents = new ArrayList<>(databaseInternshipAgents);
        internshipAgentsVM.fill(modifiedInternshipAgents);

        // Remove internship agents
        databaseInternshipAgents.stream()
                .filter(internshipAgent -> !modifiedInternshipAgents.contains(internshipAgent))
                .forEach(this.internshipAgentService::remove);

        // Add new internship agents
        modifiedInternshipAgents.stream()
                .filter(internshipAgent -> internshipAgent.getId() == null)
                .forEach(this.internshipAgentService::save);
    }

    @Override
    public List<InternshipAgent> getEnabled() {
        return this.internshipAgentService.getEnabled();
    }
}
