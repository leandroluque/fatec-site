package br.com.fatecmogidascruzes.internshipagent.service.web;

import br.com.fatecmogidascruzes.internshipagent.domain.InternshipAgent;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class InternshipAgentsVM {

    private String[] friendlyHashes;
    private String[] names;

    public boolean isEmpty() {
        return friendlyHashes == null || friendlyHashes.length == 0;
    }
    public static InternshipAgentsVM from(List<InternshipAgent> internshipAgents) {
        InternshipAgentsVM viewModel = new InternshipAgentsVM();
        viewModel.friendlyHashes = new String[internshipAgents.size()];
        viewModel.names = new String[internshipAgents.size()];
        int i = 0;
        for (InternshipAgent internshipAgent : internshipAgents) {
            viewModel.friendlyHashes[i] = internshipAgent.getHashString();
            viewModel.names[i] = internshipAgent.getName();
            i++;
        }
        return viewModel;
    }

    public void fillFrom(List<InternshipAgent> internshipAgents) {
        this.friendlyHashes = new String[internshipAgents.size()];
        this.names = new String[internshipAgents.size()];
        int i = 0;
        for (InternshipAgent internshipAgent : internshipAgents) {
            this.friendlyHashes[i] = internshipAgent.getHashString();
            this.names[i] = internshipAgent.getName();
            i++;
        }
    }

    public void fill(List<InternshipAgent> internshipAgents) {
        // Create a set of hashes to remove from the objects list
        Set<String> hashesToRemove = Arrays.stream(friendlyHashes)
                .filter(hash -> !hash.equals("new"))
                .collect(Collectors.toSet());

        // Remove objects with hashes not present in the hashes array
        internshipAgents.removeIf(obj -> !hashesToRemove.contains(obj.getHashString()));

        // Update objects
        for (int i = 0; i < friendlyHashes.length; i++) {
            if (!friendlyHashes[i].equals("new") && !names[i].isEmpty()) {
                final String currentFriendlyHash = friendlyHashes[i];
                final String newName = names[i];
                internshipAgents.stream()
                        .filter(obj -> obj.getHashString().equals(currentFriendlyHash))
                        .forEach(obj -> obj.setName(newName));
            }
        }

        // Add new objects for which the hash entry in the array is equal "new"
        for (int i = 0; i < friendlyHashes.length; i++) {
            if (friendlyHashes[i].equals("new") && !names[i].isEmpty()) {
                internshipAgents.add(new InternshipAgent(names[i]));
            }
        }
    }

    public String[] getFriendlyHashes() {
        return friendlyHashes;
    }

    public void setFriendlyHashes(String[] friendlyHashes) {
        this.friendlyHashes = friendlyHashes;
    }

    public String[] getNames() {
        return names;
    }

    public void setNames(String[] names) {
        this.names = names;
    }
}