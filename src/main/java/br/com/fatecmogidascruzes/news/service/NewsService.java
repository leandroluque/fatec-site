package br.com.fatecmogidascruzes.news.service;

import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.news.News;
import br.com.fatecmogidascruzes.service.BaseService;
import org.springframework.data.domain.Page;

import java.util.List;

public interface NewsService extends BaseService<News, Long> {

	Page<News> getEnabledByFilter(SearchCriteria searchCriteria);

	List<News> getEnabledVisibleAndHighlighted();
	
	List<News> getEnabledAndVisibleHomeNews();
	
	List<News> getEnabledAndVisibileOrderedByReferenceDateDesc();

}
