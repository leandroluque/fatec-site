package br.com.fatecmogidascruzes.news.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MustHaveImageValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MustHaveImage {

	String message() default "A imagem é obrigatória para uma notícia";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
