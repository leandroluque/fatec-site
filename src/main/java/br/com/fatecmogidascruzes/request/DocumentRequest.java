package br.com.fatecmogidascruzes.request;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fatecmogidascruzes.domain.EntityImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Entity
@Setter
@Table(name = "_document_request")
@AllArgsConstructor
@NoArgsConstructor
public class DocumentRequest extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@Column(name = "document_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentType documentType;

	@Column(name = "request_situation", nullable = false)
	@Enumerated(EnumType.STRING)
	private RequestSituation requestSituation;

	@ManyToOne
	private Request request;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_document_request")
	@Id
	@Override
	@SequenceGenerator(name = "seq_document_request", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}