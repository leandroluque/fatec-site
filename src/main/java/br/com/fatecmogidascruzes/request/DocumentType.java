package br.com.fatecmogidascruzes.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DocumentType {
	SIMPLE_ATTENDANCE("Atestado de Matrícula Simples",
			"Contém nome, registro acadêmico, curso e turno de ingresso. Prazo: 05 dias."),
	COMPLETE_ATTENDANCE("Atestado de Matrícula com Disciplina e Horário",
			"Contém nome, registro acadêmico, curso, turno e grade de horário das disciplinas matriculadas no semestre. Prazo: 05 dias."),
	ATTENDANCE("Atestado de Matrícula de Frequência",
			"Contém nome, registro acadêmico, curso, turno e frequência (%) mensal das disciplinas matriculadas no semestre. Prazo: 10 dias."),
	TRANSPORTATION_REGISTRATION("Cadastro de Aluno para Passe Escolar - EMTU",
			"Solicitação de cadastro para utilização do ônibus intermunicipal. Apenas para alunos que residem fora de Mogi das Cruzes. Prazo: 05 dias."),
	TRANSPORTATION_REGISTRATION_SP_TRANS("Cadastro de Aluno para Passe Escolar - SPTRANS",
			"Solcitação de cadastro para utilização do trem/metro. Apenas para alunos que residem fora de Mogi das Cruzes. Prazo: 05 dias."),
	MOGI_PASSES_DOCUMENT("Comprovante de Matricula - Mogi Passes",
			"Contém nome, registro acadêmico, curso, turno e grade de horário das disciplinas matriculadas no semestre. Apenas para alunos que residem em Mogi das Cruzes. Prazo: 05 dias."),
	DISCIPLINES_CONTENT("Conteúdo Programático",
			"Ementas de todas disciplinas cursadas e aprovadas na unidade. Prazo: 10 dias."),
	INTERNSHIP("Declaração para Estágio",
			"Contém nome, registro acadêmico, curso, turno e grade de horário das disciplinas matriculadas no semestre e prazo para término do curso. Prazo: 05 dias."),
	WITHDRAWAL_DISCIPLINE("Desistência de Disciplina",
			"Solicitação para desistência de uma disciplina no semestre. Apenas para alunos Veteranos. Informar a(s) disciplina(s) no campo de observações. Prazo: 03 dias."),
	ACADEMIC_HISTORIC("Histórico Escolar",
			"Contém todas as disciplinas cursadas e aprovadas na unidade. Prazo: 10 dias."),
	ID_SOLICITATION("Solicitação de Carteirinha",
			"Solicitação para emissão de uma nova carteirinha. Em caso de perda, será necessário pagar uma taxa. Prazo: 30 dias."),
	WITHDRAW_COURSE("Trancamento de Matrícula",
			"Solicitação para trancar o semestre. Apenas para alunos Veteranos. Informar o motivo do trancamento no campo de observações. Prazo: 03 dias."),
	CANCEL_APPLICATION("Cancelamento de Matrícula",
			"Solicitação para cancelar a matrícula. Prazo: 03 dias."),
	OTHER("Outros Documentos", "Especificar o documento em detalhes no campo de observações.");

	private String name;
	private String description;
}
