package br.com.fatecmogidascruzes.request;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Entity
@Setter
@Table(name = "_request")
public class Request extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "registry", nullable = false, length = 30)
	private String registry;

	@Basic
	@Column(name = "email", nullable = false, length = 50)
	private String email;

	@Basic
	@Column(name = "phone", nullable = false, length = 50)
	private String phone;

	@AllArgsConstructor
	@Getter
	public enum LearnerSituation {
		CURRENT("Matriculado"), INTERRUPTED("Matrícula Trancada"), FINISHED("Ex-Aluno");

		private String name;
	}

	@Column(name = "learner_situation", nullable = false)
	@Enumerated(EnumType.STRING)
	private LearnerSituation learnerSituation;

	@JoinColumn(name = "course")
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Course course;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "request")
	private Set<DocumentRequest> requests;

	@Column(name = "withdraw_discipline_description", nullable = true, length = 1000)
	private String withdrawDisciplineDescription;

	@Column(name = "withdraw_course_description", nullable = true, length = 1000)
	private String withdrawCourseDescription;

	@Column(name = "cancel_application_description", nullable = true, length = 1000)
	private String cancelApplicationDescription;

	@Basic
	@Column(name = "comments", nullable = true, length = 1000)
	private String comments;

	@Basic
	@Column(name = "protocol", nullable = false)
	private Long protocol = System.currentTimeMillis();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_request")
	@Id
	@Override
	@SequenceGenerator(name = "seq_request", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public void addDocumentRequest(DocumentType type, RequestSituation situation) {
		if (null == requests) {
			requests = new HashSet<>();
		}
		if(null == situation) {
			situation = RequestSituation.RECEIVED;
		}
		requests.add(new DocumentRequest(type, situation, this));
	}

	public void removeDocumentRequest(DocumentType type) {
		if (null == requests)
			return;
		requests.removeIf(it -> type.equals(it.getDocumentType()));
	}

	public boolean areAllRequestsFinished() {
		boolean allFinished = true;
		if (null != requests) {
			for (DocumentRequest docReq : requests) {
				if (docReq.getRequestSituation() != RequestSituation.FINISHED) {
					allFinished = false;
					break;
				}
			}
		}
		return allFinished;
	}
	
	public boolean areAllRequestsOnlyReceived() {
		boolean allOnlyReceived = true;
		if (null != requests) {
			for (DocumentRequest docReq : requests) {
				if (docReq.getRequestSituation() != RequestSituation.RECEIVED) {
					allOnlyReceived = false;
					break;
				}
			}
		}
		return allOnlyReceived;
	}

}