package br.com.fatecmogidascruzes.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RequestSituation {
	RECEIVED("Recebido"), UNDER_PREPARATION("Em Preparação"), FINISHED("Pronto");

	private String name;
}