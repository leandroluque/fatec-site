package br.com.fatecmogidascruzes.request.service;

import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.request.Request;
import br.com.fatecmogidascruzes.service.BaseService;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface RequestService extends BaseService<Request, Long> {

	Page<Request> getEnabledByFilter(SearchCriteria searchCriteria);

	Optional<Request> getEnabledByProtocol(Long protocol);

}