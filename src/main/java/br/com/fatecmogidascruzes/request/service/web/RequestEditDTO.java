package br.com.fatecmogidascruzes.request.service.web;

import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.dto.BaseEditDTO;
import br.com.fatecmogidascruzes.request.DocumentRequest;
import br.com.fatecmogidascruzes.request.DocumentType;
import br.com.fatecmogidascruzes.request.Request;
import br.com.fatecmogidascruzes.request.Request.LearnerSituation;
import br.com.fatecmogidascruzes.request.RequestSituation;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Setter
public class RequestEditDTO extends BaseEditDTO {
	private static final String DEFAULT_REGISTRY = "0000000000000";

	@Length(max = 30, message = "O RA do aluno requerente pode ter no máximo 30 caracteres")
	private String registry;

	@NotBlank(message = "O nome do aluno requerente é obrigatório")
	@Length(max = 100, message = "O nome do aluno requerente pode ter no máximo 100 caracteres")
	private String name;

	@NotBlank(message = "O telefone do aluno requerente é obrigatório")
	@Length(max = 50, message = "O telefone do aluno requerente pode ter no máximo 50 caracteres")
	private String phone;

	@NotBlank(message = "O e-mail do aluno requerente é obrigatório")
	@Length(max = 50, message = "O e-mail do aluno requerente pode ter no máximo 50 caracteres")
	private String email;

	@Length(max = 1000, message = "Os detalhes sobre a sua requisição podem ter no máximo 400 caracteres")
	private String cancelApplicationDescription;

	@Length(max = 1000, message = "Os detalhes sobre a sua requisição podem ter no máximo 400 caracteres")
	private String withdrawDisciplineDescription;

	@Length(max = 1000, message = "Os detalhes sobre a sua requisição podem ter no máximo 400 caracteres")
	private String withdrawCourseDescription;

	@Length(max = 400, message = "Os comentários sobre a requisição podem ter no máximo 400 caracteres")
	private String comments;

	@NotNull(message = "O curso do aluno é obrigatório")
	private String course;

	@NotNull(message = "O tipo de requerente é obrigatório")
	private LearnerSituation learnerSituation;

	private Long protocol;

	private DocumentType[] documentTypes;
	private RequestSituation[] requestSituations;

	public boolean isDocumentTypeChecked(DocumentType documentType) {
		if (null == this.documentTypes)
			return false;
		for (DocumentType type : this.documentTypes) {
			if (documentType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	public RequestSituation getSituationFor(DocumentType documentType) {
		if (null == this.documentTypes)
			return null;
		int i = 0;
		for (DocumentType type : this.documentTypes) {
			if (documentType.equals(type)) {
				System.out.println("Found at " + i + ". Size of situations: " +requestSituations.length);
				return requestSituations[i];
			}
			i++;
		}
		return null;
	}

	public static RequestEditDTO from(Request request) {
		RequestEditDTO requestEditDTO = new RequestEditDTO();

		requestEditDTO.setHashString(FriendlyId.toFriendlyId(request.getHash()));
		requestEditDTO.setRegistry(request.getRegistry());
		requestEditDTO.setName(request.getName());
		requestEditDTO.setComments(request.getComments());
		requestEditDTO.setCourse(request.getCourse().getHashString());
		requestEditDTO.setLearnerSituation(request.getLearnerSituation());
		requestEditDTO.setRequestSituations(request.getRequests().stream().map(DocumentRequest::getRequestSituation)
				.toArray(RequestSituation[]::new));
		requestEditDTO.setPhone(request.getPhone());
		requestEditDTO.setEmail(request.getEmail());
		requestEditDTO.setDocumentTypes(
				request.getRequests().stream().map(DocumentRequest::getDocumentType).toArray(DocumentType[]::new));
		requestEditDTO.setProtocol(request.getProtocol());
		requestEditDTO.setCancelApplicationDescription(request.getCancelApplicationDescription());
		requestEditDTO.setWithdrawDisciplineDescription(request.getWithdrawDisciplineDescription());
		requestEditDTO.setWithdrawCourseDescription(request.getWithdrawCourseDescription());

		return requestEditDTO;
	}

	public void fill(Request request, CourseService courseService) {

		request.setRegistry(getDefaultRegistryOrValue());
		request.setName(name);
		request.setComments(comments);
		request.setCourse(courseService.getByHash(FriendlyId.toUuid(course)).get());
		request.setLearnerSituation(learnerSituation);

		request.setCancelApplicationDescription(null);
		request.setWithdrawDisciplineDescription(null);
		request.setWithdrawCourseDescription(null);
		for(DocumentType documentType : documentTypes) {
			if(documentType == DocumentType.WITHDRAWAL_DISCIPLINE) {
				request.setWithdrawDisciplineDescription(withdrawDisciplineDescription);
			} else if(documentType == DocumentType.WITHDRAW_COURSE) {
				request.setWithdrawCourseDescription(withdrawCourseDescription);
			} else if(documentType == DocumentType.CANCEL_APPLICATION) {
				request.setCancelApplicationDescription(cancelApplicationDescription);
			}
		}

		Set<DocumentType> typesToRemove = new HashSet<>();
		if (null != request.getRequests()) {
			for (DocumentRequest docRequest : request.getRequests()) {
				typesToRemove.add(docRequest.getDocumentType());
			}
		}
		int i = 0;
		for (DocumentType docType : documentTypes) {
			RequestSituation situation = RequestSituation.RECEIVED;
			if (null != requestSituations && requestSituations.length > i) {
				situation = requestSituations[i];
			}
			typesToRemove.remove(docType);
			boolean found = false;
			if (null != request.getRequests()) {
				for (DocumentRequest docRequest : request.getRequests()) {
					if (docType.equals(docRequest.getDocumentType())) {
						docRequest.setRequestSituation(situation);
						found = true;
						break;
					}
				}
			}
			if (!found) {
				request.addDocumentRequest(docType, situation);
			}

			i++;
		}
		for (DocumentType toRemove : typesToRemove) {
			request.removeDocumentRequest(toRemove);
		}

		request.setPhone(phone);
		request.setEmail(email);
	}

	public String getDefaultRegistryOrValue() {
		return (null != registry && !registry.trim().isEmpty()) ? registry : DEFAULT_REGISTRY;
	}

}
