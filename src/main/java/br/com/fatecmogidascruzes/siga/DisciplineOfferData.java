package br.com.fatecmogidascruzes.siga;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Esta classe armazena dados extraídos de listas de médias finais e faltas
 * geradas a partir do SIGA-CPS como arquivos PDF.
 *
 * @author Leandro Luque
 */
public class DisciplineOfferData implements Iterable<StudentData> {

    private int semester;
    private int year;
    private String courseName;
    private String shift;
    private String disciplineCode;
    private String disciplineName;
    private int totalClasses;
    private String classLetter;
    private String professor;
    private List<StudentData> studentData = new ArrayList<>();

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semestre) {
        this.semester = semestre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int ano) {
        this.year = ano;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String curso) {
        this.courseName = curso.trim();
    }

    public String getDisciplineCode() {
        return disciplineCode;
    }

    public void setDisciplineCode(String codigoDisciplina) {
        this.disciplineCode = codigoDisciplina.trim();
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplina) {
        this.disciplineName = disciplina.trim();
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor.trim();
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String turno) {
        this.shift = turno.trim();
    }

    public int getTotalClasses() {
        return totalClasses;
    }

    public void setTotalClasses(int totalAulas) {
        this.totalClasses = totalAulas;
    }

    public String getClassLetter() {
        return classLetter;
    }

    public void setClassLetter(String turma) {
        this.classLetter = turma.trim();
    }

    public List<StudentData> getStudentData() {
        return studentData;
    }

    public void setStudentData(List<StudentData> dadosAlunos) {
        this.studentData = dadosAlunos;
    }

    public void addDadoAluno(String ra, String nome, String situacao, Double mediaFinal, Integer falta) {
        this.studentData.add(new StudentData(ra, nome, situacao, mediaFinal, falta));
    }

    @Override
    public Iterator<StudentData> iterator() {
        return this.studentData.iterator();
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();
        retorno.append(courseName);
        retorno.append(";");
        retorno.append(";");
        retorno.append(shift);
        retorno.append(classLetter);
        retorno.append(";");
        retorno.append(semester);
        retorno.append(";");
        retorno.append(year);
        retorno.append(";");
        retorno.append(disciplineName);
        retorno.append(";");
        retorno.append(professor);
        retorno.append(";");
        retorno.append(totalClasses);
        retorno.append(";");
        retorno.append(totalClasses * 0.25);

        retorno.append(System.getProperty("line.separator"));
        retorno.append(System.getProperty("line.separator"));
        for (StudentData dadoAluno : this) {
            retorno.append(dadoAluno.getRegistry());
            retorno.append(" ");
            retorno.append(dadoAluno.getName());
            retorno.append(" ");
            retorno.append(dadoAluno.getSituation());
            retorno.append(" ");
            retorno.append(String.format("%.2f", dadoAluno.getFinalGrade()));
            retorno.append(" ");
            retorno.append(dadoAluno.getAbsences());
            retorno.append(System.getProperty("line.separator"));
        }
        return retorno.toString();
    }

    public static String getCabecalhoLinha() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("CURSO");
        retorno.append(";");
        retorno.append("TURNO");
        retorno.append(";");
        retorno.append("TURMA");
        retorno.append(";");
        retorno.append("SEMESTRE");
        retorno.append(";");
        retorno.append("ANO");
        retorno.append(";");
        retorno.append("COD_DISC");
        retorno.append(";");
        retorno.append("DISC");
        retorno.append(";");
        retorno.append("PROFESSOR");
        retorno.append(";");
        retorno.append("TOTAL_AULAS");
        retorno.append(";");
        retorno.append("LIM_FALTAS");
        retorno.append(";");
        retorno.append("QTD_ALUNOS");
        retorno.append(";");
        retorno.append("QTD_ATE_FIM");
        retorno.append(";");
        retorno.append("APROVAD");
        retorno.append(";");
        retorno.append("FRA_APROVAD"); // Porcentagem de aprovados.
        retorno.append(";");
        retorno.append("MED_NOTA_APROVAD");
        retorno.append(";");
        retorno.append("REPROVAD");
        retorno.append(";");
        retorno.append("FRA_REPROVAD"); // Porcentagem de reprovados.
        retorno.append(";");
        retorno.append("MED_NOTA_REPROVAD");
        retorno.append(";");
        retorno.append("REPROVAD_FALTAS");
        retorno.append(";");
        retorno.append("FRA_REPROVAD_FALTAS");
        retorno.append(";");
        retorno.append("MED_NOTA_REPROVAD_FALTAS");
        retorno.append(";");
        retorno.append("REPROVAD_NOTA");
        retorno.append(";");
        retorno.append("FRA_REPROVAD_NOTA");
        retorno.append(";");
        retorno.append("MED_NOTA_REPROVAD_NOTA");
        retorno.append(";");
        retorno.append("PUL_FORA");
        retorno.append(";");
        retorno.append("FRA_PUL_FORA");
        retorno.append(";");
        retorno.append("TRANCARAM");
        retorno.append(";");
        retorno.append("CANCELARAM");
        retorno.append(";");
        retorno.append("DESISTIRAM");
        retorno.append(";");
        retorno.append("MENOR_NOTA");
        retorno.append(";");
        retorno.append("MEDIA_NOTA");
        retorno.append(";");
        retorno.append("MAIOR_NOTA");
        retorno.append(";");

        return retorno.toString();
    }

    public String resumoLinha() {
        StringBuilder retorno = new StringBuilder();
        retorno.append(courseName);
        retorno.append(";");
        retorno.append(shift);
        retorno.append(";");
        retorno.append(classLetter);
        retorno.append(";");
        retorno.append(semester);
        retorno.append(";");
        retorno.append(year);
        retorno.append(";");
        retorno.append(disciplineCode);
        retorno.append(";");
        retorno.append(disciplineName);
        retorno.append(";");
        retorno.append(professor);
        retorno.append(";");
        retorno.append(totalClasses);
        retorno.append(";");
        int limiteFaltas = (int) (totalClasses * 0.25);
        retorno.append(limiteFaltas);
        retorno.append(";");

        int totalAprovados = 0;
        int reprovadosPorFalta = 0;
        int reprovadosPorNota = 0;
        int totalReprovados = 0;
        int trancamentos = 0;
        int cancelamentos = 0;
        int desistentes = 0;

        int totalUtil = 0;
        double mediaNotaAprovados = 0;
        double mediaNotaReprovadosGeral = 0;
        double mediaNotaReprovadosFalta = 0;
        double mediaNotaReprovadosNota = 0;

        double menorNota = Double.MAX_VALUE;
        double maiorNota = Double.MIN_VALUE;
        double mediaNota = 0;
        for (StudentData aluno : this) {
            // Se o aluno não foi dispensado (Aproveitamento de Estudos/Dispensado Proficiência), conta ele
            // como sendo um aluno útil para estatísticas.
            if (aluno.getFinalGrade() != null && !aluno.getSituation().equals("AE") && !aluno.getSituation().equals("EP") && !aluno.getSituation().equals("DM") && !aluno.getSituation().equals("CM") && !aluno.getSituation().equals("TM")) {
                totalUtil++;

                try {
                    mediaNota += aluno.getFinalGrade();
                } catch (Exception erro) {
                    //System.out.println(courseName);
                    //System.out.println(semester + "/" + year);
                    //System.out.println(disciplineName);
                    //System.out.println(shift);
                    //System.out.println(aluno);
                    System.exit(-1);
                }
                if (aluno.getFinalGrade() > maiorNota) {
                    maiorNota = aluno.getFinalGrade();
                }
                if (aluno.getFinalGrade() < menorNota) {
                    menorNota = aluno.getFinalGrade();
                }

                if (aluno.getAbsences() != null && aluno.getAbsences() > limiteFaltas) {
                    reprovadosPorFalta++;
                    mediaNotaReprovadosFalta += aluno.getFinalGrade();
                    mediaNotaReprovadosGeral += aluno.getFinalGrade();
                } else if (aluno.getFinalGrade() != null && aluno.getFinalGrade() < 6) {
                    reprovadosPorNota++;
                    mediaNotaReprovadosNota += aluno.getFinalGrade();
                    mediaNotaReprovadosGeral += aluno.getFinalGrade();
                }
            } else if (aluno.getSituation().equals("DM")) {
                desistentes++;
            } else if (aluno.getSituation().equals("CM")) {
                cancelamentos++;
            } else if (aluno.getSituation().equals("TM")) {
                trancamentos++;
            }
        }
        totalReprovados = reprovadosPorFalta + reprovadosPorNota;
        totalAprovados = totalUtil - totalReprovados;

        mediaNota /= totalUtil;
        if (totalAprovados == 0) {
            mediaNotaAprovados = 0;
        } else {
            mediaNotaAprovados /= totalAprovados;
        }
        if (totalReprovados == 0) {
            mediaNotaReprovadosGeral = 0;
        } else {
            mediaNotaReprovadosGeral /= totalReprovados;
        }
        if (reprovadosPorFalta == 0) {
            mediaNotaReprovadosFalta = 0;
        } else {
            mediaNotaReprovadosFalta /= reprovadosPorFalta;
        }
        if (reprovadosPorNota == 0) {
            mediaNotaReprovadosNota = 0;
        } else {
            mediaNotaReprovadosNota /= reprovadosPorNota;
        }

        retorno.append(this.studentData.size());
        retorno.append(";");
        retorno.append(totalUtil);
        retorno.append(";");
        retorno.append(totalAprovados);
        retorno.append(";");
        retorno.append(String.format("%.2f", totalAprovados / (double) totalUtil)); // Porcentagem de aprovados.
        retorno.append(";");
        retorno.append(String.format("%.2f", mediaNotaAprovados));
        retorno.append(";");
        retorno.append(totalReprovados);
        retorno.append(";");
        retorno.append(String.format("%.2f", totalReprovados / (double) totalUtil)); // Porcentagem de reprovados.
        retorno.append(";");
        retorno.append(String.format("%.2f", mediaNotaReprovadosGeral));
        retorno.append(";");
        retorno.append(reprovadosPorFalta);
        retorno.append(";");
        retorno.append(String.format("%.2f", reprovadosPorFalta / (double) totalUtil)); // Porcentagem de reprovados.
        retorno.append(";");
        retorno.append(String.format("%.2f", mediaNotaReprovadosFalta));
        retorno.append(";");
        retorno.append(reprovadosPorNota);
        retorno.append(";");
        retorno.append(String.format("%.2f", reprovadosPorNota / (double) totalUtil)); // Porcentagem de reprovados.
        retorno.append(";");
        retorno.append(String.format("%.2f", mediaNotaReprovadosNota));
        retorno.append(";");
        retorno.append(trancamentos + cancelamentos + desistentes);
        retorno.append(";");
        retorno.append(String.format("%.2f", (trancamentos + cancelamentos + desistentes) / (double) this.studentData.size())); // Porcentagem de reprovados.
        retorno.append(";");
        retorno.append(trancamentos);
        retorno.append(";");
        retorno.append(cancelamentos);
        retorno.append(";");
        retorno.append(desistentes);
        retorno.append(";");
        retorno.append(String.format("%.2f", menorNota));
        retorno.append(";");
        retorno.append(String.format("%.2f", mediaNota));
        retorno.append(";");
        retorno.append(String.format("%.2f", maiorNota));
        retorno.append(";");

        return retorno.toString();
    }

    public static String getCabecalhoLinhaAluno() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("CURSO");
        retorno.append(";");
        retorno.append("TURNO");
        retorno.append(";");
        retorno.append("TURMA");
        retorno.append(";");
        retorno.append("SEMESTRE");
        retorno.append(";");
        retorno.append("ANO");
        retorno.append(";");
        retorno.append("COD_DISC");
        retorno.append(";");
        retorno.append("DISC");
        retorno.append(";");
        retorno.append("PROFESSOR");
        retorno.append(";");
        retorno.append("TOTAL_AULAS");
        retorno.append(";");
        retorno.append("LIM_FALTAS");
        retorno.append(";");
        retorno.append("RA");
        retorno.append(";");
        retorno.append("NOME");
        retorno.append(";");
        retorno.append("SITUACAO");
        retorno.append(";");
        retorno.append("MEDIA");
        retorno.append(";");
        retorno.append("FALTAS");

        return retorno.toString();
    }
    public String resumoLinhaPorAluno() {
        StringBuilder retorno = new StringBuilder();
        for (StudentData aluno : this) {
            retorno.append(courseName);
            retorno.append(";");
            retorno.append(shift);
            retorno.append(";");
            retorno.append(classLetter);
            retorno.append(";");
            retorno.append(semester);
            retorno.append(";");
            retorno.append(year);
            retorno.append(";");
            retorno.append(disciplineCode);
            retorno.append(";");
            retorno.append(disciplineName);
            retorno.append(";");
            retorno.append(professor);
            retorno.append(";");
            retorno.append(totalClasses);
            retorno.append(";");
            int limiteFaltas = (int) (totalClasses * 0.25);
            retorno.append(limiteFaltas);
            retorno.append(";");
            retorno.append(aluno.getRegistry());
            retorno.append(";");
            retorno.append(aluno.getName());
            retorno.append(";");
            retorno.append(aluno.getSituation());
            retorno.append(";");
            retorno.append(aluno.getFinalGrade());
            retorno.append(";");
            retorno.append(aluno.getAbsences());
            retorno.append(System.getProperty("line.separator"));
        }

        return retorno.toString();
    }

    public String multilineRep() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("Semestre: ");
        retorno.append(semester);
        retorno.append("/");
        retorno.append(year);
        retorno.append(System.getProperty("line.separator"));
        retorno.append("Curso: ");
        retorno.append(courseName);
        retorno.append(" (");
        retorno.append(shift);
        retorno.append(")");
        retorno.append(System.getProperty("line.separator"));
        retorno.append("Disciplina: ");
        retorno.append(disciplineName);
        retorno.append(" (");
        retorno.append(classLetter);
        retorno.append("): ");
        retorno.append(totalClasses);
        retorno.append(System.getProperty("line.separator"));
        retorno.append("Professor: ");
        retorno.append(professor);
        retorno.append(System.getProperty("line.separator"));
        retorno.append(System.getProperty("line.separator"));
        for (StudentData dadoAluno : this) {
            retorno.append(dadoAluno.getRegistry());
            retorno.append(" ");
            retorno.append(dadoAluno.getName());
            retorno.append(" ");
            retorno.append(dadoAluno.getSituation());
            retorno.append(" ");
            retorno.append(String.format("%.2f", dadoAluno.getFinalGrade()));
            retorno.append(" ");
            retorno.append(dadoAluno.getAbsences());
            retorno.append(System.getProperty("line.separator"));
        }
        return retorno.toString();
    }

}
