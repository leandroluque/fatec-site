package br.com.fatecmogidascruzes.siga;

/**
 * Esta classe armazena dados de um aluno constante em uma lista de médias
 * finais e faltas geradas a partir do SIGA-CPS como arquivos PDF.
 *
 * @author Leandro Luque
 */
public class StudentData {

    private String registry;
    private String name;
    private String situation;
    private Double finalGrade;
    private Integer absences;

    public StudentData() {
    }

    public StudentData(String ra, String nome, String situacao, Double mediaFinal, Integer faltas) {
        this.registry = ra.trim();
        this.name = nome.trim();
        this.situation = situacao.trim();
        this.finalGrade = mediaFinal;
        this.absences = faltas;
    }

    public String getRegistry() {
        return registry;
    }

    public void setRegistry(String ra) {
        this.registry = ra.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String nome) {
        this.name = nome.trim();
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situacao) {
        this.situation = situacao.trim();
    }

    public Double getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(Double mediaFinal) {
        this.finalGrade = mediaFinal;
    }

    public Integer getAbsences() {
        return absences;
    }

    public void setAbsences(Integer faltas) {
        this.absences = faltas;
    }

    @Override
    public String toString() {
        return "DadosAlunos{" + "ra=" + registry + ", nome=" + name + ", situacao=" + situation + ", mediaFinal=" + finalGrade + ", faltas=" + absences + '}';
    }

}
