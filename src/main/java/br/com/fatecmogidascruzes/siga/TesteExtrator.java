package br.com.fatecmogidascruzes.siga;

import br.com.fatecmogidascruzes.siga.service.extractor.FinalGradesAndAbsencesExtractor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class TesteExtrator {

    public static void main(String[] args) throws Exception {
        //testar();
        
        FinalGradesAndAbsencesExtractor extrator = new FinalGradesAndAbsencesExtractor();
        try (BufferedWriter saida = new BufferedWriter(new FileWriter("resumo_por_disciplina.csv"))) {
            saida.write(DisciplineOfferData.getCabecalhoLinha());
            saida.write(System.getProperty("line.separator"));
            File pastaListas = new File("listas");
            File[] listas = pastaListas.listFiles();
            for (File lista : listas) {
                System.out.println("Obtendo dados de " + lista.getName() + "...");
                DisciplineOfferData[] dados = extrator.extract(lista);
                for (DisciplineOfferData dado : dados) {
                    saida.write(dado.resumoLinha());
                    saida.write(System.getProperty("line.separator"));
                }
            }
        }
        
        try (BufferedWriter saida = new BufferedWriter(new FileWriter("todos_dados.csv"))) {
            saida.write(DisciplineOfferData.getCabecalhoLinhaAluno());
            saida.write(System.getProperty("line.separator"));
            File pastaListas = new File("listas");
            File[] listas = pastaListas.listFiles();
            for (File lista : listas) {
                System.out.println("Obtendo dados de " + lista.getName() + "...");
                DisciplineOfferData[] dados = extrator.extract(lista);
                for (DisciplineOfferData dado : dados) {
                    saida.write(dado.resumoLinhaPorAluno());
                }
            }
        }
    }

    public static void testar() throws Exception {
        FinalGradesAndAbsencesExtractor extrator = new FinalGradesAndAbsencesExtractor();
        DisciplineOfferData[] listaDados = extrator.extract("ADSN.pdf");
        System.out.println(DisciplineOfferData.getCabecalhoLinha());
        for (DisciplineOfferData dados : listaDados) {
            System.out.println(dados.resumoLinha());
            System.out.println(dados.multilineRep());
            System.out.println("Máximo de Faltas: " + dados.getTotalClasses() * 0.25);
            int reprovadosPorFalta = 0;
            int reprovadosPorNota = 0;
            for (StudentData dadosAluno : dados) {
                if (dadosAluno.getAbsences() != null && dadosAluno.getAbsences() > dados.getTotalClasses() * 0.25) {
                    System.out.println("REPROVOU: " + dadosAluno.getName());
                    reprovadosPorFalta++;
                } else if (!dadosAluno.getSituation().equals("AE")
                        && !dadosAluno.getSituation().equals("EP")
                        && !dadosAluno.getSituation().equals("AM")
                        && !dadosAluno.getSituation().equals("DM")
                        && !dadosAluno.getSituation().equals("CM")
                        && dadosAluno.getFinalGrade() != null && dadosAluno.getFinalGrade() < 6) {
                    reprovadosPorNota++;
                }
            }
            System.out.println("========================");
            System.out.println("Reprovados por Falta: " + reprovadosPorFalta);
            System.out.println("Reprovados por Nota: " + reprovadosPorNota);
        }
    }

}
