package br.com.fatecmogidascruzes.siga.controller;

import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.siga.dto.LoadFinalGradeAndAbsencesDTO;
import br.com.fatecmogidascruzes.siga.service.SIGAWebService;
import br.com.fatecmogidascruzes.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ADMINISTRATIVE')")
@RequestMapping("/admin/SIGA")
public class SIGAController extends MVCController {

	private final SIGAWebService sigaWebService;

	@Autowired
	public SIGAController(HttpSession httpSession, UserService userService,
						  @Qualifier("sigaWebServiceImpl") SIGAWebService sigaWebService) {
		super(httpSession, userService);
		this.sigaWebService = sigaWebService;
	}

	@GetMapping
	public ModelAndView newLoading(@ModelAttribute("load") LoadFinalGradeAndAbsencesDTO loadDTO, RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("siga/load");

		modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));

		return modelAndView;
	}

	@PostMapping
	public ModelAndView save(@Valid @ModelAttribute("load") LoadFinalGradeAndAbsencesDTO loadDTO, BindingResult binding,
							 RedirectAttributes redirectAttributes) {
		if (binding.hasErrors()) {
			redirectAttributes.addFlashAttribute("message", "error");
			return newLoading(loadDTO, redirectAttributes);
		}

		try {
			sigaWebService.load(loadDTO.getFinalGradeAndAbsences());
			return new ModelAndView("redirect:/admin/SIGA");

		} catch (Exception error) {
			error.printStackTrace();
			redirectAttributes.addFlashAttribute("message", "error");
			return newLoading(loadDTO, redirectAttributes);
		}
	}

}
