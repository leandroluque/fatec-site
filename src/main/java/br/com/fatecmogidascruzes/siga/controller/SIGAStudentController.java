package br.com.fatecmogidascruzes.siga.controller;

import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.siga.dto.LoadStudentsDTO;
import br.com.fatecmogidascruzes.siga.service.SIGAWebService;
import br.com.fatecmogidascruzes.user.service.UserService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ADMINISTRATIVE')")
@RequestMapping("/admin/SIGAStudent")
@CommonsLog
public class SIGAStudentController extends MVCController {
	private final SIGAWebService sigaWebService;

	@Autowired
	public SIGAStudentController(HttpSession httpSession, UserService userService,
								 @Qualifier("sigaStudentWebServiceImpl") SIGAWebService sigaWebService) {
		super(httpSession, userService);
		this.sigaWebService = sigaWebService;
	}

	@GetMapping
	public ModelAndView newLoading(@ModelAttribute("load") LoadStudentsDTO loadDTO, RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("siga/loadStudent");

		modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));

		return modelAndView;
	}

	@PostMapping
	public ModelAndView save(@Valid @ModelAttribute("load") LoadStudentsDTO loadDTO, BindingResult binding,
							 RedirectAttributes redirectAttributes) {
		if (binding.hasErrors()) {
			redirectAttributes.addFlashAttribute("message", "error");
			return newLoading(loadDTO, redirectAttributes);
		}

		try {
			sigaWebService.load(loadDTO.getStudents());
			redirectAttributes.addFlashAttribute("message", "success");
			return newLoading(loadDTO, redirectAttributes);

		} catch (Exception ex) {
			log.error(this, ex);
			redirectAttributes.addFlashAttribute("message", "error");
			return newLoading(loadDTO, redirectAttributes);
		}
	}

}
