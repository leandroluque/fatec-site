package br.com.fatecmogidascruzes.siga.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class LoadFinalGradeAndAbsencesDTO {

	private MultipartFile finalGradeAndAbsences;
	
}
