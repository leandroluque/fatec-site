package br.com.fatecmogidascruzes.siga.exception;

/**
 * Esta classe representa uma exceção que pode ser lançada durante o
 * processamento de dados de listas de médias finais e faltas geradas a partir
 * do SIGA-CPS como arquivos PDF.
 *
 * @author Leandro Luque
 */
public class ProcessingException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProcessingException() {
	}

	public ProcessingException(String message) {
		super(message);
	}

	public ProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProcessingException(Throwable cause) {
		super(cause);
	}

	public ProcessingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
