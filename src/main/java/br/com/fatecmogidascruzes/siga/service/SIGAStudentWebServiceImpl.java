package br.com.fatecmogidascruzes.siga.service;

import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import br.com.fatecmogidascruzes.siga.service.extractor.StudentsExtractor;
import br.com.fatecmogidascruzes.storage.Storage;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.service.StudentService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service(value = "sigaStudentWebServiceImpl")
@CommonsLog
public class SIGAStudentWebServiceImpl implements SIGAWebService {
	private final StudentService studentService;
	private final FileService fileService;
	private final Storage storage;
	private final CourseService courseService;

	@Autowired
	public SIGAStudentWebServiceImpl(StudentService studentService, FileService fileService, Storage storage,
									 CourseService courseService) {
		super();
		this.studentService = studentService;
		this.fileService = fileService;
		this.storage = storage;
		this.courseService = courseService;
	}

	@Override
	public void load(MultipartFile file) throws Exception {
		Exception catchedException = null;
		File myFile = null;

		try {
			myFile = fileService.saveFile(file);

			java.io.File loadedFile = storage.loadFile(myFile.getName());

			StudentsExtractor extractor = new StudentsExtractor();
			List<Student> extractedStudents = extractor.extract(loadedFile);

			for (Student s : extractedStudents) {
				log.info("Upinserting... " + s.getName());
				upinsertStudentEntity(s);
			}
		} catch (Exception error) {
			catchedException = error;
			log.error(error);
		} finally {
			try {
				fileService.remove(myFile);
			} catch (Exception error1) {
				error1.printStackTrace();
			}
		}
		if(catchedException != null) {
			throw catchedException;
		}
	}

	private void upinsertStudentEntity(Student sourceStudent) {
		Student queriedStudent = studentService.getEnabledByRegistry(sourceStudent.getRegistry()).orElse(sourceStudent);
		if (queriedStudent.getId() != null) {
			queriedStudent.setRegistry(sourceStudent.getRegistry());
			queriedStudent.setUsername(sourceStudent.getRegistry());
			queriedStudent.setPassword(sourceStudent.getPassword());
			queriedStudent.setName(sourceStudent.getName());
			queriedStudent.setEntryYearAndSemester(sourceStudent.getEntryYearAndSemester());
			queriedStudent.setEmail(sourceStudent.getEmail());
			queriedStudent.setCourse(courseService.getEnabledByAcronymAndShift(sourceStudent.getCourse().getAcronym(),
					sourceStudent.getCourse().getShift()).get());
			studentService.update(queriedStudent);
		} else {
			queriedStudent.setCourse(courseService.getEnabledByAcronymAndShift(sourceStudent.getCourse().getAcronym(),
					sourceStudent.getCourse().getShift()).get());
			studentService.save(queriedStudent);
		}
	}
}
