package br.com.fatecmogidascruzes.siga.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface SIGAWebService {

	public void load(MultipartFile file) throws Exception;

}
