package br.com.fatecmogidascruzes.siga.service;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment;
import br.com.fatecmogidascruzes.discipline.StudentEnrollment.Situation;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.discipline.service.DisciplineService;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.service.EmployeeService;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import br.com.fatecmogidascruzes.siga.DisciplineOfferData;
import br.com.fatecmogidascruzes.siga.StudentData;
import br.com.fatecmogidascruzes.siga.service.extractor.FinalGradesAndAbsencesExtractor;
import br.com.fatecmogidascruzes.storage.Storage;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.Optional;

@Service(value = "sigaWebServiceImpl")
@Slf4j
public class SIGAWebServiceImpl implements SIGAWebService {

	private final CourseService courseService;
	private final DisciplineService disciplineService;
	private final DisciplineOfferService disciplineOfferService;
	private final EmployeeService employeeService;
	private final FileService fileService;
	private final Storage storage;
	private final StudentService studentService;

	@Autowired
	public SIGAWebServiceImpl(CourseService courseService, DisciplineService disciplineService,
			DisciplineOfferService disciplineOfferService, EmployeeService employeeService, FileService fileService,
			Storage storage, StudentService studentService) {
		super();
		this.courseService = courseService;
		this.disciplineService = disciplineService;
		this.disciplineOfferService = disciplineOfferService;
		this.employeeService = employeeService;
		this.fileService = fileService;
		this.storage = storage;
		this.studentService = studentService;
	}

	@Override
	@Transactional
	public void load(MultipartFile file) throws Exception {
		Exception catchedError = null;
		File myFile = null;

		try {
			myFile = fileService.saveFile(file);

			java.io.File loadedFile = storage.loadFile(myFile.getName());

			FinalGradesAndAbsencesExtractor extractor = new FinalGradesAndAbsencesExtractor();
			DisciplineOfferData[] disciplineOffers = extractor.extract(loadedFile);
			for (DisciplineOfferData disciplineOfferData : disciplineOffers) {

				Shift shift = null;
				if (disciplineOfferData.getShift().equalsIgnoreCase("Tarde")) {
					shift = Shift.AFTERNOON;
				} else if (disciplineOfferData.getShift().equalsIgnoreCase("Noite")) {
					shift = Shift.NIGHT;
				} else {
					shift = Shift.MORNING;
				}

				Course course = null;
				Optional<Course> courseOptional = courseService
						.getEnabledByNameAndShift(disciplineOfferData.getCourseName(), shift);
				if (!courseOptional.isPresent()) {
					throw new IllegalArgumentException("The discipline course does not exist: "
							+ disciplineOfferData.getCourseName() + " for the shift " + shift);
				}
				course = courseOptional.get();

				// Search the discipline by code.
				Optional<Discipline> disciplineOptional = disciplineService
						.getEnabledByAcronymAndCourse(disciplineOfferData.getDisciplineCode(), course);

				// If not found, create a new one.
				Discipline discipline = null;

				if (disciplineOptional.isPresent()) {
					discipline = disciplineOptional.get();
				} else {
					discipline = new Discipline();
					discipline.setAcronym(disciplineOfferData.getDisciplineCode());
					discipline.setCourse(course);
					discipline.setGroup(disciplineOfferData.getClassLetter());
					discipline.setHours(disciplineOfferData.getTotalClasses());
					discipline.setName(disciplineOfferData.getDisciplineName());
					disciplineService.save(discipline);
				}

				//// Search the discipline offer.
				Optional<DisciplineOffer> disciplineOfferOptional = disciplineOfferService
						.getEnabledBySemesterAndYearAndDiscipline(disciplineOfferData.getSemester(),
								disciplineOfferData.getYear(), discipline);

				////// If not found, create a new one.
				DisciplineOffer disciplineOffer = null;
				Employee professor = null;
				if (disciplineOfferOptional.isPresent()) {
					disciplineOffer = disciplineOfferOptional.get();
				} else {
					Optional<Employee> professorOptional = employeeService
							.getEnabledBySIGAInName(disciplineOfferData.getProfessor());
					if (professorOptional.isPresent()) {
						professor = professorOptional.get();
					} else {
						Employee employee = new Employee();
						employee.setName(disciplineOfferData.getProfessor());
						employee.setNameInSIGA(disciplineOfferData.getProfessor());
						employeeService.save(employee);
						professor = employee;
					}

					// Check whether the employee is related to a course.
					if (!professor.isAllocatedIn(course)) {
						professor.addCourse(Courses.getByAcronym(course.getAcronym()));
						employeeService.save(professor);
					}

					disciplineOffer = new DisciplineOffer();
					disciplineOffer.setDiscipline(discipline);
					disciplineOffer.setProfessor(professor);
					disciplineOffer.setSemester(disciplineOfferData.getSemester());
					disciplineOffer.setYear(disciplineOfferData.getYear());
					disciplineOfferService.save(disciplineOffer);
				}

				////// For each student, search.
				for (StudentData studentData : disciplineOfferData.getStudentData()) {
					Optional<Student> studentOptional = studentService.getEnabledByRegistry(studentData.getRegistry());

					//////// If not found, create a new one.
					Student student = null;
					if (studentOptional.isPresent()) {
						student = studentOptional.get();
					} else {
						student = new Student();
						student.setCourse(course);
						student.setName(studentData.getName());
						student.setPassword(studentData.getRegistry());
						student.setUsername(studentData.getRegistry());
						student.setRegistry(studentData.getRegistry());
						studentService.save(student);
					}

					//////// Search student enrollment in the discipline offer.
					Optional<StudentEnrollment> studentEnrollmentOptional = disciplineOffer
							.getStudentEnrollment(student);

					////////// If not found, create a new one.
					StudentEnrollment studentEnrollment = new StudentEnrollment();
					if (studentEnrollmentOptional.isPresent()) {
						studentEnrollment = studentEnrollmentOptional.get();
					} else {
						studentEnrollment = disciplineOffer.addStudentEnrollment(student);
					}

					////////// Set the student record to the discipline.
					studentEnrollment.setAbsences(studentData.getAbsences());
					studentEnrollment.setGrade(studentData.getFinalGrade());
					if (null != studentData.getSituation() && !studentData.getSituation().isEmpty()) {
						try {
							studentEnrollment.setSituation(Situation.valueOf(studentData.getSituation()));
						} catch (Exception error) {
							log.error("Erro processando os dados do aluno " + studentData.getName() + " para a disciplina " + discipline.getName(), error);
							throw error;
						}
					}
				}

				disciplineOfferService.save(disciplineOffer);
			}
		} catch (Exception error) {
			log.error("Error while intepret attachment file.", error);
			catchedError = error;
		} finally {
			try {
				fileService.remove(myFile);
			} catch (Exception error1) {
				log.error("Error closing file.", error1);
			}
		}
		if(catchedError != null) {
			throw catchedError;
		}
	}

}
