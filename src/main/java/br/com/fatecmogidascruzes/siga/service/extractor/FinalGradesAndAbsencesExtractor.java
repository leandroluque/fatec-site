package br.com.fatecmogidascruzes.siga.service.extractor;

import br.com.fatecmogidascruzes.siga.DisciplineOfferData;
import br.com.fatecmogidascruzes.siga.exception.ProcessingException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Esta classe extrai dados de listas de médias finais e faltas geradas a partir
 * do SIGA-CPS como arquivos PDF.
 *
 * @author Leandro Luque
 */
public class FinalGradesAndAbsencesExtractor {
    
    public DisciplineOfferData[] extract(String arquivo) throws IOException, ProcessingException {
        return extract(new File(arquivo));
    }
    
    public DisciplineOfferData[] extract(File arquivo) throws IOException, ProcessingException {
        try {
            List<DisciplineOfferData> listaDados = new ArrayList<>();
            listaDados.add(new DisciplineOfferData()); // Adiciona uma nova entrada para um curso/disciplina/período/turma.
            String conteudoPDF = getPDFContent(arquivo);
            BufferedReader leitor = new BufferedReader(new StringReader(conteudoPDF));
            String linha;
            boolean iniciouAlunos = false;
            while ((linha = leitor.readLine()) != null) {
                // Recupera o último elemento da lista - o atual.
                DisciplineOfferData dados = listaDados.get(listaDados.size() - 1);
                if (!iniciouAlunos) {
                    if (linha.contains("Semestre/")) { // 2o Semestre/2010
                        dados.setSemester(Integer.valueOf(linha.substring(0, 1)));
                        dados.setYear(Integer.valueOf(linha.split("/")[1].trim()));
                    } else if (linha.startsWith("Turno ")) { // Turno Tarde
                        dados.setShift(linha.split(" ")[1].trim());
                    } else if (linha.startsWith("Total de Aulas do")) { // Total de Aulas do Período: 80
                        dados.setTotalClasses(Integer.valueOf(linha.split(": ")[1].trim()));
                    } else if (linha.startsWith("Turma ")) { // Turma A
                        dados.setClassLetter(linha.split(" ")[1].trim());
                    } else if (linha.startsWith("RA Nome Aluno Total Faltas")) { // A linha seguinte é o curso.
                        linha = leitor.readLine();
                        dados.setCourseName(linha.trim());
                    } else if (linha.startsWith("Disc:")) { // Disc: AAG001 - Administração Geral
                        String[] partes = linha.split(":")[1].trim().split(" - ");
                        dados.setDisciplineCode(partes[0].trim());
                        dados.setDisciplineName(partes[1].trim());
                    } else if (linha.startsWith("Prof")) { // Prof(a). EDDA WAGNER
                        dados.setProfessor(linha.split("\\.")[1].trim());
                    } else if (linha.startsWith("Lista de Médias Finais e Faltas")) {
                        iniciouAlunos = true;
                    }
                } else {
                    try {
                        // Verifica se a lista de alunos já terminou.
                        if (linha.contains("Total de Aprovados")) {
                            iniciouAlunos = false;
                            listaDados.add(new DisciplineOfferData()); // Adiciona uma nova entrada para um curso/disciplina/período/turma.
                            continue;
                        } else if (linha.contains("Semestre")) { // Aguarda até os alunos da próxima página.
                            iniciouAlunos = false;
                        }
                        
//                        if (linha.contains("BEATRIZ LARISSA") && dados.getDisciplina().contains("Trabalho de")) {
//                            System.out.println("Inserir ponto de depuracao");
//                        }
                        
                        Pattern padraoRA = Pattern.compile("([0-9]*-*[0-9]*)");
                        Matcher procuradorRA = padraoRA.matcher(linha);
                        String ra = null;
                        if (procuradorRA.find()) {
                            ra = procuradorRA.group(1);
                        } else {
                            continue;
                        }
                        
                        Pattern padraoNome = Pattern.compile("([a-zA-ZáàâãéèêíïóôõöüúçñÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑÜ' ]+)");
                        Matcher procuradorNome = padraoNome.matcher(linha);
                        String nome = null;
                        String situacao = null;
                        String[] partes = null;
                        if (procuradorNome.find()) {
                            nome = procuradorNome.group(1).trim();
                            partes = linha.split(nome);
                            situacao = nome.substring(nome.length() - 2);
                            nome = nome.substring(0, nome.length() - 3);
                        }
                        
                        partes = partes[1].trim().split("( )+");
                        String mediaFinalString = partes[0].trim();
                        String faltasString = partes[1].trim();
                        
                        Double mediaFinal = null;
                        Integer faltas = null;
                        try {
                            mediaFinal = Double.valueOf(mediaFinalString);
                        } catch (Exception e) {
                        }
                        try {
                            faltas = Integer.valueOf(faltasString);
                        } catch (Exception e) {
                        }
                        
                        dados.addDadoAluno(ra, nome, situacao, mediaFinal, faltas);
                    } catch (Exception e) {
                    }
                }
            }
            listaDados.remove(listaDados.size() - 1);
            return listaDados.toArray(new DisciplineOfferData[listaDados.size()]);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new ProcessingException("Ocorreu um erro ao processar o documento PDF.", e);
        }
    }
    
    private String getPDFContent(File arquivo) throws FileNotFoundException, IOException {
        PDFParser parser = new PDFParser(new RandomAccessFile(arquivo, "r"));
        parser.parse();
        COSDocument documentoEmMemoria = parser.getDocument();
        PDFTextStripper processadorDocumento = new PDFTextStripper();
        PDDocument documento = new PDDocument(documentoEmMemoria);
        return processadorDocumento.getText(documento);
    }
    
}
