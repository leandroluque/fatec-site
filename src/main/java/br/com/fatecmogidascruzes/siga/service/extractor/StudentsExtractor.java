package br.com.fatecmogidascruzes.siga.service.extractor;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Shift;
import br.com.fatecmogidascruzes.siga.exception.ProcessingException;
import br.com.fatecmogidascruzes.student.Student;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@CommonsLog
public class StudentsExtractor {
    private List<Student> studentList;

    public StudentsExtractor() {
        studentList = new ArrayList<>();
    }

    public List<Student> extract(File loadedFile) throws ProcessingException {
        try {
            FileInputStream xlsxFile = new FileInputStream(loadedFile);
            Workbook wb = new XSSFWorkbook(xlsxFile);
            Sheet sheetStudents = wb.getSheetAt(0);

            Iterator<Row> rowIterator = sheetStudents.rowIterator();
            rowIterator.next(); // Dicard the title row of the sheet
            rowIterator.forEachRemaining( r -> {
                Course course = new Course();
                course.setAcronym(r.getCell(3).getStringCellValue().trim());
                course.setShift(Shift.getShiftByName(r.getCell(2).getStringCellValue().trim()));

                Student student = new Student();
                student.setCourse(course);

                String cell0Value = fillValue(NumberToTextConverter.toText(r.getCell(0).getNumericCellValue()), 13);
                String cell1Value = fillValue(NumberToTextConverter.toText(r.getCell(1).getNumericCellValue()), 11);
                String cell5Value = NumberToTextConverter.toText(r.getCell(5).getNumericCellValue());

                student.setRegistry(cell0Value);
                student.setUsername(cell0Value);
                student.setPassword(cell1Value);
                student.setName(r.getCell(4).getStringCellValue().trim());
                student.setEntryYearAndSemester(cell5Value);
                student.setEmail(r.getCell(6).getStringCellValue().trim());
                studentList.add(student);
            });
            xlsxFile.close();
        } catch (IOException ex) {
           log.error(this, ex);
           throw new ProcessingException(ex);
        }
        return studentList;
    }

    public String fillValue(String value, int nrCharacters) {
        String newValue = value.trim();
        if ( newValue == null) {
            return newValue;
        } else {
            while(newValue.length() < nrCharacters) {
                newValue = "0" + newValue;
            }
        }
        return newValue;
    }
}
