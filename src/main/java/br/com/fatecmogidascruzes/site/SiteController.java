package br.com.fatecmogidascruzes.site;

import br.com.fatecmogidascruzes.agenda.Calendar;
import br.com.fatecmogidascruzes.agenda.service.web.CalendarWebService;
import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.Courses;
import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.service.EmployeeService;
import br.com.fatecmogidascruzes.employee.service.web.EmployeeWebService;
import br.com.fatecmogidascruzes.event.Event;
import br.com.fatecmogidascruzes.event.service.web.EventWebService;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.internshipagent.service.InternshipAgentService;
import br.com.fatecmogidascruzes.news.News;
import br.com.fatecmogidascruzes.news.service.web.NewsWebService;
import br.com.fatecmogidascruzes.storage.Storage;
import br.com.fatecmogidascruzes.testimonial.service.web.TestimonialWebService;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class SiteController {

	private final NewsWebService newsWebService;
	private final EmployeeWebService employeeWebService;
	private final EventWebService eventWebService;
	private final TestimonialWebService testimonialWebService;
	private final CourseService courseService;
	private final Storage storage;
	private final CalendarWebService calendarWebService;
	private final InternshipAgentService internshipAgentService;
	private final EmployeeService employeeService;

	@Autowired
	public SiteController(NewsWebService newsWebService, EmployeeWebService employeeWebService,
			EventWebService eventWebService, TestimonialWebService testimonialWebService, CourseService courseService,
						  Storage storage, CalendarWebService calendarWebService, InternshipAgentService internshipAgentService,
						  EmployeeService employeeService) {
		this.newsWebService = newsWebService;
		this.employeeWebService = employeeWebService;
		this.eventWebService = eventWebService;
		this.testimonialWebService = testimonialWebService;
		this.courseService = courseService;
		this.storage = storage;
		this.calendarWebService = calendarWebService;
		this.internshipAgentService = internshipAgentService;
		this.employeeService = employeeService;
	}

	@GetMapping(value = "/hash/{hashString}", produces = { "text/plain" })
	public @ResponseBody String hashStringToHash(@PathVariable("hashString") String hashString) {
		return FriendlyId.toUuid(hashString).toString();
	}

	@GetMapping("/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("site/index");
		modelAndView.addObject("highlightList", newsWebService.getHighlights());
		modelAndView.addObject("newsList", newsWebService.getHomeNews());
		modelAndView.addObject("eventList", eventWebService.getHomeEvents());
		modelAndView.addObject("testimonialList", testimonialWebService.getHomeTestimonials());
		modelAndView.addObject("preEnrollmentMenuEnabled", SemesterYearConfig.isPreEnrollMenuEnabled());

		return modelAndView;
	}

	@GetMapping("/ads")
	public ModelAndView ads() {
		ModelAndView modelAndView = new ModelAndView("site/ads");

		String acronym = "ADS";
		List<Course> courses = courseService.findEnabledByAcronym(acronym);
		if (courses.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourseAcronym(acronym));
			modelAndView.addObject("schedule", courses.get(0).getCurrentSchedule());
		}

		return modelAndView;
	}

	@GetMapping("/aFatecMogi")
	public ModelAndView aFatecMogi() {
		return new ModelAndView("site/aFatecMogi");
	}


	@GetMapping("/agro")
	public ModelAndView agro() {
		ModelAndView modelAndView = new ModelAndView("site/agro");

		String acronym = "AGRO";
		List<Course> courses = courseService.findEnabledByAcronym(acronym);
		if (courses.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourseAcronym(acronym));
			modelAndView.addObject("schedule", courses.get(0).getCurrentSchedule());
		}

		return modelAndView;
	}

	@GetMapping("/horario/{courseAcronym}")
	public ResponseEntity<StreamingResponseBody> downloadSchedule(@PathVariable("courseAcronym") String courseAcronym) throws IOException {
		List<Course> courses = courseService.findEnabledByAcronym(courseAcronym);
		if(courses.isEmpty() || courses.get(0).getCurrentSchedule() == null) {
			return ResponseEntity.notFound().build();
		}

		Course course = courses.get(0);
		return download(course.getCurrentSchedule(), storage, "horario.pdf");
	}

	public static ResponseEntity<StreamingResponseBody> download(File file, Storage storage, String defaultFileName) throws FileNotFoundException {
		java.io.File diskFile = storage.loadFile(file.getName());
		InputStream inputStream = new BufferedInputStream(new FileInputStream(diskFile));
		StreamingResponseBody responseBody = outputStream -> {
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			inputStream.close();
		};
		HttpHeaders headers = new HttpHeaders();
		String fileName = file.getName();
		//if (fileName == null) {
			fileName = defaultFileName;
		//}
		headers.setContentDisposition(ContentDisposition.builder("inline").filename(fileName).build());
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
		headers.add(HttpHeaders.CONTENT_LENGTH, String.valueOf(diskFile.length()));
		return ResponseEntity.ok()
				.headers(headers)
				.body(responseBody);
	}

	@GetMapping("/gestao")
	public ModelAndView gestao_empresarial() {
		ModelAndView modelAndView = new ModelAndView("site/gestao_empresarial");

		String acronym = "GESTAO";
		List<Course> courses = courseService.findEnabledByAcronym(acronym);
		if (courses.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourseAcronym(acronym));
			modelAndView.addObject("schedule", courses.get(0).getCurrentSchedule());
		}

		return modelAndView;
	}

	@GetMapping("/biblioteca")
	public ModelAndView biblioteca() {
		return new ModelAndView("site/biblioteca");
	}

	@GetMapping("/calendarioAcademico")
	public ModelAndView calendarioAcademico() {
		return new ModelAndView("site/calendarioAcademico");
	}

	@GetMapping("/comoChegar")
	public ModelAndView comoChegar() {
		return new ModelAndView("site/comoChegar");
	}

	@GetMapping("/concursos")
	public ModelAndView concursos() {
		return new ModelAndView("site/concursos");
	}

	@GetMapping("/corpoAdministrativo")
	public ModelAndView corpoAdministrativo() {
		return new ModelAndView("site/corpoAdministrativo");
	}

	@GetMapping("/corpoDocente")
	public ModelAndView corpoDocente(@RequestParam(name = "course", required = false) String acronym) {
		ModelAndView modelAndView = new ModelAndView("site/corpoDocente");

		List<Course> courses = null;
		if (null != courses) {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourse(Courses.getByAcronym(acronym)));
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledProfessors());
		}

		return modelAndView;
	}

	@GetMapping("/detalheNoticia")
	public ModelAndView detalheNoticia(@RequestParam("hash") UUID hash,
			@RequestParam(name = "abrirLink", required = false) boolean abrirLink) {
		News news = null;
		if (abrirLink && null != (news = newsWebService.abrirLink(hash))) {
			ModelAndView modelAndView = new ModelAndView("redirect:" + news.getLink());
			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView("site/detalheNoticia");
			modelAndView.addObject("news", newsWebService.getNewsDetail(hash));

			return modelAndView;
		}
	}

	@GetMapping("/detalheEvento")
	public ModelAndView detalheEvento(@RequestParam("hash") UUID hash,
			@RequestParam(name = "abrirLink", required = false) boolean abrirLink) {
		Event event = null;
		if (abrirLink && null != (event = eventWebService.abrirLink(hash))) {
			ModelAndView modelAndView = new ModelAndView("redirect:" + event.getLink());
			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView("site/detalheEvento");
			modelAndView.addObject("event", eventWebService.getEventDetail(hash));

			return modelAndView;
		}
	}

	@GetMapping("/direcao")
	public ModelAndView direcao() {
		return new ModelAndView("site/direcao");
	}

	@GetMapping("/diretoriaServicoAdministrativo")
	public ModelAndView diretoriaServicoAdministrativo() {
		return new ModelAndView("site/diretoriaServicoAdministrativo");
	}

	@GetMapping("/estagio")
	public ModelAndView estagio() {
		ModelAndView modelAndView = new ModelAndView("site/estagio");
		modelAndView.addObject("internshipResponsibles", employeeService.getEnabledInternshipResponsibles());
		modelAndView.addObject("internshipAgents", internshipAgentService.getEnabledSortedByName());
		return modelAndView;
	}

	@GetMapping("/intercambio")
	public ModelAndView intercambio() {
		return new ModelAndView("site/intercambio");
	}

	@GetMapping("/logistica")
	public ModelAndView logistica() {
		ModelAndView modelAndView = new ModelAndView("site/logistica");

		String acronym = "LOG";
		List<Course> courses = courseService.findEnabledByAcronym(acronym);
		if (courses.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourseAcronym(acronym));
			modelAndView.addObject("schedule", courses.get(0).getCurrentSchedule());
		}

		return modelAndView;
	}

	@GetMapping("/rh")
	public ModelAndView rh() {
		ModelAndView modelAndView = new ModelAndView("site/rh");

		String acronym = "RH";
		List<Course> courses = courseService.findEnabledByAcronym(acronym);
		if (courses.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} else {
			modelAndView.addObject("professors", employeeWebService.getEnabledByCourseAcronym(acronym));
			modelAndView.addObject("schedule", courses.get(0).getCurrentSchedule());
		}

		return modelAndView;
	}

	@GetMapping("/selecoes")
	public ModelAndView selecoes() {
		return new ModelAndView("site/selecoes");
	}

	@GetMapping("/todasNoticias")
	public ModelAndView todasNoticias() {
		ModelAndView modelAndView = new ModelAndView("site/todasNoticias");
		modelAndView.addObject("newsList", newsWebService.getEnabledNews());

		return modelAndView;
	}

	@GetMapping("/todosEventos")
	public ModelAndView todosEventos() {
		ModelAndView modelAndView = new ModelAndView("site/todosEventos");
		modelAndView.addObject("eventList", eventWebService.getEnabledEvents());

		return modelAndView;
	}

	@GetMapping("/vagasRemanescentes")
	public ModelAndView vagasRemanescentes() {
		return new ModelAndView("site/vagasRemanescentes");
	}

	@GetMapping("/passeEscolar")
	public ModelAndView passeEscolar() {
		return new ModelAndView("site/passeEscolar");
	}

	@GetMapping("/secretariaAcademica")
	public ModelAndView secretariaAcademica() {
		return new ModelAndView("site/secretariaAcademica");
	}

	@GetMapping("/congregacao")
	public ModelAndView congregacao() {
		ModelAndView modelAndView = new ModelAndView("site/congregacao");

		List<Employee> directors = employeeService.getEnabledByAllocation(Employee.Allocation.DIRETOR);
		if(!directors.isEmpty()) {
			modelAndView.addObject("director", directors.get(0));
		}
		modelAndView.addObject("coordinators", employeeService.getEnabledByAllocation(Employee.Allocation.COORDENADOR));

		Map<Employee.Level, List<Employee>> employeesByLevel = employeeService.getEnabledCongregationMembers()
				.stream()
				.collect(Collectors.groupingBy(                // outer Map values
						Employee::getLevel, // inner Map keys
						TreeMap::new,                 // inner Map is TreeMap
						Collectors.toList()           // inner Map values (default)
				));
		modelAndView.addObject("levels", employeesByLevel);

		return modelAndView;
	}

	@GetMapping("/gruposPesquisa")
	public ModelAndView gruposPesquisa() {
		return new ModelAndView("site/gruposPesquisa");
	}

	@GetMapping("/animaTerra")
	public ModelAndView animaTerra() {
		return new ModelAndView("site/animaTerra");
	}

	@GetMapping("/emailsInstitucionais")
	public ModelAndView emailsInstitucionais() {
		ModelAndView modelAndView = new ModelAndView("site/emailsInstitucionais");
		modelAndView.addObject("internshipResponsibles", employeeService.getEnabledInternshipResponsibles());
		List<Employee> directors = employeeService.getEnabledByAllocation(Employee.Allocation.DIRETOR);
		if(!directors.isEmpty()) {
			modelAndView.addObject("director", directors.get(0));
		}
		modelAndView.addObject("coordinators", employeeService.getEnabledByAllocation(Employee.Allocation.COORDENADOR));
		return modelAndView;
	}

	@GetMapping("/cpa")
	public ModelAndView cpa() {
		return new ModelAndView("site/cpa");

	}

	@GetMapping("/fateclog")
	public ModelAndView fatecLog() {
		return new ModelAndView("site/fateclog");
	}

	@GetMapping("/calendarioPDF")
	public ResponseEntity<StreamingResponseBody> downloadFile() throws IOException {

		Optional<Calendar> opCalendar = calendarWebService.getCalendar();
		if(!opCalendar.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return download(opCalendar.get().getFile(), storage, "calendario.pdf");
	}
}
