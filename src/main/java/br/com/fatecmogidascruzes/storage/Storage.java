package br.com.fatecmogidascruzes.storage;

import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public interface Storage {

	Path getPath();
	
	Path getPathTo(String fileName);

	void store(MultipartFile file, String name);

	String store(MultipartFile file);

	void copy(br.com.fatecmogidascruzes.file.File file, String withName);

	byte[] loadBytes(String name);

	BufferedImage loadImage(String name) throws IOException;

	File loadFile(String name);

	void remove(String name);

}
