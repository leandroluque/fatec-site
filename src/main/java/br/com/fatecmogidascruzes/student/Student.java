package br.com.fatecmogidascruzes.student;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.domain.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_student")
public class Student extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "registry", nullable = false, length = 30)
	private String registry;

	@Basic
	@Column(name = "username", nullable = false, length = 30)
	private String username;

	@Basic
	@Column(name = "password", nullable = false, length = 30)
	private String password;

	@JoinColumn(name = "course")
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Course course;

	@Basic
	@Column(name = "entry_year_semester", nullable = true, length = 10)
	private String entryYearAndSemester;

	@Basic
	@Column(name = "email", nullable = true, length = 100)
	private String email;

	@Basic
	@Column(name = "phone", nullable = true, length = 100)
	private String phone;
	
	@Basic
	@Column(name = "pr", nullable = true)
	private Double pr;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_student")
	@Id
	@Override
	@SequenceGenerator(name = "seq_student", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

}