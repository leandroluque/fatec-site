package br.com.fatecmogidascruzes.student.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StudentDAO extends DAOImpl<Student, Long>, JpaRepository<Student, Long> {

	Optional<Student> findByEnabledTrueAndUsernameAndPassword(String username, String password);

	@Query("SELECT s FROM Student s JOIN FETCH s.course WHERE s.registry = (:registry)")
	Optional<Student> findByEnabledTrueAndRegistry(@Param("registry") String registry);

	@Query("SELECT COUNT(DISTINCT enr.student) FROM Enrollment enr WHERE TRUE = enr.enabled AND :semester = enr.semester AND :year = enr.year")
	long countEnabledAndEnrolledIn(@Param("semester") int semester, @Param("year") int year);

}