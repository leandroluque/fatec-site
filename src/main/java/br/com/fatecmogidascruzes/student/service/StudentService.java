package br.com.fatecmogidascruzes.student.service;

import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.student.Student;

import java.util.Optional;

public interface StudentService extends BaseService<Student, Long> {

	Optional<Student> getEnabledByUsernameAndPassword(String userName, String password);

	Optional<Student> getEnabledByRegistry(String registry);
}
