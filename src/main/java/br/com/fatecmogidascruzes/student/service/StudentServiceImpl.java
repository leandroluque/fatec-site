package br.com.fatecmogidascruzes.student.service;

import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import br.com.fatecmogidascruzes.student.Student;
import br.com.fatecmogidascruzes.student.data.StudentDAO;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentServiceImpl extends BaseServiceImpl<Student, Long> implements StudentService {

	private StudentDAO studentDAO;

	public StudentServiceImpl(StudentDAO studentDAO) {
		super(studentDAO);
		this.studentDAO = studentDAO;
	}

	@Override
	public Optional<Student> getEnabledByUsernameAndPassword(String userName, String password) {
		return studentDAO.findByEnabledTrueAndUsernameAndPassword(userName, password);
	}

	@Override
	public Optional<Student> getEnabledByRegistry(String registry) {
		return studentDAO.findByEnabledTrueAndRegistry(registry);
	}

}
