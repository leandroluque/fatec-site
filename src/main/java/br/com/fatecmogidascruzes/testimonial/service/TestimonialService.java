package br.com.fatecmogidascruzes.testimonial.service;

import br.com.fatecmogidascruzes.data.SearchCriteria;
import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.testimonial.Testimonial;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TestimonialService extends BaseService<Testimonial, Long> {

	Page<Testimonial> getEnabledByFilter(SearchCriteria searchCriteria);

	List<Testimonial> getEnabledToShow();
	
	
}
