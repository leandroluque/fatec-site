package br.com.fatecmogidascruzes.timetable.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ScheduleDTO {

	@NotNull
	private MultipartFile adsSchedule;
	@NotNull
	private MultipartFile agroSchedule;
	@NotNull
	private MultipartFile gestaoSchedule;
	@NotNull
	private MultipartFile logisticaSchedule;
	@NotNull
	private MultipartFile rhSchedule;

	private boolean currentlyHasAdsFile;
	private boolean currentlyHasAgroFile;
	private boolean currentlyHasGestaoFile;
	private boolean currentlyHasLogisticaFile;
	private boolean currentlyHasRhFile;

}
