package br.com.fatecmogidascruzes.timetable.dto;

import br.com.fatecmogidascruzes.course.Shift;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TimeTableDTO {

	private int year;
	private int semester;
	private Shift shift;
	private String courseHashString;
	private List<TimeTableDisciplineDTO> disciplines = new ArrayList<>();

}
