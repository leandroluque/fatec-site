package br.com.fatecmogidascruzes.timetable.dto;

import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass.TimeSlot;
import br.com.fatecmogidascruzes.domain.WeekDay;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class TimeTableDisciplineDTO {

	private String disciplineHashString;
	private int part;
	private String name;
	private WeekDay weekDay;
	private TimeSlot slotNumber;
	private String professorHashString;
	private String disciplineOfferHashString;
	private String disciplineOfferClassHashString;

	public TimeTableDisciplineDTO(String disciplineHashString) {
		super();
		this.disciplineHashString = disciplineHashString;
	}

	public TimeTableDisciplineDTO() {
		super();
	}

	public static TimeTableDisciplineDTO[] from(DisciplineOffer disciplineOffer) {
		List<TimeTableDisciplineDTO> dtos = new ArrayList<>();
		int i = 1;
		for (DisciplineOfferClass disciplineClass : disciplineOffer.getDisciplineClasses()) {
			TimeTableDisciplineDTO timeTableDisciplineDTO = new TimeTableDisciplineDTO();
			timeTableDisciplineDTO.setDisciplineHashString(disciplineOffer.getDiscipline().getHashString());
			timeTableDisciplineDTO.setName(disciplineOffer.getDiscipline().getName());
			timeTableDisciplineDTO.setSlotNumber(disciplineClass.getTimeSlot());
			timeTableDisciplineDTO.setWeekDay(disciplineClass.getWeekDay());
			if (null != disciplineOffer.getProfessor()) {
				timeTableDisciplineDTO.setProfessorHashString(disciplineOffer.getProfessor().getHashString());
			}
			timeTableDisciplineDTO.setPart(i++);
			timeTableDisciplineDTO.setDisciplineOfferHashString(disciplineOffer.getHashString());
			timeTableDisciplineDTO.setDisciplineOfferClassHashString(disciplineClass.getHashString());
			dtos.add(timeTableDisciplineDTO);
		}
		return dtos.toArray(new TimeTableDisciplineDTO[dtos.size()]);
	}

	public static TimeTableDisciplineDTO[] from(Discipline discipline) {
		List<TimeTableDisciplineDTO> dtos = new ArrayList<>();
		int howManyClasses = discipline.getHours() / 20;
		for (int i = 0; i < howManyClasses; i++) {
			TimeTableDisciplineDTO timeTableDisciplineDTO = new TimeTableDisciplineDTO();
			timeTableDisciplineDTO.setDisciplineHashString(discipline.getHashString());
			timeTableDisciplineDTO.setName(discipline.getName());
			timeTableDisciplineDTO.setPart(i + 1);
			dtos.add(timeTableDisciplineDTO);
		}
		return dtos.toArray(new TimeTableDisciplineDTO[dtos.size()]);
	}

	public static List<TimeTableDisciplineDTO> disciplineOfferListFrom(List<DisciplineOffer> object) {
		List<TimeTableDisciplineDTO> dtos = new ArrayList<>();
		object.forEach(obj -> dtos.addAll(Arrays.asList(TimeTableDisciplineDTO.from(obj))));
		return dtos;
	}

	public static List<TimeTableDisciplineDTO> disciplineListFrom(List<Discipline> object) {
		List<TimeTableDisciplineDTO> dtos = new ArrayList<>();
		object.forEach(obj -> dtos.addAll(Arrays.asList(TimeTableDisciplineDTO.from(obj))));
		return dtos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((disciplineHashString == null) ? 0 : disciplineHashString.hashCode());
		result = prime * result + part;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TimeTableDisciplineDTO))
			return false;
		TimeTableDisciplineDTO other = (TimeTableDisciplineDTO) obj;
		if (disciplineHashString == null) {
			if (other.disciplineHashString != null)
				return false;
		} else if (!disciplineHashString.equals(other.disciplineHashString))
			return false;
		if (part != other.part)
			return false;
		return true;
	}

}
