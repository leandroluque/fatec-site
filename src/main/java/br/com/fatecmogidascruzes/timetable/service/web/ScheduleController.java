package br.com.fatecmogidascruzes.timetable.service.web;


import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.storage.Storage;
import br.com.fatecmogidascruzes.timetable.dto.ScheduleDTO;
import br.com.fatecmogidascruzes.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.*;
import java.util.Optional;

@Controller
@PreAuthorize("hasRole('ACADEMIC')")
@RequestMapping("/admin/schedule")
public class ScheduleController extends MVCController {

    private final ScheduleWebService webService;
    private final Storage storage;

    @Autowired
    public ScheduleController(HttpSession httpSession, UserService userService,
                                 ScheduleWebService webService,
                                 Storage storage) {
        super(httpSession, userService);
        this.webService = webService;
        this.storage = storage;
    }


    @GetMapping
    public ModelAndView getSchedules(RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("/timetable/editFiles");
        modelAndView.addObject("schedules", webService.getScheduleDTO());
        return modelAndView;
    }

    @PostMapping
    public ModelAndView save(@Valid @ModelAttribute("calendar") ScheduleDTO dto,
                             BindingResult binding, RedirectAttributes redirectAttributes) {
        if (binding.hasErrors()) {
            redirectAttributes.addFlashAttribute("message", "error");
            return new ModelAndView("redirect:/admin/schedule");
        }

        this.webService.save(dto);
        redirectAttributes.addFlashAttribute("message", "success");
        return new ModelAndView("redirect:/admin/schedule");
    }

    @GetMapping("/{courseAcronym}")
    public ResponseEntity<StreamingResponseBody> downloadFile(@PathVariable("courseAcronym") String courseAcronym) throws IOException {

        Optional<File> opSchedule = webService.getCourseSchedule(courseAcronym);
        if(!opSchedule.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        File file = opSchedule.get();
        return download(file, storage, "calendario.pdf");
    }

    public static ResponseEntity<StreamingResponseBody> download(File file, Storage storage, String defaultFileName) throws FileNotFoundException {
        java.io.File diskFile = storage.loadFile(file.getName());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(diskFile));
        StreamingResponseBody responseBody = outputStream -> {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            inputStream.close();
        };
        HttpHeaders headers = new HttpHeaders();
        String fileName = file.getName();
        if (fileName == null) {
            fileName = defaultFileName;
        }
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
        headers.add(HttpHeaders.CONTENT_LENGTH, String.valueOf(diskFile.length()));
        return ResponseEntity.ok()
                .headers(headers)
                .body(responseBody);
    }

}
