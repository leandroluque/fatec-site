package br.com.fatecmogidascruzes.timetable.service.web;

import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.timetable.dto.ScheduleDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ScheduleWebService {
    ScheduleDTO getScheduleDTO();

    Optional<File> getCourseSchedule(String courseAcronym);

    @Transactional
    void save(ScheduleDTO dto);
}
