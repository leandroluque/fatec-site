package br.com.fatecmogidascruzes.timetable.service.web;

import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import br.com.fatecmogidascruzes.timetable.dto.ScheduleDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class ScheduleWebServiceImpl implements ScheduleWebService {

    private final CourseService courseService;
    private final FileService fileService;

    public ScheduleWebServiceImpl(CourseService courseService, FileService fileService) {
        this.courseService = courseService;
        this.fileService = fileService;
    }

    @Override
    public ScheduleDTO getScheduleDTO() {
        ScheduleDTO dto = new ScheduleDTO();
        List<Course> courses = this.courseService.getEnabled();
        for(Course course: courses) {
            if(course.getCurrentSchedule() != null) {
                if(course.getAcronym().equalsIgnoreCase("ADS")) {
                    dto.setCurrentlyHasAdsFile(course.getCurrentSchedule() != null);
                } else if(course.getAcronym().equalsIgnoreCase("AGRO")) {
                    dto.setCurrentlyHasAgroFile(course.getCurrentSchedule() != null);
                } else if(course.getAcronym().equalsIgnoreCase("GESTAO")) {
                    dto.setCurrentlyHasGestaoFile(course.getCurrentSchedule() != null);
                } else if(course.getAcronym().equalsIgnoreCase("LOG")) {
                    dto.setCurrentlyHasLogisticaFile(course.getCurrentSchedule() != null);
                } else if(course.getAcronym().equalsIgnoreCase("RH")) {
                    dto.setCurrentlyHasRhFile(course.getCurrentSchedule() != null);
                }
            }
        }
        return dto;
    }

    @Override
    @Transactional
    public Optional<File> getCourseSchedule(String courseAcronym) {
        List<Course> courses = this.courseService.findEnabledByAcronym(courseAcronym);
        if(courses.isEmpty() || null == courses.get(0).getCurrentSchedule()) {
            return Optional.empty();
        }
        return Optional.of(courses.get(0).getCurrentSchedule());
    }

    @Override
    @Transactional
    public void save(ScheduleDTO dto) {
        List<Course> courses = this.courseService.findEnabledByAcronym("ADS");
        updateCoursesSchedule(courses, dto.getAdsSchedule());
        courses = this.courseService.findEnabledByAcronym("AGRO");
        updateCoursesSchedule(courses, dto.getAgroSchedule());
        courses = this.courseService.findEnabledByAcronym("GESTAO");
        updateCoursesSchedule(courses, dto.getGestaoSchedule());
        courses = this.courseService.findEnabledByAcronym("LOG");
        updateCoursesSchedule(courses, dto.getLogisticaSchedule());
        courses = this.courseService.findEnabledByAcronym("RH");
        updateCoursesSchedule(courses, dto.getRhSchedule());
    }

    @Transactional
    public void updateCoursesSchedule(List<Course> courses, MultipartFile multipartFile) {
        if(courses == null || courses.isEmpty()) {
            return;
        }
        if (null != multipartFile && !multipartFile.isEmpty()) {
            File schedule = fileService.saveFile(multipartFile);
            for(int i = 0; i < courses.size(); i++) {
                Course course = courses.get(i);
                // If the user has specified a new schedule but another one exists, delete the old one.
                if (null != course.getCurrentSchedule()) {
                    fileService.removeByKey(course.getCurrentSchedule().getId());
                }

                course.setCurrentSchedule(i == 0 ? schedule : fileService.copyFile(schedule));
                courseService.save(course);
            }
        }
    }

}
