package br.com.fatecmogidascruzes.timetable.service.web;

import br.com.fatecmogidascruzes.config.SemesterYearConfig;
import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.course.Course;
import br.com.fatecmogidascruzes.course.service.CourseService;
import br.com.fatecmogidascruzes.discipline.Discipline;
import br.com.fatecmogidascruzes.discipline.DisciplineOffer;
import br.com.fatecmogidascruzes.discipline.DisciplineOfferClass;
import br.com.fatecmogidascruzes.discipline.data.DisciplineOfferClassDAO;
import br.com.fatecmogidascruzes.discipline.service.DisciplineOfferService;
import br.com.fatecmogidascruzes.discipline.service.DisciplineService;
import br.com.fatecmogidascruzes.employee.Employee;
import br.com.fatecmogidascruzes.employee.service.EmployeeService;
import br.com.fatecmogidascruzes.timetable.dto.TimeTableDTO;
import br.com.fatecmogidascruzes.timetable.dto.TimeTableDisciplineDTO;
import br.com.fatecmogidascruzes.user.service.UserService;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@Controller
@PreAuthorize("hasRole('ADMINISTRATIVE')")
@RequestMapping("/admin/timetable")
public class TimetableController extends MVCController {

	private static final int SEMESTER = SemesterYearConfig.getPreEnrollSemester();
	private static final int YEAR = SemesterYearConfig.getPreEnrollYear();

	private final CourseService courseService;
	private final DisciplineService disciplineService;
	private final DisciplineOfferService disciplineOfferService;
	private final EmployeeService employeeService;
	private final DisciplineOfferClassDAO disciplineOfferClassDAO;

	@Autowired
	public TimetableController(HttpSession httpSession, UserService userService, DisciplineService disciplineService,
			CourseService courseService, DisciplineOfferService disciplineOfferService, EmployeeService employeeService,
			DisciplineOfferClassDAO disciplineOfferClassDAO) {
		super(httpSession, userService);
		this.disciplineService = disciplineService;
		this.courseService = courseService;
		this.disciplineOfferService = disciplineOfferService;
		this.employeeService = employeeService;
		this.disciplineOfferClassDAO = disciplineOfferClassDAO;
	}

	@GetMapping
	public ModelAndView open(@RequestParam(name = "course", required = false) UUID courseHash,
			@ModelAttribute("timetable") TimeTableDTO timeTableDTO, RedirectAttributes redirectAttributes) {

		if (null != courseHash) {
			Optional<Course> courseOptional = courseService.getEnabledByHash(courseHash);
			if (courseOptional.isPresent()) {

				ModelAndView modelAndView = new ModelAndView("timetable/edit");

				Course course = courseOptional.get();

				System.out.println(SEMESTER + "/" + YEAR);
				timeTableDTO.setSemester(SEMESTER);
				timeTableDTO.setYear(YEAR);
				timeTableDTO.setCourseHashString(course.getHashString());
				timeTableDTO.setShift(course.getShift());

				Set<TimeTableDisciplineDTO> disciplineDTOs = new HashSet<>();

				List<DisciplineOffer> disciplineOffers = disciplineOfferService
						.getEnabledBySemesterAndYearAndCourse(SEMESTER, YEAR, course);

				disciplineDTOs.addAll(TimeTableDisciplineDTO.disciplineOfferListFrom(disciplineOffers));

				List<Discipline> disciplines = disciplineService.getEnabledByCourse(course);

				for (Discipline disc : disciplines) {
					if (!disciplineDTOs.contains(new TimeTableDisciplineDTO(disc.getHashString()))) {
						disciplineDTOs.addAll(Arrays.asList(TimeTableDisciplineDTO.from(disc)));
					}
				}

				List<TimeTableDisciplineDTO> orderedDisciplineDTOs = new ArrayList<>(disciplineDTOs);
				Collections.sort(orderedDisciplineDTOs, new Comparator<TimeTableDisciplineDTO>() {
					@Override
					public int compare(TimeTableDisciplineDTO arg0, TimeTableDisciplineDTO arg1) {
						int difference = arg0.getName().compareTo(arg1.getName());
						if (0 == difference) {
							return arg0.getPart() - arg1.getPart();
						}

						return difference;
					}
				});

				timeTableDTO.setDisciplines(orderedDisciplineDTOs);
				modelAndView.addObject("professors",
						employeeService.getEnabledProfessorsByCourseAcronym(course.getAcronym()));
				modelAndView.addObject("course", course);
				modelAndView.addObject("semester", SEMESTER);
				modelAndView.addObject("year", YEAR);
				modelAndView.addObject("timeTable", timeTableDTO);
				modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));

				return modelAndView;

			} else {

				ModelAndView modelAndView = new ModelAndView("timetable/edit");

				modelAndView.addObject("error", "invalidCourse");

				return modelAndView;

			}
		} else {

			ModelAndView modelAndView = new ModelAndView("timetable/edit");

			modelAndView.addObject("courses", courseService.getEnabled());
			modelAndView.addObject("timeTable", new TimeTableDTO());
			return modelAndView;
		}

	}

	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("timetable") TimeTableDTO timeTableDTO, BindingResult binding,
			RedirectAttributes redirectAttributes) {
		if (binding.hasErrors()) {
			redirectAttributes.addFlashAttribute("message", "error");
			return open(null, timeTableDTO, redirectAttributes);
		}

		try {

			// For each discipline, check whether exists and offer and get it.
			for (TimeTableDisciplineDTO timeTableDisciplineDTO : timeTableDTO.getDisciplines()) {

				if (null != timeTableDisciplineDTO.getDisciplineOfferClassHashString()
						&& !timeTableDisciplineDTO.getDisciplineOfferClassHashString().isEmpty()) {

					Optional<DisciplineOfferClass> optional = disciplineOfferClassDAO
							.findByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getDisciplineOfferClassHashString()));

					if (optional.isPresent()) {

						DisciplineOfferClass disciplineOfferClass = optional.get();
						disciplineOfferClass.setTimeSlot(timeTableDisciplineDTO.getSlotNumber());
						disciplineOfferClass.setWeekDay(timeTableDisciplineDTO.getWeekDay());
						
						if (null != timeTableDisciplineDTO.getProfessorHashString()
								&& !timeTableDisciplineDTO.getProfessorHashString().isEmpty()
								&& !timeTableDisciplineDTO.getProfessorHashString().equalsIgnoreCase("null")) {
							Optional<Employee> professorOptional = employeeService
									.getByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getProfessorHashString()));
							if (professorOptional.isPresent()) {

								
								disciplineOfferClass.getDisciplineOffer().setProfessor(professorOptional.get());
							} else {
								throw new IllegalArgumentException("The professor was not found");
							}
						}
						
						disciplineOfferClassDAO.save(disciplineOfferClass);

					} else {
						throw new IllegalArgumentException("The discipline offer class was not found");
					}
				} else if (null != timeTableDisciplineDTO.getDisciplineOfferHashString()
						&& !timeTableDisciplineDTO.getDisciplineOfferHashString().isEmpty()) {

					Optional<DisciplineOffer> disciplineOfferOptional = disciplineOfferService
							.getByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getDisciplineOfferHashString()));

					if (disciplineOfferOptional.isPresent()) {

						DisciplineOffer disciplineOffer = disciplineOfferOptional.get();

						if (null != timeTableDisciplineDTO.getProfessorHashString()
								&& !timeTableDisciplineDTO.getProfessorHashString().isEmpty()
								&& !timeTableDisciplineDTO.getProfessorHashString().equalsIgnoreCase("null")) {
							Optional<Employee> professorOptional = employeeService
									.getByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getProfessorHashString()));
							if (professorOptional.isPresent()) {

								
								disciplineOffer.setProfessor(professorOptional.get());
							} else {
								throw new IllegalArgumentException("The professor was not found");
							}
						}
						
						DisciplineOfferClass disciplineOfferClass = new DisciplineOfferClass();
						disciplineOfferClass.setDisciplineOffer(disciplineOffer);
						disciplineOfferClass.setTimeSlot(timeTableDisciplineDTO.getSlotNumber());
						disciplineOfferClass.setWeekDay(timeTableDisciplineDTO.getWeekDay());

						disciplineOffer.addDisciplineOfferClass(disciplineOfferClass);
						disciplineOfferService.save(disciplineOffer);

					} else {
						throw new IllegalArgumentException("The discipline offer was not found");
					}

				} else {

					Optional<Discipline> disciplineOptional = disciplineService
							.getByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getDisciplineHashString()));
					if (disciplineOptional.isPresent()) {

						Optional<DisciplineOffer> disciplineOfferOptional = disciplineOfferService
								.getEnabledBySemesterAndYearAndDiscipline(timeTableDTO.getSemester(),
										timeTableDTO.getYear(), disciplineOptional.get());

						DisciplineOffer disciplineOffer = null;
						if (disciplineOfferOptional.isPresent()) {
							disciplineOffer = disciplineOfferOptional.get();
						} else {
							disciplineOffer = new DisciplineOffer();
							disciplineOffer.setDiscipline(disciplineOptional.get());

							if (null != timeTableDisciplineDTO.getProfessorHashString()
									&& !timeTableDisciplineDTO.getProfessorHashString().isEmpty()
									&& !timeTableDisciplineDTO.getProfessorHashString().equalsIgnoreCase("null")) {
								Optional<Employee> professorOptional = employeeService
										.getByHash(FriendlyId.toUuid(timeTableDisciplineDTO.getProfessorHashString()));
								if (professorOptional.isPresent()) {

									
									disciplineOffer.setProfessor(professorOptional.get());
								} else {
									throw new IllegalArgumentException("The professor was not found");
								}
							}
							disciplineOffer.setSemester(SEMESTER);
							disciplineOffer.setYear(YEAR);
						}

						DisciplineOfferClass disciplineOfferClass = new DisciplineOfferClass();
						disciplineOfferClass.setDisciplineOffer(disciplineOffer);
						disciplineOfferClass.setTimeSlot(timeTableDisciplineDTO.getSlotNumber());
						disciplineOfferClass.setWeekDay(timeTableDisciplineDTO.getWeekDay());

						disciplineOffer.addDisciplineOfferClass(disciplineOfferClass);
						disciplineOfferService.save(disciplineOffer);

					} else {
						throw new IllegalArgumentException("The discipline was not found");
					}
				}
			}

			// After that, update it.

			redirectAttributes.addFlashAttribute("message", "success");
			return new ModelAndView("redirect:/admin/timetable");

		} catch (Exception error) {
			error.printStackTrace();
			redirectAttributes.addFlashAttribute("message", "error");
			return open(null, timeTableDTO, redirectAttributes);
		}
	}

}
