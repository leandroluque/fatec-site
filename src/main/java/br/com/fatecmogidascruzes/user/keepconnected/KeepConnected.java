package br.com.fatecmogidascruzes.user.keepconnected;

import br.com.fatecmogidascruzes.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Entity
@NoArgsConstructor
@Setter
@Table(name = "_keep_connected")
public class KeepConnected {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_keep_conn")
	@Id
	@SequenceGenerator(name = "seq_keep_conn", initialValue = 1, allocationSize = 1)
	private Long id;

	@Basic
	@Column(name = "series", nullable = false, length = 100)
	private String series;

	@Basic
	@Column(name = "keep_conn_token", nullable = false, length = 100)
	private String token;

	@Column(name = "last_use")
	private LocalDateTime lastUse;

	@JoinColumn(name = "user")
	@ManyToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private User user;

}