package br.com.fatecmogidascruzes.user.keepconnected.data;

import br.com.fatecmogidascruzes.user.User;
import br.com.fatecmogidascruzes.user.keepconnected.KeepConnected;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface KeepConnectedDAO extends JpaRepository<KeepConnected, Long> {

	List<KeepConnected> findByUser(User user);

	List<KeepConnected> findByLastUseBefore(LocalDateTime date);

	Optional<KeepConnected> findBySeries(String series);

	void removeByUser(User user);

}