package br.com.fatecmogidascruzes.user.role.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.user.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleDAO extends DAOImpl<Role, Long>, JpaRepository<Role, Long> {

	public Optional<Role> findByEnabledTrueAndName(String name);

}