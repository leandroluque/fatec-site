package br.com.fatecmogidascruzes.user.role.service;

import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.user.role.Role;

import java.util.Optional;

public interface RoleService extends BaseService<Role, Long> {

	Optional<Role> getEnabledByName(String name);

}