package br.com.fatecmogidascruzes.user.service.web.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordChangeMustMatchValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordChangeMustMatch {

	String message() default "As senhas informadas devem ser iguais";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}