package br.com.fatecmogidascruzes.user.service.web.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordChangeMustNotBeBlankOnInclusionValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordChangeMustNotBeBlankOnInclusion {

	String message() default "A senha do usuário é obrigatória";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}