package br.com.fatecmogidascruzes.workgroup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SelectionStatus {

	STARTED("Aberto"), UNDERGOIND("Em andamento"), FINISHED("Finalizado");

	private String name;

}
