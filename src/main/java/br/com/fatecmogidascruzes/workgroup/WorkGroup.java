package br.com.fatecmogidascruzes.workgroup;

import br.com.fatecmogidascruzes.domain.EntityImpl;
import br.com.fatecmogidascruzes.file.File;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Entity
@Setter
@Table(name = "_work_group")
public class WorkGroup extends EntityImpl {

	private static final long serialVersionUID = 1L;

	@Column(name = "acronym", nullable = false)
	@Enumerated(EnumType.STRING)
	protected WorkGroupAcronymEnum acronym;

	@Basic
	@Column(name = "description", nullable = true, length = 5000)
	protected String description;

	@Basic
	@Column(name = "role", nullable = true, length = 50)
	protected String role;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "_work_group_files", joinColumns = @JoinColumn(name = "work_group_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
	@OrderBy(value = "description ASC")
	private Set<File> files = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "_work_group_reports", joinColumns = @JoinColumn(name = "work_group_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
	@OrderBy(value = "description ASC")
	private Set<File> reports = new HashSet<>();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_work_group")
	@Id
	@Override
	@SequenceGenerator(name = "seq_work_group", initialValue = 1, allocationSize = 1)
	public Long getId() {
		return super.getId();
	}

	public void addDocument(File file) {
		this.files.add(file);
	}

	public void removeDocumentByHash(UUID hash) {
		this.files.removeIf(file -> hash.equals(file.getHash()));
	}

	public void addReport(File file) {
		this.reports.add(file);
	}

	public void removeReportByHash(UUID hash) {
		this.reports.removeIf(report -> hash.equals(report.getHash()));
	}

}
	