package br.com.fatecmogidascruzes.workgroup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WorkGroupAcronymEnum {

	GEPPA("Grupo de Ensino e Pesquisa em Produção Animal"),
	GEPAR("Grupo de Ensino e Pesquisa em Agroinformática e Rastreabilidade"),
	GEPLICO("Grupo de Ensino e Pesquisa em Liderança e Coaching"),
	GPRH("Grupo de Estudos e Pesquisa em Recursos Humanos"),
	NUPPA("Núcleo de Pesquisas em Preços Agrícolas"),
	GEPEA("Grupo de Estudo e Pesquisa Econômica ATLAS");

	private String name;

}