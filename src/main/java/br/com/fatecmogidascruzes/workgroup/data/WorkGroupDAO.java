package br.com.fatecmogidascruzes.workgroup.data;

import br.com.fatecmogidascruzes.data.DAOImpl;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import br.com.fatecmogidascruzes.workgroup.WorkGroupAcronymEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WorkGroupDAO extends DAOImpl<WorkGroup, Long>, JpaRepository<WorkGroup, Long> {

	Optional<WorkGroup> findByAcronym(WorkGroupAcronymEnum acronym);
	
}
