package br.com.fatecmogidascruzes.workgroup.service;

import br.com.fatecmogidascruzes.service.BaseService;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import br.com.fatecmogidascruzes.workgroup.WorkGroupAcronymEnum;

import java.util.Optional;

public interface WorkGroupService extends BaseService<WorkGroup, Long> {

	Optional<WorkGroup> findByAcronym(WorkGroupAcronymEnum acronym);

}