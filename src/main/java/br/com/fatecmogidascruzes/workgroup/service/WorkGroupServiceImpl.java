package br.com.fatecmogidascruzes.workgroup.service;

import br.com.fatecmogidascruzes.service.BaseServiceImpl;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import br.com.fatecmogidascruzes.workgroup.WorkGroupAcronymEnum;
import br.com.fatecmogidascruzes.workgroup.data.WorkGroupDAO;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WorkGroupServiceImpl extends BaseServiceImpl<WorkGroup, Long> implements WorkGroupService {

	@SuppressWarnings("unused")
	private final WorkGroupDAO workGroupDAO;

	public WorkGroupServiceImpl(WorkGroupDAO workGroupDAO) {
		super(workGroupDAO);
		this.workGroupDAO = workGroupDAO;
	}

	@Override
	public Optional<WorkGroup> findByAcronym(WorkGroupAcronymEnum acronym) {
		return this.workGroupDAO.findByAcronym(acronym);
	}

}
