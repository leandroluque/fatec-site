package br.com.fatecmogidascruzes.workgroup.service.web;

import br.com.fatecmogidascruzes.controller.MVCController;
import br.com.fatecmogidascruzes.exception.DoesNotHaveAccessException;
import br.com.fatecmogidascruzes.exception.InexistentOrDisabledEntity;
import br.com.fatecmogidascruzes.exception.web.BadRequestException;
import br.com.fatecmogidascruzes.exception.web.ForbiddenException;
import br.com.fatecmogidascruzes.exception.web.InternalErrorException;
import br.com.fatecmogidascruzes.exception.web.NotFoundException;
import br.com.fatecmogidascruzes.file.FileDTO;
import br.com.fatecmogidascruzes.file.service.web.FileWebService;
import br.com.fatecmogidascruzes.user.service.UserService;
import br.com.fatecmogidascruzes.util.Constants;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import br.com.fatecmogidascruzes.workgroup.service.WorkGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/admin/workGroups")
public class WorkGroupController extends MVCController {

	private final WorkGroupService workGroupService;
	private final WorkGroupWebService workGroupWebService;
	private final FileWebService fileWebService;

	@Autowired
	public WorkGroupController(HttpSession httpSession, UserService userService, WorkGroupService workGroupService,
			WorkGroupWebService workGroupWebService, FileWebService fileWebService) {
		super(httpSession, userService);
		this.workGroupService = workGroupService;
		this.workGroupWebService = workGroupWebService;
		this.fileWebService = fileWebService;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if (null != text && !text.isEmpty()) {
					try {
						setValue(LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
					} catch (Exception error) {
					}
				}
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (null != getValue()) {
					try {
						return DateTimeFormatter.ofPattern("dd/MM/yyyy").format((LocalDate) getValue());
					} catch (Exception error) {
						return null;
					}
				} else {
					return null;
				}
			}
		});
		binder.registerCustomEditor(LocalDateTime.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if (null != text && !text.isEmpty()) {
					try {
						setValue(LocalDateTime.parse(text, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
					} catch (Exception error) {
					}
				}
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (null != getValue()) {
					try {
						return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm").format((LocalDateTime) getValue());
					} catch (Exception error) {
						return null;
					}
				} else {
					return null;
				}
			}
		});
	}

	@PreAuthorize("hasRole('ADMINISTRATIVE')")
	@RequestMapping(path = "new", method = RequestMethod.GET)
	public ModelAndView newWorkGroup(@ModelAttribute("workGroup") WorkGroupEditDTO workGroupEditDTO,
			RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("workGroup/edit");

		modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));
		modelAndView.addObject("workGroup", workGroupEditDTO);

		return modelAndView;
	}

	@PreAuthorize("hasRole('ADMINISTRATIVE')")
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("workGroup") WorkGroupEditDTO workGroupEditDTO,
			BindingResult binding, RedirectAttributes redirectAttributes) {
		if (binding.hasErrors()) {
			redirectAttributes.addFlashAttribute("message", "error");
			return newWorkGroup(workGroupEditDTO, redirectAttributes);
		}

		try {
			WorkGroup workGroup = this.workGroupWebService.save(workGroupEditDTO);
			redirectAttributes.addFlashAttribute("message", "success");
			return new ModelAndView("redirect:/admin/workGroups/" + workGroup.getAcronym());

		} catch (Exception error) {
			error.printStackTrace();
			redirectAttributes.addFlashAttribute("message", "error");
			return newWorkGroup(workGroupEditDTO, redirectAttributes);
		}
	}

//	@PreAuthorize("hasRole('ADMINISTRATIVE')")
//	@GetMapping
//	public ModelAndView search(RedirectAttributes redirectAttributes) {
//
//		ModelAndView modelAndView = new ModelAndView("workGroup/search");
//		modelAndView.addObject("message", redirectAttributes.getFlashAttributes().get("message"));
//		return modelAndView;
//
//	}

	@PreAuthorize("hasRole('ADMINISTRATIVE')")
	@RequestMapping(path = "/{workGroupAcronym}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(name = "workGroupAcronym", required = true) String acronym,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {

		ModelAndView modelAndView = new ModelAndView("/workGroup/edit");
		try {
			WorkGroupEditDTO workGroupEditDTO = this.workGroupWebService.find(acronym);

			if (!request.isUserInRole(workGroupEditDTO.getRole())) {
				throw new ForbiddenException("Você não tem acesso a este grupo");
			}

			modelAndView.addObject("isUpdate", true);
			modelAndView.addObject("workGroup", workGroupEditDTO);

		} catch (Exception error) {
			error.printStackTrace();
			redirectAttributes.addFlashAttribute("message", "error");
		}

		return modelAndView;
	}

	@RequestMapping(path = "/view/{workGroupAcronym}", method = RequestMethod.GET)
	public ModelAndView onlyView(@PathVariable(name = "workGroupAcronym", required = true) String acronym,
			RedirectAttributes redirectAttributes) {

		ModelAndView modelAndView = new ModelAndView("/site/workGroup");
		try {
			WorkGroupEditDTO workGroupEditDTO = this.workGroupWebService.find(acronym);
			modelAndView.addObject("workGroup", workGroupEditDTO);

		} catch (Exception error) {
			error.printStackTrace();
			redirectAttributes.addFlashAttribute("message", "error");
		}

		return modelAndView;
	}

//	@RequestMapping(path = "/delete", method = RequestMethod.GET)
//	public ModelAndView delete(@RequestParam(name = "hash", required = true) UUID hash,
//			RedirectAttributes redirectAttributes) {
//		try {
//			Optional<WorkGroup> workGroupOptional = workGroupService.getByHash(hash);
//			if (workGroupOptional.isPresent()) {
//				WorkGroup workGroup = workGroupOptional.get();
//				workGroup.setEnabled(false);
//				workGroupService.update(workGroup);
//
//				redirectAttributes.addFlashAttribute("message", "success");
//			} else {
//				redirectAttributes.addFlashAttribute("message", "error.notFound");
//			}
//		} catch (Exception error) {
//			error.printStackTrace();
//			redirectAttributes.addFlashAttribute("message", "error");
//		}
//
//		return new ModelAndView("redirect:/admin/workGroups");
//	}

	// Fields to filter the table - related to the JPQL.
//	private static String[] fields = { null, "number", "course", "discipline", null };
//
//	@RequestMapping(path = "table", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
//	public @ResponseBody TableDTO<WorkGroupTableRowDTO> getTable(
//			@RequestParam(name = "draw", required = false) Integer draw,
//			@RequestParam(name = "start", required = false, defaultValue = "0") Integer initialPage,
//			@RequestParam(name = "length", required = false, defaultValue = "10") Integer numberOfRegisters,
//			@RequestParam(name = "search[value]", required = false, defaultValue = "") String filter,
//			@RequestParam(name = "order[0][column]", required = false, defaultValue = "1") Integer columnsToSort,
//			@RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String columnsOrder) {
//
//		SearchCriteria searchCriteria = new SearchCriteria();
//		if (filter != null && !filter.equals("")) {
//			searchCriteria.setFilter(filter);
//		}
//		if (columnsToSort < fields.length) {
//			String fieldName = fields[columnsToSort];
//			searchCriteria.addSortBy(fieldName);
//			searchCriteria.setOrder(columnsOrder.equalsIgnoreCase("asc") ? SearchCriteria.Order.ASCENDING
//					: SearchCriteria.Order.DESCENDING);
//		}
//
//		searchCriteria.setWhatToFilter(Arrays.asList(fields));
//		searchCriteria.setInitialRegister(initialPage / numberOfRegisters);
//		searchCriteria.setNumberOfRegisters(numberOfRegisters);
//
//		return workGroupWebService.getTable(searchCriteria, draw);
//	}

	@RequestMapping(path = "{workGroupHash}/documents/{documentHash}", method = RequestMethod.GET)
	public /* ResponseEntity<byte[]> */ String document(@PathVariable("workGroupHash") UUID workGroupHash,
			@PathVariable("documentHash") UUID documentHash, HttpServletRequest request) {

		try {
			Optional<WorkGroup> workGroupOptional = this.workGroupService.getEnabledByHash(workGroupHash);
			if (workGroupOptional.isPresent()) {

				FileDTO fileDTO = this.fileWebService.getFile(documentHash);
				if (null != fileDTO) {

					return "redirect:" + getBaseURI(request) + Constants.FILES_PATH + fileDTO.getFileName();

				} else {
					throw new NotFoundException("The workGroup document has not been found.");
				}

			} else {
				throw new BadRequestException("The workGroup does not exist");
			}

		} catch (InexistentOrDisabledEntity e) {
			e.printStackTrace();
			throw new BadRequestException(e);
		} catch (DoesNotHaveAccessException e) {
			e.printStackTrace();
			throw new ForbiddenException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalErrorException(e);
		}

	}

	@RequestMapping(path = "{workGroupHash}/reports/{reportHash}", method = RequestMethod.GET)
	public /* ResponseEntity<byte[]> */ String report(@PathVariable("workGroupHash") UUID workGroupHash,
			@PathVariable("reportHash") UUID reportHash, HttpServletRequest request) {

		try {
			Optional<WorkGroup> workGroupOptional = this.workGroupService.getEnabledByHash(workGroupHash);
			if (workGroupOptional.isPresent()) {

				FileDTO fileDTO = this.fileWebService.getFile(reportHash);
				if (null != fileDTO) {

					return "redirect:" + getBaseURI(request) + Constants.FILES_PATH + fileDTO.getFileName();

				} else {
					throw new NotFoundException("The workGroup report has not been found.");
				}

			} else {
				throw new BadRequestException("The workGroup does not exist");
			}

		} catch (InexistentOrDisabledEntity e) {
			e.printStackTrace();
			throw new BadRequestException(e);
		} catch (DoesNotHaveAccessException e) {
			e.printStackTrace();
			throw new ForbiddenException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalErrorException(e);
		}

	}

}
