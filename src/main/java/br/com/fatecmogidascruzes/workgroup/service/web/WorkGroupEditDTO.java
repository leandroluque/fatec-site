package br.com.fatecmogidascruzes.workgroup.service.web;

import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Setter
public class WorkGroupEditDTO {

	private String hashString;

	private String name;
	private String acronym;

	@NotBlank(message = "A descrição do grupo é obrigatória")
	@Length(max = 5000, message = "A descrição do grupo é obrigatória")
	private String description;

	private String role;

	private List<WorkGroupDocumentDTO> currentDocuments = new ArrayList<>();
	private String[] documentsHashesToRemove;
	private MultipartFile[] newDocuments;
	private String[] newDocumentsDescriptions;

	private List<WorkGroupReportDTO> currentReports = new ArrayList<>();
	private String[] reportsHashesToRemove;
	private MultipartFile[] newReports;
	private String[] newReportsDescriptions;

	public void setCurrentDocuments(Collection<File> documents) {
		this.currentDocuments = new ArrayList<>();
		for (File document : documents) {
			this.currentDocuments.add(WorkGroupDocumentDTO.from(document));
		}
//		Collections.sort(this.currentDocuments, new Comparator<WorkGroupDocumentDTO>() {
//			@Override
//			public int compare(WorkGroupDocumentDTO arg0, WorkGroupDocumentDTO arg1) {
//				return arg1.getTitle().compareTo(arg0.getTitle());
//			}
//		});
	}

	public void setCurrentReports(Collection<File> reports) {
		this.currentReports = new ArrayList<>();
		for (File report : reports) {
			this.currentReports.add(WorkGroupReportDTO.from(report));
		}
//		Collections.sort(this.currentReports, new Comparator<WorkGroupReportDTO>() {
//			@Override
//			public int compare(WorkGroupReportDTO arg0, WorkGroupReportDTO arg1) {
//				return arg1.getTitle().compareTo(arg0.getTitle());
//			}
//		});
	}

	public static WorkGroupEditDTO from(WorkGroup workGroup) {
		WorkGroupEditDTO workGroupEditDTO = new WorkGroupEditDTO();

		workGroupEditDTO.setName(workGroup.getAcronym().getName());
		workGroupEditDTO.setAcronym(workGroup.getAcronym().name());
		workGroupEditDTO.setHashString(FriendlyId.toFriendlyId(workGroup.getHash()));
		workGroupEditDTO.setDescription(workGroup.getDescription());
		workGroupEditDTO.setRole(workGroup.getRole());

		workGroupEditDTO.setCurrentDocuments(workGroup.getFiles());

		workGroupEditDTO.setCurrentReports(workGroup.getReports());

		return workGroupEditDTO;
	}

	public void fill(WorkGroup workGroup) {

		workGroup.setDescription(description);
		workGroup.setLastUpdate(LocalDateTime.now());

	}

}
