package br.com.fatecmogidascruzes.workgroup.service.web;

import br.com.fatecmogidascruzes.file.File;
import com.devskiller.friendly_id.FriendlyId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class WorkGroupReportDTO {

	private UUID hash;
	private String hashString;
	private String title;
	private boolean enabled;

	public static WorkGroupReportDTO from(File file) {
		WorkGroupReportDTO postDocumentDTO = new WorkGroupReportDTO();

		postDocumentDTO.setHash(file.getHash());
		postDocumentDTO.setHashString(FriendlyId.toFriendlyId(file.getHash()));
		postDocumentDTO.setTitle(file.getDescription());
		postDocumentDTO.setEnabled(file.isEnabled());

		return postDocumentDTO;
	}

}
