package br.com.fatecmogidascruzes.workgroup.service.web;

import br.com.fatecmogidascruzes.exception.InexistentOrDisabledEntity;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;

import java.util.UUID;

public interface WorkGroupWebService {

	WorkGroupEditDTO getWorkGroupEditDTOByHash(UUID selectionHash) throws InexistentOrDisabledEntity;

	WorkGroup save(WorkGroupEditDTO workGroupEditDTO);

	WorkGroupEditDTO find(String acronym) throws InexistentOrDisabledEntity;

}
