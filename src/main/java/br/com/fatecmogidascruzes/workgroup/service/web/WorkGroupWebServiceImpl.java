package br.com.fatecmogidascruzes.workgroup.service.web;

import br.com.fatecmogidascruzes.exception.InexistentOrDisabledEntity;
import br.com.fatecmogidascruzes.file.File;
import br.com.fatecmogidascruzes.file.service.FileService;
import br.com.fatecmogidascruzes.workgroup.WorkGroup;
import br.com.fatecmogidascruzes.workgroup.WorkGroupAcronymEnum;
import br.com.fatecmogidascruzes.workgroup.service.WorkGroupService;
import com.devskiller.friendly_id.FriendlyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.UUID;

@Service
public class WorkGroupWebServiceImpl implements WorkGroupWebService {

	private final WorkGroupService workGroupService;
	private final FileService fileService;

	@Autowired
	public WorkGroupWebServiceImpl(WorkGroupService workGroupService, FileService fileService) {
		super();
		this.workGroupService = workGroupService;
		this.fileService = fileService;
	}

	@Override
	public WorkGroupEditDTO find(String acronym) throws InexistentOrDisabledEntity {
		Optional<WorkGroup> workGroupOptional = this.workGroupService.findByAcronym(WorkGroupAcronymEnum.valueOf(acronym));
		if (workGroupOptional.isPresent() && workGroupOptional.get().isEnabled()) {
			WorkGroup workGroup = workGroupOptional.get();

			return WorkGroupEditDTO.from(workGroup);
		} else {
			throw new InexistentOrDisabledEntity("The informed workGroup does not exist or is disabled");
		}
	}

	@Override
	public WorkGroupEditDTO getWorkGroupEditDTOByHash(UUID workGroupHash) throws InexistentOrDisabledEntity {
		Optional<WorkGroup> workGroupOptional = this.workGroupService.getByHash(workGroupHash);
		if (workGroupOptional.isPresent() && workGroupOptional.get().isEnabled()) {
			WorkGroup workGroup = workGroupOptional.get();

			return WorkGroupEditDTO.from(workGroup);
		} else {
			throw new InexistentOrDisabledEntity("The informed workGroup does not exist or is disabled");
		}
	}

	@Override
	public WorkGroup save(WorkGroupEditDTO workGroupEditDTO) {
		WorkGroup workGroup = new WorkGroup();
		if (null != workGroupEditDTO.getHashString() && !workGroupEditDTO.getHashString().trim().isEmpty()) {
			Optional<WorkGroup> workGroupOptional = this.workGroupService
					.getByHash(FriendlyId.toUuid(workGroupEditDTO.getHashString()));
			if (workGroupOptional.isPresent() && workGroupOptional.get().isEnabled()) {
				workGroup = workGroupOptional.get();
			}
		}

		workGroupEditDTO.fill(workGroup);

		// Remove documents.
		if (null != workGroupEditDTO.getDocumentsHashesToRemove()) {
			for (String friendlyId : workGroupEditDTO.getDocumentsHashesToRemove()) {
				UUID hash = FriendlyId.toUuid(friendlyId);
				fileService.removeByHash(hash);
				workGroup.removeDocumentByHash(hash);
			}
		}

		// Add new documents.
		if (null != workGroupEditDTO.getNewDocuments()) {
			int i = 0;
			for (MultipartFile multipartFile : workGroupEditDTO.getNewDocuments()) {
				if (!multipartFile.isEmpty()) {
					File newDocument = fileService.saveFile(multipartFile);
					if (workGroupEditDTO.getNewDocumentsDescriptions().length > i) {
						if (null != workGroupEditDTO.getNewDocumentsDescriptions()[i]
								&& !workGroupEditDTO.getNewDocumentsDescriptions()[i].trim().isEmpty()) {
							newDocument.setDescription(workGroupEditDTO.getNewDocumentsDescriptions()[i]);
						} else {
							newDocument.setDescription("Sem descrição");
						}
					}
					fileService.save(newDocument);
					workGroup.addDocument(newDocument);
					i++;
				}
			}
		}

		// Remove reports.
		if (null != workGroupEditDTO.getReportsHashesToRemove()) {
			for (String friendlyId : workGroupEditDTO.getReportsHashesToRemove()) {
				UUID hash = FriendlyId.toUuid(friendlyId);
				fileService.removeByHash(hash);
				workGroup.removeReportByHash(hash);
			}
		}

		// Add new reports.
		if (null != workGroupEditDTO.getNewReports()) {
			int i = 0;
			for (MultipartFile multipartFile : workGroupEditDTO.getNewReports()) {
				if (!multipartFile.isEmpty()) {
					File newReport = fileService.saveFile(multipartFile);
					if (workGroupEditDTO.getNewReportsDescriptions().length > i) {
						if (null != workGroupEditDTO.getNewReportsDescriptions()[i]
								&& !workGroupEditDTO.getNewReportsDescriptions()[i].trim().isEmpty()) {
							newReport.setDescription(workGroupEditDTO.getNewReportsDescriptions()[i]);
						} else {
							newReport.setDescription("Sem descrição");
						}
					}
					fileService.save(newReport);
					workGroup.addReport(newReport);
					i++;
				}
			}
		}

		this.workGroupService.save(workGroup);
		
		return workGroup;
	}

}
