function setHashToDelete(id) {
    $("#hashToDelete").val(id);
}

$(document)
	.ready(
		function() {
		    var contextPath = $('meta[name="_context_path"]').attr(
			    'content');

		    $('#list')
			    .DataTable(
				    {
					'responsive' : true,
					'order' : [ [ 1, 'asc' ] ],
					'paging' : true,
					'sPaginationType' : 'full_numbers',
					'searching' : true,
					'ordering' : true,
					'processing' : true,
					'serverSide' : true,
					'ajax' : contextPath
						+ 'admin/accommodations/table',
					'language' : {
					    'url' : contextPath
						    + 'scripts/pt-br.json'
					},
					'columns' : [
						{
						    'data' : 'hash',
						    'bSortable' : false,
						    'bSearchable' : false,
						    'render' : function(data,
							    type, row) {
							if (type === 'display') {
							    return '<a href="'
								    + contextPath
								    + 'admin/accommodations/'
								    + row.hash
								    + '"><i class="fa fa-pencil-square-o text-success" aria-hidden="true"></i></a>';
							}
							return data;
						    },
						},
						{
						    'data' : 'studentName',
						    'bSortable' : true,
						    'render' : function(data,
							    type, row) {
							return data;
						    },
						},
						{
						    'data' : 'disciplinesToAdd',
						    'bSortable' : false,
						    'bSearchable' : false,
						    'render' : function(data,
							    type, row) {
							return data;
						    },
						},
						{
						    'data' : 'disciplinesToRemove',
						    'bSortable' : false,
						    'bSearchable' : false,
						    'render' : function(data,
							    type, row) {
							return data;
						    },
						} ]
				    });
		});