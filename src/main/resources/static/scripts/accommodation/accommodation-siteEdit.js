$(document).ready(function() {
    var contextPath = $('meta[name="_context_path"]').attr('content');

    $('#disciplinesToAddModal').DataTable({
	"pageLength" : 5,
	'searching' : true,
	'ordering' : true,
	'language' : {
	    'url' : contextPath + 'scripts/pt-br.json'
	}
    });

    $('#disciplinesToRemoveModal').DataTable({
	"pageLength" : 5,
	'searching' : true,
	'ordering' : true,
	'language' : {
	    'url' : contextPath + 'scripts/pt-br.json'
	}
    });
});