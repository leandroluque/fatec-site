$(function() {

	let currentYear = 2019;

	$(document)
			.ready(
					function() {
						$('#calendar')
								.eCalendar(
										{

											events : [
													// JANEIRO

													{
														id : 1,
														description : 'Confraternização Universal - Feriado',
														title : '1/jan/2019',
														datetime : new Date(
																currentYear, 0,
																1),
														endDate : new Date(
																currentYear, 0,
																1)
													},

													{
														id : 2,
														description : 'Divulgação da classificação geral e desempenho dos candidatos',
														title : '9/jan/2019',
														datetime : new Date(
																currentYear, 0,
																9),
														endDate : new Date(
																currentYear, 0,
																9)
													},

													{
														id : 3,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/jan/2019',
														datetime : new Date(
																currentYear, 0,
																10),
														endDate : new Date(
																currentYear, 0,
																10)
													},

													{
														id : 4,
														description : 'Matrícula da 1ª chamada de ingressantes e solicitação de aproveitamento de estudos',
														title : '10/jan/2019 - 11/jan/2019',
														datetime : new Date(
																currentYear, 0,
																10),
														endDate : new Date(
																currentYear, 0,
																11)
													},

													{
														id : 5,
														description : 'Divulgação da 2ª Lista de convocação',
														title : '14/jan/2019',
														datetime : new Date(
																currentYear, 0,
																14),
														endDate : new Date(
																currentYear, 0,
																14)
													},

													{
														id : 6,
														description : 'Matrícula da 2ª chamada de ingressantes e solicitação de aproveitamento de estudos',
														title : '15/jan/2019',
														datetime : new Date(
																currentYear, 0,
																15),
														endDate : new Date(
																currentYear, 0,
																15)
													},

													{
														id : 7,
														description : 'Rematrícula de alunos Veteranos - Ead, Agro, RH, Log (SIGA) e ADS (Presencial)',
														title : '21/jan/2019 - 26/jan/2019',
														datetime : new Date(
																currentYear, 0,
																21),
														endDate : new Date(
																currentYear, 0,
																26)
													},

													// FEVEREIRO

													{
														id : 8,
														description : 'Início do Semestre Letivo',
														title : '1/fev/2019',
														datetime : new Date(
																currentYear, 1,
																1),
														endDate : new Date(
																currentYear, 1,
																1)
													},

													{
														id : 9,
														description : 'Semana de Planejamento e Aperfeiçoamento Pedagógico',
														title : '1/fev/2019 - 5/fev/2019',
														datetime : new Date(
																currentYear, 1,
																1),
														endDate : new Date(
																currentYear, 1,
																5)
													},

													{
														id : 10,
														description : 'Início das Aulas do 1º Semestre Letivo de 2019 (Presencial e EaD)',
														title : '6/fev/2019',
														datetime : new Date(
																currentYear, 1,
																6),
														endDate : new Date(
																currentYear, 1,
																6)
													},

													{
														id : 11,
														description : 'Acomodação de matrícula para alunos veteranos',
														title : '6/fev/2019 - 13/fev/2019',
														datetime : new Date(
																currentYear, 1,
																6),
														endDate : new Date(
																currentYear, 1,
																13)
													},

													{
														id : 12,
														description : 'Reposição Referente à Sexta-Feira - Agronegócio, ADS, RH e Logística',
														title : '9/fev/2019',
														datetime : new Date(
																currentYear, 1,
																9),
														endDate : new Date(
																currentYear, 1,
																9)
													},

													{
														id : 13,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/fev/2019',
														datetime : new Date(
																currentYear, 1,
																10),
														endDate : new Date(
																currentYear, 1,
																10)
													},

													{
														id : 14,
														description : 'Reposição Referente à Sábado - Agronegócio, ADS, RH e Logística',
														title : '16/fev/2019',
														datetime : new Date(
																currentYear, 1,
																16),
														endDate : new Date(
																currentYear, 1,
																16)
													},

													{
														id : 15,
														description : 'Reposição Referente à Segunda-Feira - Agronegócio, ADS, RH e Logística',
														title : '23/fev/2019',
														datetime : new Date(
																currentYear, 1,
																23),
														endDate : new Date(
																currentYear, 1,
																23)
													},

													// MARÇO

													{
														id : 16,
														description : 'Não haverá aula - Emenda de Feriado (Carnaval) / Quarta-Feira de Cinzas',
														title : '2/mar/2019 - 6/mar/2019',
														datetime : new Date(
																currentYear, 2,
																2),
														endDate : new Date(
																currentYear, 2,
																6)
													},

													{
														id : 17,
														description : 'Reposição Referente à Terça-Feira - Agronegócio, ADS, RH e Logística',
														title : '9/mar/2019',
														datetime : new Date(
																currentYear, 2,
																9),
														endDate : new Date(
																currentYear, 2,
																9)
													},

													{
														id : 18,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/mar/2019',
														datetime : new Date(
																currentYear, 2,
																10),
														endDate : new Date(
																currentYear, 2,
																10)
													},

													{
														id : 19,
														description : 'Reposição Referente à Sábado - Agronegócio, ADS, RH e Logística',
														title : '16/mar/2019',
														datetime : new Date(
																currentYear, 2,
																16),
														endDate : new Date(
																currentYear, 2,
																16)
													},

													{
														id : 20,
														description : 'Reposição Referente à Quarta-Feira - Agronegócio, ADS, RH e Logística',
														title : '23/mar/2019',
														datetime : new Date(
																currentYear, 2,
																23),
														endDate : new Date(
																currentYear, 2,
																23)
													},

													// ABRIL

													{
														id : 21,
														description : 'Semana 10 - Avaliações presenciais do EAD',
														title : '8/abr/2019 - 13/abr/2019',
														datetime : new Date(
																currentYear, 3,
																8),
														endDate : new Date(
																currentYear, 3,
																13)
													},

													{
														id : 22,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/abr/2019',
														datetime : new Date(
																currentYear, 3,
																10),
														endDate : new Date(
																currentYear, 3,
																10)
													},

													{
														id : 23,
														description : 'Prazo FINAL para desistência de disciplinas',
														title : '15/abr/2019',
														datetime : new Date(
																currentYear, 3,
																15),
														endDate : new Date(
																currentYear, 3,
																15)
													},

													{
														id : 24,
														description : 'Não haverá aula - Emenda de Feriado (Paixão de Cristo - Páscoa)',
														title : '19/abr/2019 - 21/abr/2019',
														datetime : new Date(
																currentYear, 3,
																19),
														endDate : new Date(
																currentYear, 3,
																21)
													},

													// MAIO

													{
														id : 25,
														description : 'Não haverá aula - Dia do Trabalho',
														title : '1/mai/2019',
														datetime : new Date(
																currentYear, 4,
																1),
														endDate : new Date(
																currentYear, 4,
																1)
													},

													{
														id : 26,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/mai/2019',
														datetime : new Date(
																currentYear, 4,
																10),
														endDate : new Date(
																currentYear, 4,
																10)
													},

													{
														id : 27,
														description : 'Apresentação de projeto de HAES (Projeto, Estágio e TG) para 2º semestre de 2019 - Diretoria Administrativa',
														title : '21/mai/2019 - 26/mai/2019',
														datetime : new Date(
																currentYear, 4,
																21),
														endDate : new Date(
																currentYear, 4,
																26)
													},

													{
														id : 28,
														description : 'Início do período das Inscrições para vagas remanescentes e transferência (Presencia) 2º/2019',
														title : '27/mai/2019 - 7/jun/2019',
														datetime : new Date(
																currentYear, 4,
																27),
														endDate : new Date(
																currentYear, 5,
																7)
													},

													// JUNHO

													{
														id : 29,
														description : 'Início da Pré-Matrícula para os cursos - AGRO, ADS, LOG e RH ',
														title : '10/jun/2019 - 5/jul/2019',
														datetime : new Date(
																currentYear, 5,
																10),
														endDate : new Date(
																currentYear, 6,
																5)
													},

													{
														id : 30,
														description : 'Prazo máximo para entrega de Declaração de INSS - Diretoria Administrativa',
														title : '10/jun/2019',
														datetime : new Date(
																currentYear, 5,
																10),
														endDate : new Date(
																currentYear, 5,
																10)
													},

													{
														id : 31,
														description : 'Entrega dos relatórios dos projetos de HAES do 1º semestre de 2019 - Diretoria Administrativa',
														title : '10/jun/2019 - 15/jun/2019',
														datetime : new Date(
																currentYear, 5,
																10),
														endDate : new Date(
																currentYear, 5,
																15)
													},

													{
														id : 32,
														description : 'Semana 19 - Avaliações presenciais do EAD',
														title : '10/jun/2019 - 15/jun/2019',
														datetime : new Date(
																currentYear, 5,
																10),
														endDate : new Date(
																currentYear, 5,
																15)
													},

													{
														id : 33,
														description : 'Não haverá aula - Emenda de Feriado - Corpus Christi',
														title : '20/jun/2019 - 22/jun/2019',
														datetime : new Date(
																currentYear, 5,
																20),
														endDate : new Date(
																currentYear, 5,
																22)
													},

													{
														id : 34,
														description : 'Prova Final - EAD',
														title : '24/jun/2019 - 25/jun/2019',
														datetime : new Date(
																currentYear, 5,
																24),
														endDate : new Date(
																currentYear, 5,
																25)
													},

													{
														id : 35,
														description : 'Avaliações Finais e lançamento de notas no SIGA',
														title : '24/jun/2019 - 29/jun/2019',
														datetime : new Date(
																currentYear, 5,
																24),
														endDate : new Date(
																currentYear, 5,
																29)
													},

													{
														id : 36,
														description : 'Prazo final para entrega de médias finais (EaD e Presencial)',
														title : '29/jun/2019',
														datetime : new Date(
																currentYear, 5,
																29),
														endDate : new Date(
																currentYear, 5,
																29)
													},

													{
														id : 37,
														description : 'Término das Aulas do 1º Semestre Letivo de 2019',
														title : '29/jun/2019',
														datetime : new Date(
																currentYear, 5,
																29),
														endDate : new Date(
																currentYear, 5,
																29)
													},

													// JULHO

													{
														id : 38,
														description : 'Prazo final para solicitar revisão de notas (Art. 41 Reg.) ',
														title : '4/jul/2019',
														datetime : new Date(
																currentYear, 6,
																4),
														endDate : new Date(
																currentYear, 6,
																4)
													},

													{
														id : 39,
														description : 'Divulgação resultados de revisão de notas / Prazo final para erratas',
														title : '9/jul/2019',
														datetime : new Date(
																currentYear, 6,
																9),
														endDate : new Date(
																currentYear, 6,
																9)
													},

													{
														id : 40,
														description : 'Dia da Revolução Constitucionalista',
														title : '9/jul/2019',
														datetime : new Date(
																currentYear, 6,
																9),
														endDate : new Date(
																currentYear, 6,
																9)
													},
													{
														id : 41,
														description : '* Início do Semestre Letivo',
														title : '29/jul/2019',
														datetime : new Date(
																currentYear, 6,
																29),
														endDate : Date(
																currentYear, 6,
																29)
													},
													{
														id : 42,
														name : '* Semana de Planejamento e Aperfeiçoamento Pedagógico',
														title : '29/jul/2019 - 2/ago/2019',
														datetime : new Date(
																currentYear, 6,
																29),
														endDate : Date(
																currentYear, 7,
																2)
													},
													{
														id : 43,
														name : '* Início das Aulas do 2º Semestre Letivo de 2019',
														title : '5/ago/2019',
														datetime : new Date(
																currentYear, 7,
																5),
														endDate : Date(
																currentYear, 7,
																5)
													},
													{
														id : 44,
														description : '* Prazo para Acomodação de Matrícula (art. 33 do Regulamento) ',
														title : '5/ago/2019 - 10/ago/2019',
														datetime : new Date(
																currentYear, 7,
																5),
														endDate : Date(
																currentYear, 7,
																10)
													},
													{
														id : 45,
														description : '* Reposição Referente à Sábado ',
														title : '17/ago/2019 - Agronegócio, ADS, RH e Logística',
														datetime : new Date(
																currentYear, 7,
																17),
														endDate : Date(
																currentYear, 7,
																17)
													},

													{
														id : 46,
														description : '* Reposição Referente à Sábado ',
														title : '24/ago/2019 - Agronegócio, ADS, RH e Logística',
														datetime : new Date(
																currentYear, 7,
																24),
														endDate : Date(
																currentYear, 7,
																24)
													},

													{
														id : 47,
														description : '* Reposição Referente à Sexta-Feira ',
														title : '31/ago/2019 - Agronegócio, ADS, RH e Logística ',
														datetime : new Date(
																currentYear, 7,
																31),
														endDate : Date(
																currentYear, 7,
																31)
													},
													{
														id : 48,
														description : '* Não haverá aula ',
														title : '7/set/2019 - Independência do Brasil',
														datetime : new Date(
																currentYear, 8,
																7),
														endDate : Date(
																currentYear, 8,
																7)
													},

													{
														id : 49,
														description : '* Reposição Referente à Sábado ',
														title : '14/set/2019 - Agronegócio, ADS, RH e Logística',
														datetime : new Date(
																currentYear, 8,
																14),
														endDate : Date(
																currentYear, 8,
																14)
													},

													{
														id : 50,
														description : '* Reposição Referente à Sábado ',
														title : '28/set/2019 - Agronegócio, ADS, RH e Logística',
														datetime : new Date(
																currentYear, 8,
																28),
														endDate : Date(
																currentYear, 8,
																28)
													},
													{
														id : 51,
														description : '* Semana 09',
														title : '5/out/2019 - 7/out/2019 - Avaliações Presenciais do EAD',
														datetime : new Date(
																currentYear, 9,
																5),
														endDate : Date(
																currentYear, 9,
																7)
													},
													{
														id : 52,
														description : '* Prazo Final para Desistência de Disciplinas (at. 34 do Regulamento)',
														title : '10/out/2019 - Exceto para alunos ingressantes',
														datetime : new Date(
																currentYear, 9,
																10),
														endDate : Date(
																currentYear, 9,
																10)
													},
													{
														id : 53,
														description : '* Não haverá aula ',
														title : '12/out/2019 Emenda de Feriado - Nossa Senhora do Brasil',
														datetime : new Date(
																currentYear, 9,
																12),
														endDate : Date(
																currentYear, 9,
																12)
													},

													{
														id : 54,
														description : '* Não haverá aula ',
														title : '2/nov/2019 - Emenda de Feriado - Finados',
														datetime : new Date(
																currentYear,
																10, 2),
														endDate : Date(
																currentYear,
																10, 2)
													},

													{
														id : 55,
														description : '* Prazo Final de Trancamento de Matrícula',
														title : '4/nov/2019 - Exceto para alunos ingressantes (art. 35 do Regulamento.',
														datetime : new Date(
																currentYear,
																10, 4),
														endDate : Date(
																currentYear,
																10, 4)
													},

													{
														id : 56,
														description : '* Não haverá aula ',
														title : '15/nov/2019 - 16/nov/2019 - Emenda de Feriado - Proclamação da República',
														datetime : new Date(
																currentYear,
																10, 15),
														endDate : Date(
																currentYear,
																10, 16)
													},
													{
														id : 57,
														description : '* Inscrições para o Vagas Remanescentes e Transferência de Cursos / 1º 2020 ',
														title : '21/nov/2019 - 30/nov/2019',
														datetime : new Date(
																currentYear,
																10, 21),
														endDate : Date(
																currentYear,
																10, 30)
													},
													{
														id : 58,
														description : '* Início da Pré Matrícula',
														title : '2/dez/2019 - 13/dez/2019 - Agronegócio, ADS, RH e Logística',
														datetime : new Date(
																currentYear,
																11, 2),
														endDate : Date(
																currentYear,
																11, 13)
													},

													{
														id : 59,
														description : '* Abertura do sistema para lançamento das notas no SIGA',
														title : '9/dez/2019 - 14/dez/2019',
														datetime : new Date(
																currentYear,
																11, 9),
														endDate : Date(
																currentYear,
																11, 14)
													},

													{
														id : 60,
														description : '* Semana 19',
														title : '7/dez/2019 - 9/dez/2019 - Avaliações Presenciais do EAD',
														datetime : new Date(
																currentYear,
																11, 7),
														endDate : Date(
																currentYear,
																11, 9)
													},

													{
														id : 61,
														description : '* Provas Finais',
														title : '12/dez/2019 - Apenas para EAD',
														datetime : new Date(
																currentYear,
																11, 12),
														endDate : Date(
																currentYear,
																11, 12)
													},
													{
														id : 62,
														description : '* Prazo para Solicitação de Erratas (Revisão de Notas e Faltas)',
														title : '15/dez/2019 - 20/dez/2019 - Apenas para alunos',
														datetime : new Date(
																currentYear,
																11, 15),
														endDate : Date(
																currentYear,
																11, 20)
													},

													{
														id : 63,
														description : '* Prazo para Responder Solicitação de Erratas (Revisão de Notas e Faltas)',
														title : '20/dez/2019 - 24/dez/2019 - Apenas para docentes',
														datetime : new Date(
																currentYear,
																11, 20),
														endDate : Date(
																currentYear,
																11, 24)
													},

													{
														id : 64,
														description : '* Término das aulas do 2º Semestre Letivo de 2019)',
														title : '21/dez/2019',
														datetime : new Date(
																currentYear,
																11, 21),
														endDate : Date(
																currentYear,
																11, 21)
													} ]

										});
					});

});
