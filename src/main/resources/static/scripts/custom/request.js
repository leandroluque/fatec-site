$(document).ready(function() {
    var contextPath = $('meta[name="_context_path"]').attr('content');

    const documentTypes = document.getElementsByName("documentTypes");
    const cancelApplicationDescription = document.getElementById("cancelApplicationDescription");
    const withdrawCourseDescription = document.getElementById("withdrawCourseDescription");
    const withdrawDisciplineDescription = document.getElementById("withdrawDisciplineDescription");

    documentTypes.forEach(checkbox => {
        checkbox.addEventListener('change', function handleChange() {
            handleDocumentTypeChanges(checkbox);
        });
        handleDocumentTypeChanges(checkbox);
    })


    function handleDocumentTypeChanges(checkbox) {
        if(checkbox.value === 'CANCEL_APPLICATION') {
            if(cancelApplicationDescription) {
                if(checkbox.checked) {
                    cancelApplicationDescription.classList.remove('hide');
                    cancelApplicationDescription.required = true;
                } else {
                    cancelApplicationDescription.value = "";
                    cancelApplicationDescription.classList.add('hide');
                    cancelApplicationDescription.required = false;
                }
            }
        } else if(checkbox.value === 'WITHDRAW_COURSE') {
            if(withdrawCourseDescription) {
                if(checkbox.checked) {
                    withdrawCourseDescription.classList.remove('hide');
                    withdrawCourseDescription.required = true;
                } else {
                    withdrawCourseDescription.value = "";
                    withdrawCourseDescription.classList.add('hide');
                    withdrawCourseDescription.required = false;
                }
            }
        } else if(checkbox.value === 'WITHDRAWAL_DISCIPLINE') {
            if(withdrawDisciplineDescription) {
                if(checkbox.checked) {
                    withdrawDisciplineDescription.classList.remove('hide');
                    withdrawDisciplineDescription.required = true;
                } else {
                    withdrawDisciplineDescription.value = "";
                    withdrawDisciplineDescription.classList.add('hide')
                    withdrawDisciplineDescription.required = false;
                }
            }
        }
    }

    countMaxLength('protocol', 30, 'protocolRemainingLength');
    countMaxLength('registry', 30, 'registryRemainingLength');
    countMaxLength('name', 100, 'nameRemainingLength');
    countMaxLength('email', 50, 'emailRemainingLength');
    countMaxLength('phone', 50, 'phoneRemainingLength');
    countMaxLength('comments', 400, 'commentsRemainingLength');

    $('#phone').mask('00-00000-0000', {
	    reverse : true
    });
    
    $('#registry').focus();

    
});