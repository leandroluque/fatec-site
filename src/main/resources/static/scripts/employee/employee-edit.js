$(document).ready(function() {
    var contextPath = $('meta[name="_context_path"]').attr('content');
    const congregationMember = document.getElementById('congregationMember');
    const congregationCheckboxes = document.querySelectorAll('.congregation-checkbox');
    const internshipResponsible = document.getElementById('internshipResponsible');
    const internshipResponsibleCheckboxes = document.querySelectorAll('.internshipResponsible-checkbox');
    const allocation = document.getElementById('allocation');
    const coordinatorCourseFieldSet = document.getElementById('coordinatorCourseFieldSet');
    const coordinatorCheckboxes = document.querySelectorAll('.coordinator-checkbox');

    countMaxLength('name', 100, 'nameRemainingLength');
    countMaxLength('curriculum', 2000, 'curriculumRemainingLength');
    countMaxLength('lattes', 100, 'lattesRemainingLength');
    countMaxLength('homepage', 100, 'homepageRemainingLength');
    countMaxLength('imageAlternativeDescription', 100, 'imageAlternativeDescriptionRemainingLength');

    $('#name').focus();

    function toggleCoordinatorCourse() {
        if(allocation.options[allocation.selectedIndex].value === 'COORDENADOR') {
            coordinatorCourseFieldSet.disabled = false;
        } else {
            coordinatorCourseFieldSet.disabled = true;
        }
    }
    toggleCoordinatorCourse();

    allocation.addEventListener('change', function() {
        toggleCoordinatorCourse();
    })

    coordinatorCheckboxes.forEach(
        checkbox => checkbox.addEventListener('change', function() {
            const that = this;
            if (this.checked) {
                document.querySelectorAll('.congregation-checkbox:checked').forEach(
                    checkbox => {
                        if(checkbocx !== that) {
                            checkbox.checked = false;
                        }
                    }
                )
            }
        })
    )

    congregationCheckboxes.forEach(
        checkbox => checkbox.addEventListener('change', function() {
            if (this.checked) {
                congregationMember.checked = true;
            } else {
                if(document.querySelectorAll('.congregation-checkbox:checked').length === 0) {
                    congregationMember.checked = false;
                }
            }
        })
    )

    internshipResponsibleCheckboxes.forEach(
        checkbox => checkbox.addEventListener('change', function() {
            if (this.checked) {
                internshipResponsible.checked = true;
            } else {
                if(document.querySelectorAll('.internshipResponsible-checkbox:checked').length === 0) {
                    internshipResponsible.checked = false;
                }
            }
        })
    )

});