$(document)
		.ready(
				function() {
					var contextPath = $('meta[name="_context_path"]').attr(
							'content');

					$('#list')
							.DataTable(
									{
										'responsive' : true,
										'order' : [ [ 1, 'asc' ] ],
										'paging' : true,
										'sPaginationType' : 'full_numbers',
										'searching' : true,
										'ordering' : true,
										'processing' : true,
										'serverSide' : true,
										'ajax' : contextPath
												+ 'enrollment/table',
										'language' : {
											'url' : contextPath
													+ 'scripts/pt-br.json'
										},
										'columns' : [
												{
													'data' : 'hash',
													'bSortable' : false,
													'bSearchable' : false,
													'render' : function(data,
															type, row) {
														if (type === 'display') {
															return '<a href="'
																	+ contextPath
																	+ 'admin/enrollment/'
																	+ row.hash
																	+ '"><i class="fa fa-pencil-square-o text-success" aria-hidden="true"></i></a>';
														}
														return data;
													},
												},
												{
													'data' : 'course',
													'bSortable' : true,
													'render' : function(data,
															type, row) {
														return data;
													},
												},
												{
													'data' : 'shift',
													'bSortable' : true,
													'render' : function(data,
															type, row) {
														return data;
													},
												},
												{
													'data' : 'registry',
													'bSortable' : true,
													'render' : function(data,
															type, row) {
														return data;
													},
												},
												{
													'data' : 'name',
													'bSortable' : true,
													'render' : function(data,
															type, row) {
														return data;
													},
												},
												{
													'data' : 'howManyDisciplines',
													'bSortable' : false,
													'render' : function(data,
															type, row) {
														return data;
													},
												} ]
									});
				});