$(document)
    .ready(
        function () {
            var contextPath = $('meta[name="_context_path"]').attr(
                'content');

            $('#list')
                .DataTable(
                    {
                        'responsive': true,
                        'order': [[3, 'asc']],
                        'paging': true,
                        'sPaginationType': 'full_numbers',
                        'searching': true,
                        'ordering': true,
                        'processing': true,
                        'serverSide': true,
                        'ajax': contextPath
                            + 'admin/enrollment/student/table',
                        'language': {
                            'url': contextPath
                                + 'scripts/pt-br.json'
                        },
                        'columns': [
                            {
                                'data': 'hash',
                                'bSortable': false,
                                'bSearchable': false,
                                'render': function (data,
                                                    type, row) {
                                    if (type === 'display') {
                                        return '<a href="'
                                            + contextPath
                                            + 'admin/enrollment/view/student/'
                                            + row.hash
                                            + '"><i class="fa fa-search text-success" aria-hidden="true"></i></a>';
                                    }
                                    return data;
                                },
                            },
                            {
                                'data': 'enrollmentHash',
                                'bSortable': false,
                                'bSearchable': false,
                                'render': function (data,
                                                    type, row) {
                                    if (type === 'display' && row.hasFile) {
                                        return '<a href="'
                                            + contextPath
                                            + 'enrollment/'
                                            + row.enrollmentHash
                                            + '/file'
                                            + '" target="_blank">'
                                            + '<i class="fa fa-download text-success" aria-hidden="true"></i></a>';
                                    }
                                    return "";
                                },
                            },
                            {
                                'data': 'course',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'ra',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'shift',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'name',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'internship',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'capstone',
                                'bSortable': true,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            },
                            {
                                'data': 'howManyDisciplines',
                                'bSortable': false,
                                'render': function (data,
                                                    type, row) {
                                    return data;
                                },
                            }]
                    });
        });