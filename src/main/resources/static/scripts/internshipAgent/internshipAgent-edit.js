const agentForms = document.getElementById('agent-forms');
const cloneModel = document.querySelector('.clone-model');
let agentFormCount = 0;

registerFormEvents(document);

function registerFormEvents(base) {
    const addAgentBtn = base.querySelector('.add-agent-btn');
    const deleteAgentBtns = base.querySelectorAll('.delete-agent-btn');

    // Add event listener to the add agent button
    addAgentBtn.addEventListener('click', () => {
        const newAgentForm = cloneModel.cloneNode(true);

        // Update the id and name of the input element
        agentFormCount++;
        const nameInput = newAgentForm.querySelector('input[type="text"]');
        nameInput.setAttribute('id', `name_${agentFormCount}`);
        nameInput.setAttribute('name', 'names');
        nameInput.value = "";
        nameInput.required = true;

        newAgentForm.querySelector('.delete-button').classList.remove('invisible');

        // Append the new agent form to the container
        agentForms.appendChild(newAgentForm);

        registerFormEvents(newAgentForm);
    });
    deleteAgentBtns.forEach((deleteAgentBtn) => {
        deleteAgentBtn.addEventListener('click', () => {
            const agentForm = deleteAgentBtn.parentNode.parentNode;
            agentForm.remove();
        });
    });
}