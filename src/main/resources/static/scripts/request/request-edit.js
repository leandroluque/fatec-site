$(document).ready(function() {
    var contextPath = $('meta[name="_context_path"]').attr('content');

    countMaxLength('registry', 30, 'registryRemainingLength');
    countMaxLength('name', 100, 'nameRemainingLength');
    countMaxLength('email', 50, 'emailRemainingLength');
    countMaxLength('phone', 50, 'phoneRemainingLength');
    countMaxLength('comments', 400, 'commentsRemainingLength');

    $('#phone').mask('00-00000-0000', {
	reverse : true
    });
    
    $('#registry').focus();
   
   	document.querySelectorAll(`[name="documentTypes"]`).forEach(it => {
		it.addEventListener("change", (e) => {
			if(e.target.checked) {
				document.querySelector(`.doc-type-${e.target.value}`).removeAttribute("disabled");
			} else {
				document.querySelector(`.doc-type-${e.target.value}`).setAttribute("disabled", true);
			}
		});
	});
   
    function toggleSituation() {
	    document.querySelectorAll(`[name="documentTypes"]`).forEach(it => {
			if(it.checked) {
				document.querySelector(`.doc-type-${it.value}`).removeAttribute("disabled");
			} else {
				document.querySelector(`.doc-type-${it.value}`).setAttribute("disabled", true);
			}
		});
	}
	
	toggleSituation();
    
});