function changedWeekDay(target) {
    let disciplineHash = target.parent().parent().attr('disciplineHash');
    target.parent().parent().parent().find('tr[disciplineHash=' + disciplineHash + '] select.weekDay').each(function(i, obj) {
	if ($(this).find('option:selected').val() === '') {
	    $(this).find('option[value=' + target.find('option:selected').val() + ']').attr('selected', true);
	}
    });
}

function changedProfessor(target) {
    let disciplineHash = target.parent().parent().attr('disciplineHash');
    target.parent().parent().parent().find('tr[disciplineHash=' + disciplineHash + '] select.professor').each(function(i, obj) {
	$(this).find('option[value=' + target.find('option:selected').val() + ']').attr('selected', true);
    });
}

function changedTimeSlot(target) {
    let disciplineHash = target.parent().parent().attr('disciplineHash');
    let positionOfTarget = -1;
    target.parent().parent().parent().find('tr[disciplineHash=' + disciplineHash + '] select.timeSlot').each(function(i, obj) {
	if (target.is($(this))) {
	    positionOfTarget = i;
	}
    });
    target.parent().parent().parent().find('tr[disciplineHash=' + disciplineHash + '] select.timeSlot').each(function(i, obj) {
	if ($(this).find('option:selected').val() === '') {
	    let $current = target.find('option:selected');
	    let howFar = Math.abs(positionOfTarget - i);
	    while ($current !== undefined && howFar > 0) {
		if (positionOfTarget > i) {
		    $current = $current.prev();
		} else {
		    $current = $current.next();
		}
		howFar--;
	    }
	    $(this).find('option[value=' + $current.val() + ']').attr('selected', true);
	}
    });
}

$(document).ready(function() {
    var contextPath = $('meta[name="_context_path"]').attr('content');

});