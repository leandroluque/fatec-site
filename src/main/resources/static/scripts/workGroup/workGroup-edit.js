let documentToDelete = null;
let documentId = 1;
let reportToDelete = null;
let reportId = 1;

function setDocumentHashToDelete(tableRow) {
	documentToDelete = $('#' + tableRow);
}

function setReportHashToDelete(tableRow) {
	reportToDelete = $('#' + tableRow);
}

$(document).ready(
		function() {
			var contextPath = $('meta[name="_context_path"]').attr('content');

			countMaxLength('description', 5000, 'descriptionRemainingLength');

			$('#addNewDocument').on(
					'click',
					function() {
						var newDocument = $('#documentModel').clone();

						let firstLabel = newDocument.find('.documentLabel');
						let newDocumentLabel = 'newDocuments' + (documentId);
						firstLabel.prop('for', newDocumentLabel);
						let inputFile = newDocument.find('input[type=file]');
						inputFile.prop('id', newDocumentLabel)

						let secondLabel = newDocument
								.find('.documentDescriptionLabel');
						let newDocumentDescriptionLabel = 'newDocuments'
								+ (documentId);
						secondLabel.prop('for', newDocumentDescriptionLabel);
						let inputText = newDocument.find('input[type=text]');
						inputText.prop('id', newDocumentDescriptionLabel)

						newDocument.find('.remove-current-document')
								.removeClass('invisible');

						documentId++;

						$("#documentsContainer").append(newDocument);
					});

			$('#btnConfirmDocumentDeletion').on(
					'click',
					function() {
						$('#documentContainer').append(
								'<input type="hidden" name="documentsHashesToRemove" value="'
										+ documentToDelete.data('hash')
										+ '" />');
						documentToDelete.parent().parent().remove();
						$('#documentRemoval').modal('toggle');
					});

			$('#addNewReport').on(
					'click',
					function() {
						var newReport = $('#reportModel').clone();

						let firstLabel = newReport.find('.reportLabel');
						let newReportLabel = 'newReports' + (reportId);
						firstLabel.prop('for', newReportLabel);
						let inputFile = newReport.find('input[type=file]');
						inputFile.prop('id', newReportLabel)

						let secondLabel = newReport
								.find('.reportDescriptionLabel');
						let newReportDescriptionLabel = 'newReports'
								+ (reportId);
						secondLabel.prop('for', newReportDescriptionLabel);
						let inputText = newReport.find('input[type=text]');
						inputText.prop('id', newReportDescriptionLabel)

						newReport.find('.remove-current-report')
								.removeClass('invisible');

						reportId++;

						$("#reportsContainer").append(newReport);
					});

			$('#btnConfirmReportDeletion').on(
					'click',
					function() {
						$('#reportContainer').append(
								'<input type="hidden" name="reportsHashesToRemove" value="'
										+ reportToDelete.attr('hash')
										+ '" />');
						reportToDelete.parent().parent().remove();
								$('#reportRemoval').modal('toggle');
					});

		});